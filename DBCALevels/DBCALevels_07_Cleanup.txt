----- (0,0), 0, 'cleanup of the PI explosion from the initial GPSE collision starts', offset=(0, 0), setmag=0, priority=2 -----
(g0%2)[-79](g0%2)[-80](g0%2)[-81](g1%2)[-82](g1%2)[-83](g1%2)[-84] -- PI explosion start
(g1%2)[-85](g1%2)[-86] -- phase restricted to pass through p256 stream
(g1%2)[-87](g1%2)[-88](g1%2)[-89] -- phase restricted to pass through p256 stream 
(g0%1)[-60](g0%1)[-61](g0%1)[-62](g0%1)[-63](g0%1)[-64] 
(g0%1)[-62](g0%1)[-63](g0%1)[-64](g0%1)[-65](g0%1)[-66](g0%1)[-67] 
(g1%2)[-69](g1%2)[-70] -- phase restricted to pass through p256 stream
(g1%2)[-68](g1%2)[-69](g1%2)[-70](g1%2)[-71](g1%2)[-76] -- phase restricted to pass through p256 stream
(g1%2)[-56](g1%2)[-57](g1%2)[-58](g1%2)[-59](g1%2)[-60](g1%2)[-61] -- phase restricted to pass through p256 stream
(g1%2)[-43](g1%2)[-44](g0%2)[-47](g0%2)[-48] 
(g0%2)[-43](g0%2)[-44](g0%2)[-45](g1%2)[-46](g1%2)[-47](g1%2)[-48] -- PI explosion end
----- (0,0), 0, 'cleanup of the PI explosion from the initial GPSE collision ended,', priority=2 -----
(g0%1)[4] -- GPSE stopper start
(g1%2)[1] 
(g0%2)[-6] 
(g0%2)[-23] 
(g0%2)[-21] 
(g0%2)[-33] 
(g0%1)[-33] -- GPSE stopper end
----- (0,0), 0, 'GPSE stopping block positioned', priority=2 -----
(g0%1)[87] -- 1st FSE glider start
(g1%2)[90]
(g0%2)[92](g0%2)[93](g1%2)[96](g1%2)[97] 
(g1%2)[92] 
(g0%2)[98] 
(g1%2)[93] 
(g0%1)[92] 
(g0%1)[100] 
(g0%1)[91] -- 1st FSE glider end both phases are ok on FSW
----- (1,1), 4, 'First glider arrived to FSE', time_offs=-65536, magstep=-1, priority=2 -----
----- (0,0), 0, 'Releasing 3 gliders FSE to prepare a one time turner', bit_offs=-1, priority=0 -----
(g0%1)[82](g0%1)[89] -- 2nd FSE glider start
(g0%1)[97] 
(g0%1)[106] 
(g1%2)[91] 
(g0%2)[111] 
(g0%1)[110] 
(g0%1)[118]
(g0%1)[109] -- 2nd FSE glider end both phases are ok on FSW
----- (1,1), 4, 'Second glider arrived to FSE', time_offs=-65536, priority=2 -----
----- (0,0), 0, 'Releasing second glider FSE', bit_offs=-1, priority=2 -----
(g0%1)[114](g0%1)[121] -- 3rd FSE glider start
(g0%1)[106] 
(g0%1)[112] 
(g1%2)[95] 
(g1%2)[110] 
(g0%1)[102] 
(g0%1)[93] 
(g0%1)[101]
(g0%1)[92] -- 3rd FSE glider end both phases are ok on FSW
----- (1,1), 4, 'Third glider arrived to FSE', time_offs=-65536, priority=2 -----
----- (0,0), 0, 'Releasing third glider FSE', bit_offs=-1, priority=2 -----
(g0%2)[93] -- converting to target block through blinker
(g0%2)[91] -- converting to target block
----- (0,0), 0, 'Target for ECCA construction and SW switching circuitry ready', priority=2 -----
