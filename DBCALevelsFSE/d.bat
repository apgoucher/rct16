copy DBCALevels_10_FSEStartTrashRemoval.txt + DBCALevels_11_FSEECCAreflectors.txt + DBCALevels_12_FSECorderabsorbers.txt DBCALevels_FSE.txt 
copy DBCALevels_10_FSEStartTrashRemoval.txt + DBCALevels_11_FSEECCAreflectorsExtraGlider.txt + DBCALevels_12_FSECorderabsorbers.txt DBCALevels_FSE_ExtraGlider.txt 
copy ..\DBCALevels\DBCALevels_central.txt + DBCATagSwitchFSE.txt + DBCALevels_FSE.txt DBCALevels.txt
copy ..\DBCALevels\DBCALevels_central.txt + DBCATagSwitchFSE.txt + DBCALevels_FSE_ExtraGlider.txt DBCALevels_ExtraGlider.txt
rem we have to choose the variant ending in semistate 2 rather to semistate 4
copy DBCALevels_ExtraGlider.txt c:\Golly\DBCALevels.txt
