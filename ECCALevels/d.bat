copy ECCALevels_13a_DBCAStopWithBoat.txt+ECCALevels_13b_StopP256Discounted_2.txt+ECCALevels_13c_Destroyal_2.txt ECCALevels_13_DBCAdestroyalBoat2.txt
copy ECCALevels_13a_DBCAStopWithoutBoat.txt+ECCALevels_13b_StopP256Discounted_2.txt+ECCALevels_13c_Destroyal_2.txt ECCALevels_13_DBCAdestroyalNoBoat2.txt
copy ECCALevels_13a_DBCAStopWithBoat.txt+ECCALevels_13b_StopP256Discounted_4.txt+ECCALevels_13c_Destroyal_4.txt ECCALevels_13_DBCAdestroyalBoat4.txt
copy ECCALevels_13a_DBCAStopWithoutBoat.txt+ECCALevels_13b_StopP256Discounted_4.txt+ECCALevels_13c_Destroyal_4.txt ECCALevels_13_DBCAdestroyalNoBoat4.txt
rem chose one corresponding to current 08_ECCA dependent variant ... (versions with _4 are not finished yet)
copy ECCALevels_13_DBCAdestroyalNoBoat2.txt ECCALevels_13_DBCAdestroyal.txt

copy ECCALevels_15c1.txt+ECCALevels_15c2.txt+ECCALevels_15c3.txt+ECCALevels_15c4.txt+ECCALevels_15c5.txt+ECCALevels_15c6.txt+ECCALevels_15c7.txt+ECCALevels_15c8.txt+ECCALevels_15c9.txt+ECCALevels_15c10.txt+ECCALevels_15c11.txt+ECCALevels_15c12.txt+ECCALevels_15c13.txt+ECCALevels_15c14.txt+ECCALevels_15c15.txt+ECCALevels_15c16.txt+ECCALevels_15c17.txt+ECCALevels_15c18.txt+ECCALevels_15c19.txt+ECCALevels_15c20.txt+ECCALevels_15c21.txt+ECCALevels_15c22.txt+ECCALevels_15c23.txt+ECCALevels_15c24.txt+ECCALevels_15c25.txt+ECCALevels_15c26.txt+ECCALevels_15c27.txt+ECCALevels_15c28.txt+ECCALevels_15c29.txt+ECCALevels_15c30.txt+ECCALevels_15c31.txt+ECCALevels_15c32.txt+ECCALevels_15c33.txt+ECCALevels_15c34.txt+ECCALevels_15c35.txt+ECCALevels_15c36.txt+ECCALevels_15c37.txt+ECCALevels_15c38.txt+ECCALevels_15c39.txt ECCALevels_15c.txt

copy ECCALevels_15f0.txt+ECCALevels_15f1.txt+ECCALevels_15f2.txt+ECCALevels_15f3.txt+ECCALevels_15f4.txt+ECCALevels_15f5.txt+ECCALevels_15f6.txt+ECCALevels_15f7.txt+ECCALevels_15f8.txt+ECCALevels_15f9.txt+ECCALevels_15f10.txt+ECCALevels_15f11.txt+ECCALevels_15f12.txt+ECCALevels_15f13.txt+ECCALevels_15f14.txt+ECCALevels_15f15.txt+ECCALevels_15f16.txt+ECCALevels_15f17.txt+ECCALevels_15f18.txt+ECCALevels_15f19.txt+ECCALevels_15f20.txt+ECCALevels_15f21.txt+ECCALevels_15f22.txt ECCALevels_15f.txt

copy ECCALevels_15e0.txt+ECCALevels_15e1.txt+ECCALevels_15e2.txt+ECCALevels_15e3.txt+ECCALevels_15e4.txt+ECCALevels_15e5.txt+ECCALevels_15e6.txt+ECCALevels_15e7.txt+ECCALevels_15e8.txt+ECCALevels_15e9.txt+ECCALevels_15e10.txt+ECCALevels_15e11.txt+ECCALevels_15e12.txt+ECCALevels_15e13.txt+ECCALevels_15e14.txt+ECCALevels_15e15.txt+ECCALevels_15e16.txt+ECCALevels_15e17.txt+ECCALevels_15e18.txt+ECCALevels_15e19.txt ECCALevels_15e.txt

copy ECCALevels_15a1.txt+ECCALevels_15a2.txt+ECCALevels_15a3.txt+ECCALevels_15a4.txt+ECCALevels_15a5.txt+ECCALevels_15a6.txt ECCALevels_15a.txt

copy ECCALevels_15a.txt+ECCALevels_15c.txt+ECCALevels_15f.txt+ECCALevels_15d.txt+ECCALevels_15e.txt+ECCALevels_15f23.txt+ECCALevels_15g.txt ECCALevels_15.txt
copy ECCALevels_16a_ATBC.txt+ECCALevels_16b_ATBC.txt ECCALevels_16_ATBC.txt

rem 15c40 removed
rem reordered fleets ash(15a), CSEL (15c), CNWR(15f), FSWabsorbers(15d), CSWL+CSWR(15e), CNWL(15f23)

rem copy ECCALevels_13_DBCAdestroyal.txt+ECCALevels_14_White.txt+ECCALevels_15.txt+ECCALevels_16.txt+ECCALevels_17.txt ECCALevels_central.txt
copy ECCALevels_13_DBCAdestroyal.txt+ECCALevels_14_WhiteSoD.txt+ECCALevels_15.txt+ECCALevels_16.txt+ECCALevels_17.txt ECCALevels_central.txt
rem copy ECCALevels_13_DBCAdestroyal.txt+ECCALevels_14_WhiteSoD.txt+ECCALevels_15.txt+ECCALevels_16_ATBC.txt+ECCALevels_17_ATBC.txt ECCALevels_central.txt

copy ECCALevels_central.txt c:\Golly\ECCALevels.txt
cd ..\ECCALevelsFNW
d.bat
