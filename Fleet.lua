local g = golly()
local workDir="c:\\Golly\\Fleet\\"
local setboxInc = 4

local function inttostring(num)
  return string.sub(num,1,string.find(num..".","%.")-1)
end

local function setbox(dx,dy)
  for x=dx,dx+1 do
    for y=dy,dy+1 do
      g.setcell(x,y,g.getcell(x,y)+setboxInc)
    end
  end
end


local function SELfleet()
  local i = io.open(workDir..'SELFleet.txt',"r")
  local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="CSEL",-1,-2,-3,3 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0-string.sub(inputLine,1,pos-1),0-string.sub(inputLine,pos+1)
    local norm=(dx+dy)//16
    a:write("echo "..prefix..inttostring(level).." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dx-8*k,yShift+dy+8*k)
      local basename=prefix..inttostring(level)..string.sub("ABCDEFGHIJKLMNOP",k-mink+1,k-mink+1)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 15 65535\r")
      a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
    end
    a:write("echo "..prefix..inttostring(level).." ends\r")
    inputLine = i:read()
    level=1+level
  end
  i:close()
  a:close()
  g.dellayer()
end

local function NWRfleet()
  local i = io.open(workDir..'NWRFleet.txt',"r")
  local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="CNWR",-1,-1,-3,3 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
    local norm=(dx+dy)//16
    a:write("echo "..prefix..inttostring(level).." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dx-8*k,yShift+dy+8*k)
      local basename=prefix..inttostring(level)..string.sub("ABCDEFGHIJKLMNOP",k-mink+1,k-mink+1)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 14 65535\r")
      a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
    end
    a:write("echo "..prefix..inttostring(level).." ends\r")
    inputLine = i:read()
    level=1+level
  end
  i:close()
  a:close()
  g.dellayer()
end

local function NWLfleet()
  local i = io.open(workDir..'NWLFleet.txt',"r")
  local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="CNWLF",-1,-1,-3,3 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
    local norm=(dx+dy)//16
    a:write("echo "..prefix..inttostring(level).." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dx-8*k,yShift+dy+8*k)
      local basename=prefix..inttostring(level)..string.sub("ABCDEFGHIJKLMNOP",k-mink+1,k-mink+1)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 14 65535\r")
      a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
    end
    a:write("echo "..prefix..inttostring(level).." ends\r")
    inputLine = i:read()
    level=1+level
  end
  i:close()
  a:close()
  g.dellayer()
end

local function SWLfleet()
  local i = io.open(workDir..'SWLFleet.txt',"r")
  local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="CSWL",-1,-1,-3,3 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
    local norm=(dy+dx)//16
    a:write("echo "..prefix..inttostring(level).." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dy-8*k,yShift-dx-8*k)
      local basename=prefix..inttostring(level)..string.sub("ABCDEFGHIJKLMNOP",k-mink+1,k-mink+1)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 14 65535\r")
      a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
    end
    a:write("echo "..prefix..inttostring(level).." ends\r")
    inputLine = i:read()
    level=1+level
  end
  i:close()
  a:close()
  g.dellayer()
end

local function SWRfleet()
  local i = io.open(workDir..'SWRFleet.txt',"r")
  local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="CSWRF",-1,-1,-3,16 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
    local norm=(dy-dx)//16
    a:write("echo "..prefix..inttostring(level).." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dy-8*k,yShift-dx-8*k)
      local basename=prefix..inttostring(level)..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 14 65535\r")
      a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
    end
    a:write("echo "..prefix..inttostring(level).." ends\r")
    inputLine = i:read()
    level=1+level
  end
  i:close()
  a:close()
  g.dellayer()
end

local function FNWCorderabsorbers()
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="FNWCorderabsorbers",-1,-1,-16,3 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  local pos=string.find(inputLine,";")
  local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
  local norm=(dx+dy)//16
  a:write("echo "..prefix.." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
  dx,dy=dx-8*norm,dy-8*norm
  for k=mink,maxk do
    g.new('Work')
    g.putcells(basepatt)
    setbox(xShift-dx-8*k,yShift+dy+8*k)
    local basename=prefix..string.sub("ABCDEFGHIJKLMNOP",k-mink+1,k-mink+1)
    g.save(workDir..basename.."4pslmake.mc","mc")
    a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 80 65535\r")
    a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
  end
  a:write("echo "..prefix.." ends\r")
  a:close()
  g.dellayer()
end

local function FSWNWCorderabsorber()
  local i = io.open(workDir..'FSWNWCorderabsorber.txt',"r")
  local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="FSWNWCorderabsorber",-1,-1,-12,12 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..prefix..'.sh',"wb")
  while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
    local norm=0
    a:write("echo "..prefix..inttostring(level).." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\r")
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dx-8*k,yShift+dy-8*k)
      local basename=prefix..inttostring(level)..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 14 65535\r")
      a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename..".mc\r")
    end
    a:write("echo "..prefix..inttostring(level).." ends\r")
    inputLine = i:read()
    level=1+level
  end
  i:close()
  a:close()
  g.dellayer()
end

local function FSWCorderabsorber(shipNo)
  --local i = io.open(workDir..'FSWCorderabsorber..shipNo.txt',"r")
  --local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  --setboxInc = 1 -- we are trying to connect patterns not specifying optimal start location
  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="FSWCorderabsorber"..shipNo,-1,-1,-3,3 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..'fleet1.sh',"wb")
  a:write("mkdir "..prefix.."\n")
  a:write("mv "..prefix..".sh "..prefix.."\n")
  for k=mink,maxk do
    local basename=prefix..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
    a:write("mv "..basename.."* "..prefix.."\n")
  end
  a:write("cd "..prefix.."\n")
  a:write("chmod +x "..prefix..".sh\n")
  a:write("./"..prefix..".sh\n")
  a:close()
  local a = io.open(workDir..prefix..'.sh',"wb")
  for k=mink,maxk do
    local basename=prefix..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
    a:write("sbatch "..basename..".sh\n")
  end
  a:close()
  --while inputLine do
    local pos=string.find(inputLine,";")
    local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
    local norm=0
    dx,dy=dx-8*norm,dy-8*norm
    for k=mink,maxk do
      local basename=prefix..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
      local a = io.open(workDir..basename..'.sh',"wb")
      a:write("#!/bin/bash\n#SBATCH --cpus-per-task=64\n")
      a:write("#SBATCH --time=12:00:00\n")
      a:write("#SBATCH --partition=mpi-hetero-long\n")
      a:write("#SBATCH --mail-user=maj@ktiml.mff.cuni.cz\n")
      a:write("cd ../pslmake\n")
      a:write("echo "..basename.." starts"..dx..","..dy.."->"..(dx-8*norm)..","..(dy-8*norm).."\n")
      g.new('Work')
      g.putcells(basepatt)
      setbox(xShift-dx+8*k,yShift+dy+8*k)
      g.save(workDir..basename.."4pslmake.mc","mc")
      a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..basename.."salvo.mc 1 65535\n")
      --a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 1 65535\n")
      --a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename.."salvo.mc\n")
      a:write("echo "..basename.." ends\n")
      a:close()
    end
    --inputLine = i:read()
  --end
  --i:close()
  g.dellayer()
end

local function Pattern()
  --local i = io.open(workDir..'FSWCorderabsorber..shipNo.txt',"r")
  --local level = 0
  local basepatt = g.getcells(g.getrect()) -- cordership directed NE seed cleaning on left edge
  g.addlayer()

  --setboxInc = 1 -- we are trying to connect patterns not specifying optimal start location
  local inputLine = "0;0"
  local prefix,xShift,yShift,mink,maxk="Pattern",-1,-1,-13,12 -- for 0;0 xShift,yShift should put block to position where the block remains at the end
  local a = io.open(workDir..'fleet1.sh',"wb")
  a:write("mkdir "..prefix.."\n")
  a:write("mv "..prefix..".sh "..prefix.."\n")
  for k=mink,maxk do
    local basename=prefix..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
    a:write("mv "..basename.."* "..prefix.."\n")
  end
  a:write("cd "..prefix.."\n")
  a:write("chmod +x "..prefix..".sh\n")
  a:write("./"..prefix..".sh\n")
  a:close()
  local a = io.open(workDir..prefix..'.sh',"wb")
  for k=mink,maxk do
    local basename=prefix..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
    a:write("sbatch "..basename..".sh\n")
  end
  a:close()
  --while inputLine do
  local pos=string.find(inputLine,";")
  local dx,dy=0+string.sub(inputLine,1,pos-1),0+string.sub(inputLine,pos+1)
  for k=mink,maxk do
    local basename=prefix..string.sub("ABCDEFGHIJKLMNOPQRSTUVWXYZ",k-mink+1,k-mink+1)
    local a = io.open(workDir..basename..'.sh',"wb")
    a:write("#!/bin/bash\n#SBATCH --cpus-per-task=64\n")
    a:write("#SBATCH --time=12:00:00\n")
    a:write("#SBATCH --partition=mpi-hetero-long\n")
    a:write("#SBATCH --mail-user=maj@ktiml.mff.cuni.cz\n")
    a:write("cd ../pslmake\n")
    a:write("echo "..basename.." starts"..dx..","..dy.."->"..dx..","..dy.."\n")
    g.new('Work')
    g.putcells(basepatt)
    setbox(xShift-dx-k,yShift+dy+k)
    g.save(workDir..basename.."4pslmake.mc","mc")
    a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..basename.."salvo.mc 1 65535\n")
    --a:write("./pslmake 1024 64 data/stdxl.bin ../"..prefix.."/".. basename.."4pslmake.mc ../"..prefix.."/"..prefix..".mc 1 65535\n")
    --a:write("mv ../"..prefix.."/"..prefix..".mc ../"..prefix.."/"..basename.."salvo.mc\n")
    a:write("echo "..basename.." ends\n")
    a:close()
  end
  --inputLine = i:read()
  --end
  --i:close()
  g.dellayer()
end

--SELfleet()
--NWRfleet()
--SWLfleet()
Pattern()
--SWRfleet()
--NWLfleet()
--FNWCorderabsorbers()
--FSWCorderabsorber("0")
--FSWCorderabsorber("Block4")