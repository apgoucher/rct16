local g = golly()
local gp = require "gplus"
local gpo = require "gplus.objects"
local gpt = require "gplus.text"
local bothColors=true -- only even Lines
local fixedColors =false -- true -- color of the replacing glider must match the original color
local upsideDown=false --false for trashmarks only
local ymxOffset = 0 --35-- -980+4 -- -6 -- -302 -- -12 -- 1520 -- -327 --598 -- -119 -- -247 -- -201 --  155 --322 --  198 -- 276 --1321 -- -1898 -- -207 -- -84 --375 --  93 --  -362 -- -362 box at 0,0
local debug=0--16--72
local confDebug=-1
local confDebugStart=-1
local showHead=""

--g.new("playLevels")
g.setbase(2)
g.setstep(15)
g.autoupdate(true)
g.autoupdate(false)
g.setpos(500,-1200)
g.setmag(-2)
local block  =  g.parse("2o$2o") -- for alternatives ...
-- g.parse("2E$2E") -- ... to see result in lifehistory ... would not detect alternatives
local boatSW = g.parse("bo$obo$2o") -- g.parse("bE$EbE$2E")
local glider = g.parse("b2o$2o$2bo")

local bn = require "../gplus.bignum"
local workDir="c:\\Golly\\"
local a --  = io.open(workDir.."BlinkerLevelsX.txt","w")
local log = io.open("c:\\Golly\\BlinkerLevels.log","w")
local oriPattern=gp.pattern()
local l,inputline,fail
oriPattern.array=g.getcells(g.getrect())

local aCache=""
local function writeACache()
  if a then
    a:write(aCache.."\n")
  end
  aCache=""
end

local function writeLog(msg)
  if log then
    log:write(msg.."\n")
  end
end

local function readXY()
  local startPos, endPos, ret
  startPos=string.find(inputline,'%[')+1
  endPos=string.find(inputline,'%]')-1
  ret=string.sub(inputline,startPos,endPos) -- blinkers center (y-x),((x+y)=8)
  return ret
end

local function inDelimiters(name, leftDelimiter, rightDelimiter)
  return string.match(name, '%' .. leftDelimiter .. '(.-)%' .. rightDelimiter)
end

local maxarange = 0

local function tabletostring(t)
  local s=""
  s = s .. "{"
  for k, v in pairs(t) do
    if type(k)=="string" then
      s=s.."['"..k.."']="
    else
      s=s.."["..k.."]="
    end
    if type(v)=="string" then
      s=s.."'"..v.."', "
    elseif type(v)=="number" then
      s=s..v..", "
    elseif type(v)=="table" then
      s=s..tabletostring(v)..", "
    elseif type(v)=="boolean" then
      if v==true then
        s=s.."T, "
      else
        s=s.."F, "
      end
    elseif type(v)=="nil" then
        s=s.."nil,"
    end
  end
  s = s .. "}"
  return s
end

local function equalRerectFills(f1,f2) --we know the arrays have the same dimensions
  local i
  if #f1~=#f2 then return false end
  for i=1,#f1 do
    if f1[i]~=f2[i] then return false end
  end
  return true
end

local function inttostring(num)
  return string.sub(num,1,string.find(num..".","%.")-1)
end

local level,levelAlt,levels

local function alternatives(range)
  if not range then
    range = 12
  end
  level,levelAlt,levels=1,0,{{}}
  local ymx, mod8, xyshift, p, ymxshift, smod8, ap, amod8, minymx, maxymx
  local shift,blockShift = 3000,-500
  local t=gp.pattern()
  local t0=gp.pattern()
  local tx=gp.pattern()
  fail=false
  while inputline do
    local nextinputline
    local pos1=string.find(inputline,'%-%-')
    local pos2=string.find(inputline,'%;')
    if pos2 and (not pos1 or pos1>pos2) then
      nextinputline=string.sub(inputline,pos2+1)
      inputline=string.sub(inputline,1,pos2-1)
    end
    if string.sub(inputline,1,15)=="(block_right_r)" then
      ymx=readXY()+24
      g.show(showHead.."(block_right_r) "..inttostring(shift-18-(ymx//2))..","..inttostring(shift-18+(ymx//2)))
      g.putcells(block,blockShift-18-(ymx//2),blockShift-18+(ymx//2))
      if not levels[level].aCache then
        levels[level].aCache=''
      end
      levels[level].aCache=levels[level].aCache..inputline.."\n"
    elseif string.sub(inputline,1,14)=="(block_left_r)" then
      ymx=readXY()+26
      g.show(showHead.."(block_left_r) "..inttostring(19-(ymx//2))..","..inttostring(19+(ymx//2)))
      g.putcells(block,blockShift+19-(ymx//2),blockShift+19+(ymx//2))
      if not levels[level].aCache then
        levels[level].aCache=''
      end
      levels[level].aCache=levels[level].aCache..inputline.."\n"
    elseif string.sub(inputline,1,8)=="(boatSW)" then
      ymx=readXY()
      g.show(showHead.."(boatSW) "..inttostring(blockShift-(ymx//2))..","..inttostring(blockShift+(ymx//2)))
      g.putcells(boatSW,blockShift-(ymx//2),blockShift+(ymx//2))
      if not levels[level].aCache then
        levels[level].aCache=''
      end
      levels[level].aCache=levels[level].aCache..inputline.."\n"
    elseif string.sub(inputline,1,2)=="(g" or string.find("oe",string.sub(inputline,1,1)) then
      if string.sub(inputline,1,2)=="(g" then
        ymx=readXY()+1
        mod8=string.sub(inputline,3,3)
      else
        local endPos=string.find(inputline,'% ')
        if endPos then
          nextinputline=string.sub(inputline,endPos+1,-1)
          inputline=string.sub(inputline,1,endPos-1)
        end
        ymx=string.sub(inputline,2)+1
        mod8=string.sub(inputline,1,1)=="e" and 0 or 1
      end
      xyshift=2*(ymx//4)
      levelAlt=0
      t.array=g.getcells(g.getrect())
      g.new("playLevels");g.setbase(2);g.setstep(15)
      g.putcells(t.array,0,0,1,0,0,1,"copy")
      --g.note("D")
      writeLog("level "..level.." inputline "..inputline)
      if level<2 then
        g.putcells(g.evolve(glider,(mod8+4*ymx)%8 ),shift+xyshift-ymx-ymxOffset,shift+xyshift)
        --g.note('test ymxOffset '..ymxOffset)
      end
      tx.array=g.evolve(t.array,8)
      if not equalRerectFills(t.array,tx.array) then
        if not fail then
          g.note("Not a p8 target "..ymxOffset)
        end
        fail=true
      end
      t0=(t+gpo.glider[(mod8+4*ymx)%8].t(shift+xyshift-ymx-ymxOffset,shift+xyshift))[16384]
      --g.note("B")
      maxymx,minymx=ymx,ymx
      for ymxshift=-range,range,fixedColors and 2 or 1 do
        local mask,m=0,1
        for i=0,7 do
          tx=(t+gpo.glider[(i+4*(ymx+ymxshift))%8].t(shift+xyshift-(ymx+ymxshift)-ymxOffset,shift+xyshift))[16384]
          if equalRerectFills(t0.array,tx.array) then
            mask = mask + m
            writeLog("level "..level.."ymxshift"..ymxshift.."mask"..mask)
          end
          m=m+m
        end
        if mask~=0 then
          if ymxshift<0 then
            if ymx+ymxshift<minymx then
              minymx = ymx+ymxshift
            end
          else
            if ymx+ymxshift>maxymx then
              maxymx = ymx+ymxshift
            end
          end
          ap=8
          if mask*16 == mask + 255*(mask%16) then
            ap=4
            mask=mask % 16
            if mask*4 == mask + 15*(mask % 4) then
              ap=2
              mask=mask % 4
              if mask == 3 then
                ap,mask=1,1
              end
            end
          end
          if ap>2 then
            for i=0,ap-1 do
              if mask%2>0 then
                if bothColors or ((ymx+ymxshift) % 2 == 1) then
                  levelAlt=1+levelAlt
                  levels[level][levelAlt]={i,ap,ymx+ymxshift-1}
                end
              end
              mask=mask//2
            end
          else
            if bothColors or ((ymx+ymxshift) % 2 == 1) then
              levelAlt=1+levelAlt
              levels[level][levelAlt]={mask-1,ap,ymx+ymxshift-1}
            end
          end
        end
      end
      g.show(showHead.."level "..level.." "..(maxymx-1)..","..(minymx-1)..","..maxarange)
      writeLog("level "..level.." "..(maxymx-1)..","..(minymx-1)..","..maxarange)
      if maxymx-minymx > maxarange then
        maxarange = maxymx-minymx
      end
      --g.note("C")
      g.new("playLevels");g.setbase(2);g.setstep(15)
      g.putcells(t.array,0,0,1,0,0,1,"copy")
      --g.note("D")
      g.putcells(g.evolve(glider,(mod8+4*ymx)%8 ),shift+xyshift-ymx-ymxOffset,shift+xyshift)
      if debug>0 then
        debug=debug-1
        g.setpos(shift+xyshift-ymx-ymxOffset+50,shift+xyshift+50)
        g.setmag(2)
        g.update()
        g.note('level '..level..' debug '..debug..' more')
      end
      g.step()
      if level==confDebugStart then
        confDebug=1+confDebug
        debug=1+debug
      end
      if confDebug>0 then
        g.fit()
        --g.setpos(-600,-600)
        --g.setmag(1)
        g.update()
        if confDebug>1 then
          g.note('conf debug')
        end
      end
      level = level+1
      levels[level]={}
    else
      if not levels[level].aCache then
        levels[level].aCache=''
      end
      levels[level].aCache=levels[level].aCache..inputline.."\n"
    end
    if nextinputline then
      inputline=nextinputline
    else
      inputline = l:read()
    end
    if fail then
      --inputline = nil
    end
  end
  levels[level]=nil
  g.fit()
  g.update()
  --g.note("End of recipe or fail")
end

local function toPatternSalvo()
  g.new("playLevels")
  local delta,shift=512,3000
  local t=gp.pattern()+oriPattern
  for i=1,#levels do
    g.show(showHead..i)
    shift = shift + delta
    local ymx = levels[i][1][3] + 1
    local xyshift = 2*(ymx//4)
    t=t+gpo.glider[(levels[i][1][1]+4*ymx)%8].t(shift+xyshift-ymx-ymxOffset,shift+xyshift)
  end
  g.putcells(t.array,0,0,1,0,0,1,"copy")
  g.save("c:\\Golly\\BlinkerLevels.mc",'mc',false)
end

local function trashDetection()
  g.new("playLevels")
  g.putcells(oriPattern.array,0,0,1,0,0,1,"copy")
  local shift = 3000
  local t=gp.pattern()
  local t0=gp.pattern()
  local tx=gp.pattern()
  local tt0=gp.pattern() -- with some trash
  local allTrash=gp.pattern()
  for i=1,#levels do
    g.show(showHead..i)
    t.array=g.getcells(g.getrect())
    local trashFails
    for j=1,#levels[i] do
      levels[i][j].trashFails=nil
      local ymx=levels[i][j][3]+1
      local xyshift=2*(ymx//4)
      local trashFails
      for k=levels[i][j][1],levels[i][j][1]+8-levels[i][j][2],levels[i][j][2] do
        t0=(t+gpo.glider[(k+4*ymx)%8].t(shift+xyshift-ymx-ymxOffset,shift+xyshift))[16384]
        tt0=(t+allTrash+gpo.glider[(k+4*ymx)%8].t(shift+xyshift-ymx-ymxOffset,shift+xyshift))[16384]
        g.new("playLevels");
        g.putcells(t0.array,0,0,1,0,0,1,"copy")
        g.putcells(allTrash.array,0,0,1,0,0,1,"or")
        g.putcells(tt0.array,0,0,1,0,0,1,"xor")
        local trashGliderCheck = tonumber(g.getpop())
        if trashGliderCheck ~= 0 then
          if not levels[i][j].trashFails then
            levels[i][j].trashFails={}
          end
          levels[i][j].trashFails[k+1]={}
          for l=1,i-1 do
            if levels[l].trashCells then
              tt0=(t+levels[l].trashCells+gpo.glider[(k+4*ymx)%8].t(shift+xyshift-ymx-ymxOffset,shift+xyshift))[16384]
              g.new("playLevels")
              g.putcells(t0.array,0,0,1,0,0,1,"copy")
              g.putcells(levels[l].trashCells.array,0,0,1,0,0,1,"or")
              g.putcells(tt0.array,0,0,1,0,0,1,"xor")
              trashGliderCheck = tonumber(g.getpop())
              if trashGliderCheck ~= 0 then
                levels[i][j].trashFails[k+1][#levels[i][j].trashFails[k+1]+1]=l
                writeLog(inttostring(i).." "..inttostring(j).." "..inttostring(k).." "..inttostring(l).." glider incompatible with trash")
              end
            end
          end
        else
          trashFails={}
        end
      end
      if not trashFails then
        for k=levels[i][j][1],levels[i][j][1]+8-8/levels[i][j][2],levels[i][j][2] do
          if not trashFails then
            trashFails = levels[i][j].trashFails[k+1]
          else
            local trashSet={}
            for m=1,#levels[i][j].trashFails[k+1] do
              trashSet[levels[i][j].trashFails[k+1][m]]=true
            end
            for m=#trashFails,1,-1 do
              if not trashSet[trashFails[m]] then
                trashFails[m]=trashFails[#trashFails]
                trashFails[#trashFails]=nil
              end
            end
          end
        end
      end
      g.new("playLevels")
      g.putcells(allTrash.array,0,0,1,0,0,1,"copy")
      for m=1,#trashFails do
        g.putcells(levels[trashFails[m]].trashCells.array,0,0,1,0,0,1,"or")
        g.putcells(levels[trashFails[m]].trashCells.array,0,0,1,0,0,1,"xor")
        levels[trashFails[m]].trashCells=false
        writeLog(inttostring(i).." all variants incompatible with trash on level "..inttostring(trashFails[m]))
      end
      allTrash.array=g.getcells(g.getrect())
    end
    g.new("playLevels")
    g.putcells(t.array,0,0,1,0,0,1,"copy")
    g.putcells(t0.array,0,0,1,0,0,1,"or")
    g.putcells(t.array,0,0,1,0,0,1,"xor")
    local trashGliderCheck = tonumber(g.getpop())
    writeLog(inttostring(i).." trashGliderCheck: "..inttostring(trashGliderCheck))
    if trashGliderCheck == 0 then
      writeLog(inttostring(i).." trash destroying level detected")
      g.putcells(t.array,0,0,1,0,0,1,"copy")
      --g.putcells(t0.array,0,0,1,0,0,1,"or")
      g.putcells(t0.array,0,0,1,0,0,1,"xor")
      if levels[i].trashCells==nil then
        levels[i].trashCells = {}
        levels[i].trashCells.array=g.getcells(g.getrect())
      end
      if levels[i].trashCells ~= false then
        allTrash = allTrash + levels[i].trashCells
      else
        writeLog(inttostring(i).." trash destroying level failed in previous run")
      end
    end
    g.new("playLevels");
    g.putcells(t0.array,0,0,1,0,0,1,"copy")
  end
end

local function outputlevels(fileName,curt)
  a = io.open(workDir..fileName,"w")
  for i=1,#levels do
    if levels[i].aCache then
      aCache = levels[i].aCache
    end
    local failTrash=''
    for j=1,#levels[i] do
      aCache=aCache.."(g"..inttostring(levels[i][j][1]).."%"..inttostring(levels[i][j][2])..")["..inttostring(levels[i][j][3]).."]"
      if levels[i][j].trashFails then
        failTrash = failTrash..inttostring(j)..">"
        for k=levels[i][j][1],levels[i][j][1]+8-levels[i][j][2],levels[i][j][2] do
          if #levels[i][j].trashFails[k+1]>0 then
            failTrash = failTrash..inttostring(k)..":("
            for l=1,#levels[i][j].trashFails[k+1] do
              failTrash = failTrash .. inttostring(levels[i][j].trashFails[k+1][l])..","
            end
            failTrash = string.sub(failTrash,1,-2)..")"
          end
        end
      end
    end
    if not curt and (levels[i].trashCells~=nil or failTrash~='') or levels[i].trashCells then
      aCache=aCache.." -- "..inttostring(i)..' '
    end
    if levels[i].trashCells then
      for j=1,#levels[i].trashCells.array do
        aCache = aCache .. inttostring(levels[i].trashCells.array[j])..","
      end
      aCache=string.sub(aCache,1,-2)
    elseif not curt and levels[i].trashCells ~= nil then
      aCache=aCache.." incompatible with later levels"
    end
    if not curt then
      aCache=aCache..' '..failTrash
    end
    g.show(showHead..level.." "..aCache)
    writeACache()
  end
  a:close()
end

local function markTrash(file)
  local t,r,s=gp.pattern(),gp.pattern(),gp.pattern()
 -- g.join(r.array,{0})
  t.array=g.getcells(g.getrect())
  g.new("playLevels")
  g.setrule("LifeHistory")
  local tr = (upsideDown and gp.flip_y) or gp.identity
  for i=1,#levels do
    if levels[i].trashCells then
      local text = gpt.maketext(inttostring(i),"mono").t(levels[i].trashCells.array[1],levels[i].trashCells.array[2]-10,tr)
      r=r+text.state(4)
      for j=1,#levels[i].trashCells.array,2 do
        s=s+gp.pattern([["*"]],levels[i].trashCells.array[j],levels[i].trashCells.array[j+1]).state(5)
      end
    end
  end
  r=r+s+t
  r.display("")
  g.save(workDir..file,'mc',false)
end

local start=gp.pattern()
start.array=g.getcells(g.getrect())
--for y=6,6 do
--  g.new("playLevels");
--  g.putcells(start.array,0,0,1,0,0,1,"copy")
--  ymxOffset=y
--  g.show(y)
local function oneFile(file)
  g.new("playLevels");
  g.setrule("Life")
  g.putcells(start.array,0,0,1,0,0,1,"copy")
  if not file then
     file = ""
  end
  showHead = file..": "
  l = io.open(workDir.."BlinkerLevels"..file..".txt","r")
  if l then
    inputline = l:read()
    alternatives()
    --alternatives(0)
    toPatternSalvo()
    if not fail then
      outputlevels("BlinkerLevels"..file.."A"..ymxOffset..".txt")
    end
    l:close()
  end
end

local function fleet(direction)
  workDir = "c:\\Golly\\rct16Local\\15_ECCACleanup\\"..direction.."Fleet\\"
  for i=1,39 do
  --  oneFile(i)
  end
  --oneFile(17)
  oneFile(18)
  --oneFile(23)
end

local function patterntries()
  workDir = "c:\\Golly\\rct16Local\\16_PatternSeed\\"
  local tries="AEMQUWY"
  for i=1,string.len(tries) do
    local try=string.sub(tries,i,i)
    oneFile(try)
    trashDetection()
    outputlevels("BlinkerLevels"..try.."B.txt", false)
    trashDetection()
    outputlevels("BlinkerLevels"..try.."C.txt",true)
    markTrash("BlinkerLevels"..try.."T.mc")
  end
end

local direction = "SW"
--fleet(direction)
patterntries()

if log then
  log:close()
end
--g.note("maxARange="..maxarange)