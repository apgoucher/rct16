local g = golly() -- for debug messages
local log = io.open("c:\\Golly\\LevelsToDBCABitslog.txt", "w")
local etab = io.open("c:\\Golly\\LevelsToDBCABitsEvenLines.txt", "w")
local logLevel=0

local d,dCache,delta
local fires={{"o","1"},{"e","0"},{"o","0"},{"e","1"},{"o","1"},{"e","0"},{"o","0"},{"e","1"}}  -- {firecolor*1+firephase*2+phaseSwitchReady*4}
-- fc  changes color (and fires):        semiState ~= 1
-- Rcsp changes color and semichanges phase:        semiState ~= (((semiState%8)//2)|5) --5,5,5,5,7,7,7,7
-- white requires dummy fire semiState &8 denotes the fire was not used yet
--                           semiState &16 denotes white is ready for fake fire (we know it is ready for line -1233 in recipe and later, we know there is no odd line before 0)

local function writeLog(msg, level)
  local time = g.millisecs()
  if log and ((not level and logLevel <= 0) or (level and level >= logLevel)) then
    log:write(msg .. "\n")
  end
end

local function inttostring(num)
  return string.sub(num, 1, string.find(num .. ".", "%.") - 1)
end

local function writeDCache()
  --I want all rows except last exactly 80 characters to easily compare the 12 lengths ... I write the bilLen to the end note anyways
  local dExtra=""
  if string.len(dCache)>80 then
    local poscomm = string.find(dCache,'%-%-')
    local poseol = string.find(dCache,'%\n')
    if poscomm then
      --g.note('A')
      if poscomm>80 then
        --g.note('AA')
        if poseol and poseol<80 then
          --g.note('AAA')
          dExtra=string.sub(dCache,poseol+1)
          dCache=string.sub(dCache,1,poseol-1)
          if string.len(dCache)>0 and d then
            --g.note('AAAA')
            --bitLen=bitLen+string.len(dCache)
            d:write(dCache.."\n")
          end
          dCache=dExtra
          return
        end
        --g.note('AA2')
        dExtra=string.sub(dCache,81)
        dCache=string.sub(dCache,1,80)
        if string.len(dCache)>0 and d then
          --g.note('AA2A')
          --bitLen=bitLen+string.len(dCache)
          d:write(dCache.."\n")
        end
        --g.note('AA3')
        dCache=dExtra
        return
      else
        --g.note('AB')
        if poseol and poseol<81 then
          --g.note('ABA')
          dExtra=string.sub(dCache,poseol+1)
          dCache=string.sub(dCache,1,poseol-1)
          if string.len(dCache)>0 and d then
            --g.note('ABAA')
            --bitLen=bitLen+string.len(dCache)
            d:write(dCache.."\n")
          end
          --g.note('ABA2')
          dCache=dExtra
          return
        else
          --g.note('ABB')
          if poscomm>1 then
            --g.note('ABBA')
            dExtra=string.sub(dCache,poscomm)
            dCache=string.sub(dCache,1,poscomm-1)
            if string.len(dCache)>0 and d then
              --g.note('ABBAA')
              --bitLen=bitLen+string.len(dCache)
              d:write(dCache.."\n")
            end
            --g.note('ABBA2')
            dCache=dExtra
            return
          else
            --g.note('ABBB')
            if poseol then
              --g.note('ABBBA')
              dExtra=string.sub(dCache,poseol+1)
              dCache=string.sub(dCache,1,poseol-1)
              if string.len(dCache)>0 and d then
                --g.note('ABBBAA')
                --bitLen=bitLen+string.len(dCache)
                d:write(dCache.."\n")
              end
              --g.note('ABBBA2')
              dCache=dExtra
              return
            else
              --g.note('ABBBB')
              if string.len(dCache)>0 and d then
                --g.note('ABBBBA')
                --bitLen=bitLen+string.len(dCache)
                d:write(dCache.."\n")
              end
              --g.note('ABBBB2')
              dCache=''
              return
            end
          end
        end
      end
    end
    if poseol and poseol<80 then
      --g.note('2A')
      dExtra=string.sub(dCache,poseol+1)
      dCache=string.sub(dCache,1,poseol-1)
      if string.len(dCache)>0 and d then
        --g.note('2AA')
        --bitLen=bitLen+string.len(dCache)
        d:write(dCache.."\n")
      end
      --g.note('2A2')
      dCache=dExtra
      return
    end
    dExtra=string.sub(dCache,81)
    dCache=string.sub(dCache,1,80)
  end
  if string.len(dCache)>0 and d then
    --bitLen=bitLen+string.len(dCache)
    d:write(dCache.."\n")
  end
  dCache=dExtra
end

local function boolean2Bit(b)
  return b and "1" or "2"
end

local function boolMove(b,dist)
  dCache = dCache..boolean2Bit(b)
  if b and dist then delta = delta-dist end
  --codon virtually (+1)
end

local function move_d3_i4()
  do boolMove(((delta%4)>0) or (delta<0),-3) end
  do boolMove(((delta%3)>0) or (delta>0),4) end
end

--[[
local function process_command_fcp_Rcsp(command,moves)
  local color,phase,firematch,phaseexact,semiState
  delta=tonumber(string.sub(command,1,-3)) or 0
  color=string.sub(command,-2,-2)
  phase=string.sub(command,-1,-1)
  -- we are after fire (and the following reset)
  repeat
    --g.note(dCache.." A "..command.." "..delta..","..color..","..phase..","..semiState..boolean2Bit(phaseknown==nil))
    do moves() end
    phaseexact = phase == fires[semiState+1][2]
    firematch = (delta==0) and (color == fires[semiState+1][1])
                and (phaseexact or phase=="2" or (startphase==nil and (semiState<4)) or (startsemiphaseswitch==nil and (semiState>3)))
    do boolMove(firematch) end
    semiState = semiState ~ ((semiState//2)|5) --#cps
  until firematch
  semiState = semiState ~ 3 -- fcp
end
--]]

local dirCommonPrefix = "c:\\Golly\\"

local function inDelimiters(name, leftDelimiter, rightDelimiter)
  return string.match(name, '%' .. leftDelimiter .. '(.-)%' .. rightDelimiter)
end

local levels={} -- {{phase,periodicity,line}}
local comLines={} -- {lines}
-- if signFlip all ne numbers are considered negated
-- colorFlip all lines are increased by 1 (after possible negation)
local function readLevels(signFlip, lineShift, infile, outfile, finalComment, lastphase)
  infile = infile or dirCommonPrefix .. "DBCALevels.txt"
  outfile = outfile or dirCommonPrefix .. "DBCALevelsA.txt"
  local lastphasedone=False
  local levelFile = io.open(infile, "r")
  local levetoutput = io.open(outfile, "w")
  local inputLine = levelFile:read()
  local outputlineStart, outputlineEnd
  lineShift = lineShift or 0
  local l = 0
  g.show(inputLine)
  local commentLines = ''
  while inputLine do
    outputlineStart, outputlineEnd = '', ''
    local pos = string.find(inputLine, "%-%-")
    if pos then
      outputlineEnd = string.sub(inputLine, pos, -1)
      inputLine = string.sub(inputLine, 1, pos - 1)
      --      g.note("outputLineEnd '"..outputlineEnd.."' startOfiInputLine "..inputLine)
    end
    pos = string.find(inputLine, "%(g")
    if pos then
      l = l + 1
      levels[l] = {}
      comLines[l], commentLines = commentLines, ''
      if comLines[l] ~= '' then
        --g.note('comLines['..l..']='..comLines[l])
      end
      while pos do
        --g.note('A')
        local gp, gm, gl = 0 + string.sub(inputLine, pos + 2, pos + 2), 0 + string.sub(inputLine, pos + 4, pos + 4), 0 + inDelimiters(inputLine, '[', ']')
        gl = gl * ((signFlip and -1) or 1) + lineShift
        if lastphasedone then
          g.note("Something wrong 1, lastphase expects just last mod>2")
        end
        if gm > 2 and lastphase then
          outputlineStart = outputlineStart .. "(g" .. lastphase.. ")[" .. gl .. "]"
          lastphasedone = true
        else
          outputlineStart = outputlineStart .. "(g" .. gp .. "%" .. gm .. ")[" .. gl .. "]"
        end
        if gm>2 then
          g.show('%' .. gm .. "? converted to %2!")
          gp, gm = gp % 2, 2
        end
        levels[l][1 + #levels[l]] = { gp, gm, gl }
        pos = string.find(inputLine, "%]")
        inputLine = string.sub(inputLine, pos + 1)
        pos = string.find(inputLine, "%(g")
        g.show("levels[" .. l .. "][" .. #levels[l] .. "]={" .. levels[l][#levels[l]][1] .. "," .. levels[l][#levels[l]][2] .. "," .. levels[l][#levels[l]][3] .. "}" .. inputLine .. (pos or "nil"))
      end
      --g.note('B')
    elseif outputlineEnd ~= '' then
      commentLines = commentLines .. outputlineEnd .. '\n'
      if string.find(commentLines, 'EvenLinesOption=') then
        l = l + 1
        levels[l] = {}
        comLines[l], commentLines = commentLines, ''
      end
      --g.note('commentLines '..commentLines)
    end
    levetoutput:write(outputlineStart .. outputlineEnd .. "\n")
    inputLine = levelFile:read()
    g.show(inputLine or "nil")
  end
  comLines[l + 1], commentLines = commentLines, ''
  --g.note(l.." levels read")
  if lastphase and not lastphasedone then
    g.note("Something wrong 2, lastphase expects just last mod>2")
  end
  if finalComment then
    levetoutput:write(finalComment .. "\n")
  end
end

local function formatLevelBits(levelBits)
  local res=''
  local prevTriple,prevTripleCnt,triple='X',0
  while levelBits~="" do
    triple,levelBits=string.sub(levelBits,1,3),string.sub(levelBits,4)
    if triple==prevTriple then
      prevTripleCnt = prevTripleCnt + 1
    else
      if prevTripleCnt>1 then
        res = res .. '*' .. prevTripleCnt
      end
      res = res .. "+'"..triple.."'"
      prevTriple,prevTripleCnt = triple,1
    end
  end
  if prevTripleCnt>1 then
    res = res .. '*' .. prevTripleCnt
  end
  return string.sub(res,2)
end
-- unless lastFireLine is specified we expect the position of the arm block correspond to firing on first level
-- (when there are more options, each starts with optimally placed block).
local function levels2Bits(startSemiState, lastFireLine, commentOffset,evenLinesOption)
-- prelevelState semiState.."_"..blockPosition->{semiState,blockPosition,levelBits,countOfBits,prevLevelIndex}
-- blockPosition before the fireDec
  local prevLevelStates={}
  local evenLinesImportant = evenLinesOption ~= nil
  local evenLinesFilter = tonumber(evenLinesOption) -- could be nil if we just need a statistic
  local midFireLine
  local oddOffs=9
  prevLevelStates[1]={}
  local thisLevelStates={}
  for i=1,#levels[1] do
    local line,blockPos=levels[1][i][3]
    if lastFireLine then
      line=lastFireLine
    end
    local numEvenLines=((line+1)%2)
    blockPos = (line + (line % 2)*oddOffs)/2
    local DP_State=startSemiState.."_"..blockPos
    if evenLinesImportant then
      DP_State = DP_State.."_"..numEvenLines
    end
    if not thisLevelStates[DP_State] then
      thisLevelStates[DP_State]=1+#prevLevelStates[1]
    end
    prevLevelStates[1][thisLevelStates[DP_State]]={startSemiState,blockPos,"",0,0,0,numEvenLines}
    writeLog("StartState and Pos and numEvenLines "..startSemiState.." "..inttostring(blockPos).." "..numEvenLines)
    if lastFireLine then
      break
    end
  end
  writeLog("#Levels " .. #levels)
  for l=1,#levels do
    thisLevelStates={}
    prevLevelStates[l+1]={}
    --writeLog("Level " .. l .. "#levels[l] " .. #levels[l])
    if #levels[l]>0 then
      for i=1,#levels[l] do
        local line,phase=levels[l][i][3],(levels[l][i][2]==1) and "2" or levels[l][i][1]..""
        local colorInd=(line % 2)
        local color = string.sub("oe",1+colorInd,1+colorInd)
        local blockPos = (line + colorInd * oddOffs)/2
        writeLog("Option " .. i .. " line " .. line .. " phase " .. phase .. " pos " .. inttostring(blockPos) .. " #prevLevelStates[l] " .. #prevLevelStates[l])
        for j=1,#prevLevelStates[l] do
          local semiState = prevLevelStates[l][j][1]
          for Wfake = (color=="e" or semiState<8) and 0 or 1,(semiState//8 == 1) and 1 or 0 do
            delta = blockPos - prevLevelStates[l][j][2] + 1 -- +1 to compensate after fire decrement
            semiState = prevLevelStates[l][j][1]
            if line==-1233 and semiState>=16 then
              semiState = semiState - 16 -- White color (odd lines ... e) is ready
            end
            dCache=""
            repeat
              --writeLog("Level " .. l .. " option " .. i .. " Wfake "..Wfake .. " prev " .. j
              --        .. " pos " .. inttostring(blockPos) .. " prevpos " .. inttostring(prevLevelStates[l][j][2]) .. " delta " .. inttostring(delta) .. " line " .. line
              --        .. " color " .. colorInd .. " phase " .. phase .. " semiState " .. semiState .. " dCache " ..dCache)
              move_d3_i4()
              local firematch = (delta==0) and (color == fires[(semiState%8)+1][1]) and (phase=="2" or phase == fires[(semiState%8)+1][2]) and (semiState<8 or (color=="e" and Wfake == 0))
              local Wfakematch = (semiState // 8 == 1) and fires[(semiState%8)+1][1] == "o" and Wfake == 1
              if Wfakematch then
                boolMove(true)
                firematch = false
                semiState = semiState ~ 9 -- fake+c ... it occurs before #cps, but the changes comute
                delta = delta + 1 -- to compensate the dec (of the fake fire)
              else
                boolMove(firematch)
              end
              semiState = semiState ~ (((semiState%8)//2)|5) --#cps
            until firematch
            semiState = semiState ~ 1 -- c ... it occurs before #cps, but the changes comute
            local DP_State=semiState.."_"..blockPos
            local numEvenLines = prevLevelStates[l][j][7]+(1-colorInd)
            if evenLinesImportant then
              DP_State = DP_State.."_"..numEvenLines
            end
            writeLog(formatLevelBits(dCache))
            local cost=string.len(dCache)+prevLevelStates[l][j][4]
            writeLog("Level " .. l .. " option " .. i .. " Wfake "..Wfake .. " prev " .. j
                    .. " pos " .. inttostring(blockPos) .. " prevpos " .. inttostring(prevLevelStates[l][j][2]) .. " delta " .. inttostring(delta) .. " line " .. line
                    .. " color " .. colorInd .. " phase " .. phase .. " semiState " .. semiState .. " cost " .. cost.." numEvenLines "..numEvenLines)
            if thisLevelStates[DP_State] then
              if prevLevelStates[l+1][thisLevelStates[DP_State]][4]>cost then
                prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,dCache,cost,j,Wfake,numEvenLines}
              end
            else
              thisLevelStates[DP_State]=1+#prevLevelStates[l+1]
              prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,dCache,cost,j,Wfake,numEvenLines}
            end
          end
        end
      end
    else
      evenLinesImportant = true
      local pos=string.find(comLines[l],'EvenLinesOption=')
      evenLinesFilter = 0+inDelimiters(string.sub(comLines[l],pos),'=',',')
      pos=string.find(comLines[l],'LastFireLine=',pos)
      midFireLine = 0+inDelimiters(string.sub(comLines[l],pos),'=',' ')
      local blockPos = (midFireLine + (midFireLine % 2)*oddOffs)/2
      for j=1,#prevLevelStates[l] do
        local semiState = prevLevelStates[l][j][1]
        local DP_State=semiState.."_"..blockPos.."_0"
        local cost=prevLevelStates[l][j][4]
        if thisLevelStates[DP_State] then
          if prevLevelStates[l+1][thisLevelStates[DP_State]][4]>cost then
            prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,'',cost,j,0,0}
          end
        else
          thisLevelStates[DP_State]=1+#prevLevelStates[l+1]
          prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,'',cost,j,0,0}
        end
      end
    end
  end
  local nameSuffix=startSemiState..
          ((lastFireLine and "_"..lastFireLine) or "")..
          ((midFireLine and "_"..midFireLine) or "")..
          ((evenLinesFilter and "e"..evenLinesFilter) or "")..".txt"
  d = io.open(dirCommonPrefix.."DBCAbits_"..nameSuffix,"w")
  local r = io.open(dirCommonPrefix.."DBCAbitsR_"..nameSuffix,"w")
  local state=prevLevelStates[#levels+1][1]
  local bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines,numEvenLines=
    state[4],state[3],state[5],state[6],state[1],state[2],state[7],state[7]
  local bestCostEvenLines,evenLinesSet={},{}
  if evenLinesImportant then
    evenLinesSet[1]=numEvenLines
    bestCostEvenLines[numEvenLines]={bestCost,semiState}
  end
  for j=2,#prevLevelStates[#levels+1] do
    state=prevLevelStates[#levels+1][j]
    if evenLinesImportant then
      local numEvenLines=state[7]
      if bestCostEvenLines[numEvenLines] then
        if bestCostEvenLines[numEvenLines][1]>state[4] then
          bestCostEvenLines[numEvenLines]={state[4],state[1]}
          if evenLinesFilter and numEvenLines == evenLinesFilter then
            bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines=
              state[4],state[3],state[5],state[6],state[1],state[2],state[7]
          end
        end
      else
        evenLinesSet[1+#evenLinesSet]=numEvenLines
        bestCostEvenLines[numEvenLines]={state[4],state[1]}
        if evenLinesFilter and numEvenLines == evenLinesFilter then
          bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines=
            state[4],state[3],state[5],state[6],state[1],state[2],state[7]
        end
      end
    end
    if not evenLinesFilter then
      if bestCost>state[4] then
        bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines=
          state[4],state[3],state[5],state[6],state[1],state[2],state[7]
      end
    end
  end
  if evenLinesImportant then
    etab:write("Even Lines table for startSemiState "..startSemiState.."\n")
    etab:write("Number of leves: "..#levels.."\n")
    for i=1,#evenLinesSet do
      numEvenLines = evenLinesSet[i]
      etab:write(numEvenLines.." "..bestCostEvenLines[numEvenLines][1].." ("..bestCostEvenLines[numEvenLines][2]..")".."\n")
    end
  end
  local info = (#levels+commentOffset) .. " pos " .. inttostring(blockPos) .. " semistate ".. semiState .. " evenLines " .. bestELines ..
          " (g"..(((semiState%8)//4+((semiState%4)//2)+semiState)%2).."%2)["..inttostring(2*blockPos-(semiState%2)*oddOffs).."]\n"
  r:write("    BRecipe=" .. formatLevelBits(bestDCache) .. " # " .. info)
  bestDCache = bestDCache .. "-- " .. info
  if comLines[#levels+1] and comLines[#levels+1]~='' then
    bestDCache = bestDCache .. "\n" .. comLines[#levels+1]
  end
  for l=#levels,2,-1 do
    local levelBits,semiState,blockPos,eLines=
      prevLevelStates[l][bestPrevInd][3],
      prevLevelStates[l][bestPrevInd][1],
      prevLevelStates[l][bestPrevInd][2],
      prevLevelStates[l][bestPrevInd][7]
    local o_m_sqrt_g_p=((semiState%8)//2)%3
    info = (l+commentOffset-1) .. " pos " .. inttostring(prevLevelStates[l][bestPrevInd][2]) ..
            " semistate ".. semiState .. " evenLines " .. eLines ..
            " (g"..(((semiState%8)//4+((semiState%4)//2)+semiState)%2).."%2)["..inttostring(2*blockPos-(semiState%2)*oddOffs).."]\n"
    r:write("    BRecipe=" .. formatLevelBits(levelBits) .. " + BRecipe # " .. info)
    if comLines[l]~='' then
      bestDCache = comLines[l]..bestDCache
    end
    bestDCache,bestPrevInd=levelBits .. "-- " .. info .. bestDCache,prevLevelStates[l][bestPrevInd][5]
  end
  if comLines[1]~='' then
    bestDCache = comLines[1]..bestDCache
  end
  r:write("    recipe+=BRecipe\n")
  dCache=bestDCache
  while dCache~="" do
    g.show(dCache)
    writeDCache()
  end
  d:close()
end

local function fleetSE()
  readLevels(false,300,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels1A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c1.txt","----- (0,0), 8, 'The ECCA launched 1th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,317,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels2A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c2.txt","----- (0,0), 8, 'The ECCA launched 2th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,329,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels3A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c3.txt","----- (0,0), 8, 'The ECCA launched 3th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,335,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels4A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c4.txt","----- (0,0), 8, 'The ECCA launched 4th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,338,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels5A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c5.txt","----- (0,0), 8, 'The ECCA launched 5th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,349,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels6A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c6.txt","----- (0,0), 8, 'The ECCA launched 6th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,355,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels7A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c7.txt","----- (0,0), 8, 'The ECCA launched 7th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,332,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels8A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c8.txt","----- (0,0), 8, 'The ECCA launched 8th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,342,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels9A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c9.txt","----- (0,0), 8, 'The ECCA launched 9th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,306,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels10A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c10.txt","----- (0,0), 8, 'The ECCA launched 10th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,312,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels11A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c11.txt","----- (0,0), 8, 'The ECCA launched 11th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,323,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels12A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c12.txt","----- (0,0), 8, 'The ECCA launched 12th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,332,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels13A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c13.txt","----- (0,0), 8, 'The ECCA launched 13th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,336,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels14A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c14.txt","----- (0,0), 8, 'The ECCA launched 14th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,351,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels15A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c15.txt","----- (0,0), 8, 'The ECCA launched 15th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,320,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels16A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c16.txt","----- (0,0), 8, 'The ECCA launched 16th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,306,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels17A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c17.txt","----- (0,0), 8, 'The ECCA launched 17th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,325,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels18A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c18.txt","----- (0,0), 8, 'The ECCA launched 18th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,302,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels19A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c19.txt","----- (0,0), 8, 'The ECCA launched 19th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,318,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels20A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c20.txt","----- (0,0), 8, 'The ECCA launched 20th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,304,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels21A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c21.txt","----- (0,0), 8, 'The ECCA launched 21th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,315,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels22A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c22.txt","----- (0,0), 8, 'The ECCA launched 22th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,327,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels23A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c23.txt","----- (0,0), 8, 'The ECCA launched 23th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,331,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels24A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c24.txt","----- (0,0), 8, 'The ECCA launched 24th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,337,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels25A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c25.txt","----- (0,0), 8, 'The ECCA launched 25th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,344,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels26A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c26.txt","----- (0,0), 8, 'The ECCA launched 26th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,348,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels27A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c27.txt","----- (0,0), 8, 'The ECCA launched 27th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,362,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels28A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c28.txt","----- (0,0), 8, 'The ECCA launched 28th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,379,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels29A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c29.txt","----- (0,0), 8, 'The ECCA launched 29th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,389,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels30A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c30.txt","----- (0,0), 8, 'The ECCA launched 30th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,395,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels31A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c31.txt","----- (0,0), 8, 'The ECCA launched 31th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,392,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels32A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c32.txt","----- (0,0), 8, 'The ECCA launched 32th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,404,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels33A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c33.txt","----- (0,0), 8, 'The ECCA launched 33th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,380,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels34A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c34.txt","----- (0,0), 8, 'The ECCA launched 34th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,392,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels35A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c35.txt","----- (0,0), 8, 'The ECCA launched 35th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,402,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels36A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c36.txt","----- (0,0), 8, 'The ECCA launched 36th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,409,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels37A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c37.txt","----- (0,0), 8, 'The ECCA launched 37th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,413,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels38A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c38.txt","----- (0,0), 8, 'The ECCA launched 38th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,418,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels39A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c39.txt","----- (0,0), 8, 'The ECCA launched 39th Cordership to clean the southeast trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,421,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels40A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\ECCALevels_15c40.txt","----- (0,0), 8, 'Temporary hook before program for SW corderfleet is ready', note='debug?', setmag=-2, priority=2 -----")
end

local function fleetNW()
  readLevels(false,421,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels1A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f1.txt","----- (0,0), 8, 'The ECCA launched 1st Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,427,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels2A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f2.txt","----- (0,0), 8, 'The ECCA launched 2nd Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,430,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels3A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f3.txt","----- (0,0), 8, 'The ECCA launched 3rd Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,448,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels4A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f4.txt","----- (0,0), 8, 'The ECCA launched 4th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,474,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels5A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f5.txt","----- (0,0), 8, 'The ECCA launched 5th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,506,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels6A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f6.txt","----- (0,0), 8, 'The ECCA launched 6th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,497,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels7A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f7.txt","----- (0,0), 8, 'The ECCA launched 7th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,492,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels8A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f8.txt","----- (0,0), 8, 'The ECCA launched 8th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,478,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels9A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f9.txt","----- (0,0), 8, 'The ECCA launched 9th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,469,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels10A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f10.txt","----- (0,0), 8, 'The ECCA launched 10th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,480,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels11A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f11.txt","----- (0,0), 8, 'The ECCA launched 11th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,481,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels12A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f12.txt","----- (0,0), 8, 'The ECCA launched 12th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,480,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels13A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f13.txt","----- (0,0), 8, 'The ECCA launched 13th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,490,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels14A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f14.txt","----- (0,0), 8, 'The ECCA launched 14th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,491,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels15A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f15.txt","----- (0,0), 8, 'The ECCA launched 15th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,477,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels16A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f16.txt","----- (0,0), 8, 'The ECCA launched 16th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,444,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels17A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f17.txt","----- (0,0), 8, 'The ECCA launched 17th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,441,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels18A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f18.txt","----- (0,0), 8, 'The ECCA launched 18th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,424,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels19A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f19.txt","----- (0,0), 8, 'The ECCA launched 19th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(false,405,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels20A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f20.txt","----- (0,0), 8, 'The ECCA launched 20th Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(false,408,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels21A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f21.txt","----- (0,0), 8, 'The ECCA launched 21st Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(false,401,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels22A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f22.txt","----- (0,0), 8, 'The ECCA launched 22nd Cordership to clean the northwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
end

local function fleetSW()
  readLevels(true,216,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels1A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e1.txt","----- (0,0), 8, 'The ECCA launched 1th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,158,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels2A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e2.txt","----- (0,0), 8, 'The ECCA launched 2th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,176,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels3A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e3.txt","----- (0,0), 8, 'The ECCA launched 3th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,176,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels4A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e4.txt","----- (0,0), 8, 'The ECCA launched 4th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(true,202,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels5A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e5.txt","----- (0,0), 8, 'The ECCA launched 5th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(true,194,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels6A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e6.txt","----- (0,0), 8, 'The ECCA launched 6th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(true,213,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels7A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e7.txt","----- (0,0), 8, 'The ECCA launched 7th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(true,221,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels8A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e8.txt","----- (0,0), 8, 'The ECCA launched 8th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,242,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels9A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e9.txt","----- (0,0), 8, 'The ECCA launched 9th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,254,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels10A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e10.txt","----- (0,0), 8, 'The ECCA launched 10th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,279,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels11A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e11.txt","----- (0,0), 8, 'The ECCA launched 11th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,281,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels12A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e12.txt","----- (0,0), 8, 'The ECCA launched 12th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,293,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels13A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e13.txt","----- (0,0), 8, 'The ECCA launched 13th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(true,314,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels14A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e14.txt","----- (0,0), 8, 'The ECCA launched 14th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","1%2")
  readLevels(true,315,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels15A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e15.txt","----- (0,0), 8, 'The ECCA launched 15th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%2")
  readLevels(true,326,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels16A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e16.txt","----- (0,0), 8, 'The ECCA launched 16th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")
  readLevels(true,333,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels17A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e17.txt","----- (0,0), 8, 'The ECCA launched 17th Cordership to clean the southwest trail', no te='debug?', setmag=-2, priority=2 -----","0%1")

  readLevels(false,436,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\BlinkerLevels18A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\ECCALevels_15e18.txt","----- (0,0), 8, 'The ECCA launched 18th Cordership to clean the southwest trail', note='debug?', setmag=-2, priority=2 -----","0%1")
end

local case=2
if (case==-2) then --DBCA after hand white leak portion
  readLevels() -- have to be true, true
  levels2Bits(25,-1645, 0) -- have to be (1+8+16,-413,20) and 16, 8 logic changed to other color
elseif (case==1) then --flipping level not required for DBCA ...
  readLevels(true)
elseif (case==2) then -- flip with shift
  --readLevels(true,211-3)--53+146)-- 98-3) ---1250-2480)---1347-2508)
  --readLevels(false,  469-6)-- 270) --234+353)--455)--53-142)--211-6)---1611-273)--435)-- -980) --428) --392+353)--16+1516)--396+593) --507-114) --318)-- 78-241) --  -269+160) --79-196)--297-6) --256) -- 351)-- -428+160)--79-196)
  --readLevels(true,461,"c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\new2\\sol10\\BlinkerLevelsA0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\SWFleet\\new2\\sol10\\ECCALevels_15d.txt","----- (0,0), 8, 'FSW Corderabsorbers creating salvo sent, Corderfleet SW would follow', no te='debug?', setmag=-2, priority=0 -----\n----- (-1,1), 12, 'FSW Corderabsorbers finished', no te='debug?', setmag=-2, priority=1 -----")
  --readLevels(false,466+178-6,"c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\BlinkerLevels23A0.txt","c:\\Golly\\rct16Local\\15_ECCACleanup\\NWFleet\\ECCALevels_15f23.txt","----- (0,0), 8, 'The ECCA launched 23rd Cordership to clean the northwest trail', note='debug?', setmag=-2, priority=2 -----","0%1")
  --readLevels(true,208,"c:\\Golly\\rct16Local\\12_FSECorderabsorbers\\BlinkerLevelsA0.txt","c:\\Golly\\rct16Local\\12_FSECorderabsorbers\\DBCALevels_12_FSECorderabsorbers.txt","-- last glider must be on an even line and odd phase to force prepared state for DBCA destroyal.\n----- (0,0), 0, 'Corderabsorbers program sent, N gliders absorber is away so stopping the DBCA', bit_offs=-1, no te='debug DBCA stop', priority=0 -----\n----- (1,1), 4, 'FSE corderabsorbers ready', no te='debug FSE corderabsorbers ready', priority=1 -----")
  --466+168 ... beehive2block +10 1st line +9 2nd line
  local patternShift=466+25+3
  readLevels(false, patternShift,"c:\\Golly\\rct16Local\\16_PatternSeed\\BlinkerLevelsYC.txt","c:\\Golly\\rct16Local\\16_PatternSeed\\ECCALevels_16.txt","----- (0,0), 8, '(empty) Pattern seed finished, ECCA arm block destroyal would follow', no te='debug?', setmag=-2, priority=0 -----")
  readLevels(false, patternShift-32,"c:\\Golly\\rct16Local\\17_SwitchFNW\\DBCALevels_17.txt","c:\\Golly\\rct16Local\\17_SwitchFNW\\ECCALevels_17.txt","----- (0,0), 8, 'ECCA near arm block destroyed, FNW corderabsorbers creating savlo would follow', note='debug?', setmag=-2, priority=0 -----")
  --+233
else -- FSE DBCA portion (when compiled indepenently on pre switch portion)
  fleetSW()
end