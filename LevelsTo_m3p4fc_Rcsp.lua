local g = golly() -- for debug messages
local log = io.open("c:\\Golly\\LevelsToDBCABitslog.txt", "w")
local etab = io.open("c:\\Golly\\LevelsToDBCABitsEvenLines.txt", "w")
local logLevel=0

local d,dCache,delta
local fires={{"o","1"},{"e","0"},{"o","0"},{"e","1"},{"o","1"},{"e","0"},{"o","0"},{"e","1"}}  -- {firecolor*1+firephase*2+phaseSwitchReady*4}
-- fc  changes color (and fires):        semiState ~= 1
-- Rcsp changes color and semichanges phase:        semiState ~= (((semiState%8)//2)|5) --5,5,5,5,7,7,7,7
-- white requires dummy fire semiState &8 denotes the fire was not used yet
--                           semiState &16 denotes white is ready for fake fire (we know it is ready for line -1233 in recipe and later, we know there is no odd line before 0)

local function writeLog(msg, level)
  local time = g.millisecs()
  if log and ((not level and logLevel <= 0) or (level and level >= logLevel)) then
    log:write(msg .. "\n")
  end
end

local function inttostring(num)
  return string.sub(num, 1, string.find(num .. ".", "%.") - 1)
end

local function writeDCache()
  --I want all rows except last exactly 80 characters to easily compare the 12 lengths ... I write the bilLen to the end note anyways
  local dExtra=""
  if string.len(dCache)>80 then
    local poscomm = string.find(dCache,'%-%-')
    local poseol = string.find(dCache,'%\n')
    if poscomm then
      --g.note('A')
      if poscomm>80 then
        --g.note('AA')
        if poseol and poseol<80 then
          --g.note('AAA')
          dExtra=string.sub(dCache,poseol+1)
          dCache=string.sub(dCache,1,poseol-1)
          if string.len(dCache)>0 and d then
            --g.note('AAAA')
            --bitLen=bitLen+string.len(dCache)
            d:write(dCache.."\n")
          end
          dCache=dExtra
          return
        end
        --g.note('AA2')
        dExtra=string.sub(dCache,81)
        dCache=string.sub(dCache,1,80)
        if string.len(dCache)>0 and d then
          --g.note('AA2A')
          --bitLen=bitLen+string.len(dCache)
          d:write(dCache.."\n")
        end
        --g.note('AA3')
        dCache=dExtra
        return
      else
        --g.note('AB')
        if poseol and poseol<81 then
          --g.note('ABA')
          dExtra=string.sub(dCache,poseol+1)
          dCache=string.sub(dCache,1,poseol-1)
          if string.len(dCache)>0 and d then
            --g.note('ABAA')
            --bitLen=bitLen+string.len(dCache)
            d:write(dCache.."\n")
          end
          --g.note('ABA2')
          dCache=dExtra
          return
        else
          --g.note('ABB')
          if poscomm>1 then
            --g.note('ABBA')
            dExtra=string.sub(dCache,poscomm)
            dCache=string.sub(dCache,1,poscomm-1)
            if string.len(dCache)>0 and d then
              --g.note('ABBAA')
              --bitLen=bitLen+string.len(dCache)
              d:write(dCache.."\n")
            end
            --g.note('ABBA2')
            dCache=dExtra
            return
          else
            --g.note('ABBB')
            if poseol then
              --g.note('ABBBA')
              dExtra=string.sub(dCache,poseol+1)
              dCache=string.sub(dCache,1,poseol-1)
              if string.len(dCache)>0 and d then
                --g.note('ABBBAA')
                --bitLen=bitLen+string.len(dCache)
                d:write(dCache.."\n")
              end
              --g.note('ABBBA2')
              dCache=dExtra
              return
            else
              --g.note('ABBBB')
              if string.len(dCache)>0 and d then
                --g.note('ABBBBA')
                --bitLen=bitLen+string.len(dCache)
                d:write(dCache.."\n")
              end
              --g.note('ABBBB2')
              dCache=''
              return
            end
          end
        end
      end
    end
    if poseol and poseol<80 then
      --g.note('2A')
      dExtra=string.sub(dCache,poseol+1)
      dCache=string.sub(dCache,1,poseol-1)
      if string.len(dCache)>0 and d then
        --g.note('2AA')
        --bitLen=bitLen+string.len(dCache)
        d:write(dCache.."\n")
      end
      --g.note('2A2')
      dCache=dExtra
      return
    end
    dExtra=string.sub(dCache,81)
    dCache=string.sub(dCache,1,80)
  end
  if string.len(dCache)>0 and d then
    --bitLen=bitLen+string.len(dCache)
    d:write(dCache.."\n")
  end
  dCache=dExtra
end

local function boolean2Bit(b)
  return b and "1" or "2"
end

local function boolMove(b,dist)
  dCache = dCache..boolean2Bit(b)
  if b and dist then delta = delta-dist end
  --codon virtually (+1)
end

local function move_m3_p4()
  do boolMove(((delta%4)>0) or (delta<0),-3) end
  do boolMove(((delta%3)>0) or (delta>0),4) end
end

--[[
local function process_command_fcp_Rcsp(command,moves)
  local color,phase,firematch,phaseexact,semiState
  delta=tonumber(string.sub(command,1,-3)) or 0
  color=string.sub(command,-2,-2)
  phase=string.sub(command,-1,-1)
  -- we are after fire (and the following reset)
  repeat
    --g.note(dCache.." A "..command.." "..delta..","..color..","..phase..","..semiState..boolean2Bit(phaseknown==nil))
    do moves() end
    phaseexact = phase == fires[semiState+1][2]
    firematch = (delta==0) and (color == fires[semiState+1][1])
                and (phaseexact or phase=="2" or (startphase==nil and (semiState<4)) or (startsemiphaseswitch==nil and (semiState>3)))
    do boolMove(firematch) end
    semiState = semiState ~ ((semiState//2)|5) --#cps
  until firematch
  semiState = semiState ~ 3 -- fcp
end
--]]

local dirCommonPrefix = "c:\\Golly\\"

local function inDelimiters(name, leftDelimiter, rightDelimiter)
  return string.match(name, '%' .. leftDelimiter .. '(.-)%' .. rightDelimiter)
end

local levels={} -- {{phase,periodicity,line}}
local comLines={} -- {lines}
-- if signFlip all ne numbers are considered negated
-- colorFlip all lines are increased by 1 (after possible negation)
local function readLevels(signFlip, lineShift)
  local levelFile = io.open(dirCommonPrefix.."DBCAlevels.txt")
  local levetoutput = io.open(dirCommonPrefix.."DBCAlevelsA.txt", "w")
  local inputLine = levelFile:read()
  local outputlineStart, outputlineEnd
  lineShift=lineShift or 0
  local l=0
  g.show(inputLine)
  local commentLines=''
  while inputLine do
    outputlineStart,outputlineEnd='',''
    local pos = string.find(inputLine,"%-%-")
    if pos then
      outputlineEnd = string.sub(inputLine, pos, -1)
      inputLine = string.sub(inputLine,1, pos-1)
--      g.note("outputLineEnd '"..outputlineEnd.."' startOfiInputLine "..inputLine)
    end
    pos=string.find(inputLine,"%(g")
    if pos then
      l=l+1
      levels[l]={}
      comLines[l],commentLines=commentLines,''
      if comLines[l]~='' then
        --g.note('comLines['..l..']='..comLines[l])
      end
      while pos do
        --g.note('A')
        local gp,gm,gl=0+string.sub(inputLine,pos+2,pos+2),0+string.sub(inputLine,pos+4,pos+4),0+inDelimiters(inputLine,'[',']')
        gl=gl*((signFlip and -1) or 1)+lineShift
        outputlineStart=outputlineStart.."(g"..gp.."%"..gm..")["..gl.."]"
        if gm>2 then
          g.show('%'..gm.."? converted to %2!")
          gp,gm=gp%2,2
        end
        levels[l][1+#levels[l]]={gp,gm,gl}
        pos=string.find(inputLine,"%]")
        inputLine = string.sub(inputLine, pos+1)
        pos=string.find(inputLine,"%(g")
        g.show("levels["..l.."]["..#levels[l].."]={"..levels[l][#levels[l]][1]..","..levels[l][#levels[l]][2]..","..levels[l][#levels[l]][3].."}"..inputLine..(pos or "nil"))
      end
      --g.note('B')
    elseif outputlineEnd~='' then
      commentLines=commentLines..outputlineEnd..'\n'
      if string.find(commentLines,'EvenLinesOption=') then
        l=l+1
        levels[l]={}
        comLines[l],commentLines=commentLines,''
      end
      --g.note('commentLines '..commentLines)
    end
    levetoutput:write(outputlineStart..outputlineEnd.."\n")
    inputLine = levelFile:read()
    g.show(inputLine or "nil")
  end
  comLines[l+1],commentLines=commentLines,''
  --g.note(l.." levels read")
end

local function formatLevelBits(levelBits)
  local res=''
  local prevTriple,prevTripleCnt,triple='X',0
  while levelBits~="" do
    triple,levelBits=string.sub(levelBits,1,3),string.sub(levelBits,4)
    if triple==prevTriple then
      prevTripleCnt = prevTripleCnt + 1
    else
      if prevTripleCnt>1 then
        res = res .. '*' .. prevTripleCnt
      end
      res = res .. "+'"..triple.."'"
      prevTriple,prevTripleCnt = triple,1
    end
  end
  if prevTripleCnt>1 then
    res = res .. '*' .. prevTripleCnt
  end
  return string.sub(res,2)
end
-- unless lastFireLine is specified we expect the position of the arm block correspond to firing on first level
-- (when there are more options, each starts with optimally placed block).
local function levels2Bits(startSemiState, lastFireLine, commentOffset,evenLinesOption)
-- prelevelState semiState.."_"..blockPosition->{semiState,blockPosition,levelBits,countOfBits,prevLevelIndex}
-- blockPosition before the fireDec
  local prevLevelStates={}
  local evenLinesImportant = evenLinesOption ~= nil
  local evenLinesFilter = tonumber(evenLinesOption) -- could be nil if we just need a statistic
  local midFireLine
  local oddOffs=9
  prevLevelStates[1]={}
  local thisLevelStates={}
  for i=1,#levels[1] do
    local line,blockPos=levels[1][i][3]
    if lastFireLine then
      line=lastFireLine
    end
    local numEvenLines=((line+1)%2)
    blockPos = (line + (line % 2)*oddOffs)/2
    local DP_State=startSemiState.."_"..blockPos
    if evenLinesImportant then
      DP_State = DP_State.."_"..numEvenLines
    end
    if not thisLevelStates[DP_State] then
      thisLevelStates[DP_State]=1+#prevLevelStates[1]
    end
    prevLevelStates[1][thisLevelStates[DP_State]]={startSemiState,blockPos,"",0,0,0,numEvenLines}
    writeLog("StartState and Pos and numEvenLines "..startSemiState.." "..inttostring(blockPos).." "..numEvenLines)
    if lastFireLine then
      break
    end
  end
  writeLog("#Levels " .. #levels)
  for l=1,#levels do
    thisLevelStates={}
    prevLevelStates[l+1]={}
    --writeLog("Level " .. l .. "#levels[l] " .. #levels[l])
    if #levels[l]>0 then
      for i=1,#levels[l] do
        local line,phase=levels[l][i][3],(levels[l][i][2]==1) and "2" or levels[l][i][1]..""
        local colorInd=(line % 2)
        local color = string.sub("oe",1+colorInd,1+colorInd)
        local blockPos = (line + colorInd * oddOffs)/2
        writeLog("Option " .. i .. " line " .. line .. " phase " .. phase .. " pos " .. inttostring(blockPos) .. " #prevLevelStates[l] " .. #prevLevelStates[l])
        for j=1,#prevLevelStates[l] do
          local semiState = prevLevelStates[l][j][1]
          for Wfake = (color=="e" or semiState<8) and 0 or 1,(semiState//8 == 1) and 1 or 0 do
            delta = blockPos - prevLevelStates[l][j][2] + 1 -- +1 to compensate after fire decrement
            semiState = prevLevelStates[l][j][1]
            if line==-1233 and semiState>=16 then
              semiState = semiState - 16 -- White color (odd lines ... e) is ready
            end
            dCache=""
            repeat
              --writeLog("Level " .. l .. " option " .. i .. " Wfake "..Wfake .. " prev " .. j
              --        .. " pos " .. inttostring(blockPos) .. " prevpos " .. inttostring(prevLevelStates[l][j][2]) .. " delta " .. inttostring(delta) .. " line " .. line
              --        .. " color " .. colorInd .. " phase " .. phase .. " semiState " .. semiState .. " dCache " ..dCache)
              move_m3_p4()
              local firematch = (delta==0) and (color == fires[(semiState%8)+1][1]) and (phase=="2" or phase == fires[(semiState%8)+1][2]) and (semiState<8 or (color=="e" and Wfake == 0))
              local Wfakematch = (semiState // 8 == 1) and fires[(semiState%8)+1][1] == "o" and Wfake == 1
              if Wfakematch then
                boolMove(true)
                firematch = false
                semiState = semiState ~ 9 -- fake+c ... it occurs before #cps, but the changes comute
                delta = delta + 1 -- to compensate the dec (of the fake fire)
              else
                boolMove(firematch)
              end
              semiState = semiState ~ (((semiState%8)//2)|5) --#cps
            until firematch
            semiState = semiState ~ 1 -- c ... it occurs before #cps, but the changes comute
            local DP_State=semiState.."_"..blockPos
            local numEvenLines = prevLevelStates[l][j][7]+(1-colorInd)
            if evenLinesImportant then
              DP_State = DP_State.."_"..numEvenLines
            end
            writeLog(formatLevelBits(dCache))
            local cost=string.len(dCache)+prevLevelStates[l][j][4]
            writeLog("Level " .. l .. " option " .. i .. " Wfake "..Wfake .. " prev " .. j
                    .. " pos " .. inttostring(blockPos) .. " prevpos " .. inttostring(prevLevelStates[l][j][2]) .. " delta " .. inttostring(delta) .. " line " .. line
                    .. " color " .. colorInd .. " phase " .. phase .. " semiState " .. semiState .. " cost " .. cost.." numEvenLines "..numEvenLines)
            if thisLevelStates[DP_State] then
              if prevLevelStates[l+1][thisLevelStates[DP_State]][4]>cost then
                prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,dCache,cost,j,Wfake,numEvenLines}
              end
            else
              thisLevelStates[DP_State]=1+#prevLevelStates[l+1]
              prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,dCache,cost,j,Wfake,numEvenLines}
            end
          end
        end
      end
    else
      evenLinesImportant = true
      local pos=string.find(comLines[l],'EvenLinesOption=')
      evenLinesFilter = 0+inDelimiters(string.sub(comLines[l],pos),'=',',')
      pos=string.find(comLines[l],'LastFireLine=',pos)
      midFireLine = 0+inDelimiters(string.sub(comLines[l],pos),'=',' ')
      local blockPos = (midFireLine + (midFireLine % 2)*oddOffs)/2
      for j=1,#prevLevelStates[l] do
        local semiState = prevLevelStates[l][j][1]
        local DP_State=semiState.."_"..blockPos.."_0"
        local cost=prevLevelStates[l][j][4]
        if thisLevelStates[DP_State] then
          if prevLevelStates[l+1][thisLevelStates[DP_State]][4]>cost then
            prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,'',cost,j,0,0}
          end
        else
          thisLevelStates[DP_State]=1+#prevLevelStates[l+1]
          prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,'',cost,j,0,0}
        end
      end
    end
  end
  local nameSuffix=startSemiState..
          ((lastFireLine and "_"..lastFireLine) or "")..
          ((midFireLine and "_"..midFireLine) or "")..
          ((evenLinesFilter and "e"..evenLinesFilter) or "")..".txt"
  d = io.open(dirCommonPrefix.."DBCAbits_"..nameSuffix,"w")
  local r = io.open(dirCommonPrefix.."DBCAbitsR_"..nameSuffix,"w")
  local state=prevLevelStates[#levels+1][1]
  local bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines,numEvenLines=
    state[4],state[3],state[5],state[6],state[1],state[2],state[7],state[7]
  local bestCostEvenLines,evenLinesSet={},{}
  if evenLinesImportant then
    evenLinesSet[1]=numEvenLines
    bestCostEvenLines[numEvenLines]=bestCost
  end
  for j=2,#prevLevelStates[#levels+1] do
    state=prevLevelStates[#levels+1][j]
    if evenLinesImportant then
      local numEvenLines=state[7]
      if bestCostEvenLines[numEvenLines] then
        if bestCostEvenLines[numEvenLines]>state[4] then
          bestCostEvenLines[numEvenLines]=state[4]
          if evenLinesFilter and numEvenLines == evenLinesFilter then
            bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines=
              state[4],state[3],state[5],state[6],state[1],state[2],state[7]
          end
        end
      else
        evenLinesSet[1+#evenLinesSet]=numEvenLines
        bestCostEvenLines[numEvenLines]=state[4]
        if evenLinesFilter and numEvenLines == evenLinesFilter then
          bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines=
            state[4],state[3],state[5],state[6],state[1],state[2],state[7]
        end
      end
    end
    if not evenLinesFilter then
      if bestCost>state[4] then
        bestCost,bestDCache,bestPrevInd,bestPrevWfake,semiState,blockPos,bestELines=
          state[4],state[3],state[5],state[6],state[1],state[2],state[7]
      end
    end
  end
  if evenLinesImportant then
    etab:write("Even Lines table for startSemiState "..startSemiState.."\n")
    for i=1,#evenLinesSet do
      numEvenLines = evenLinesSet[i]
      etab:write(numEvenLines.." "..bestCostEvenLines[numEvenLines].."\n")
    end
  end
  local info = (#levels+commentOffset) .. " pos " .. inttostring(blockPos) .. " semistate ".. semiState .. " evenLines " .. bestELines ..
          " (g"..(((semiState%8)//4+((semiState%4)//2)+semiState)%2).."%2)["..inttostring(2*blockPos-(semiState%2)*oddOffs).."]\n"
  r:write("    BRecipe=" .. formatLevelBits(bestDCache) .. " # " .. info)
  bestDCache = bestDCache .. "-- " .. info
  if comLines[#levels+1] and comLines[#levels+1]~='' then
    bestDCache = bestDCache .. "\n" .. comLines[#levels+1]
  end
  for l=#levels,2,-1 do
    local levelBits,semiState,blockPos,eLines=
      prevLevelStates[l][bestPrevInd][3],
      prevLevelStates[l][bestPrevInd][1],
      prevLevelStates[l][bestPrevInd][2],
      prevLevelStates[l][bestPrevInd][7]
    local o_m_sqrt_g_p=((semiState%8)//2)%3
    info = (l+commentOffset-1) .. " pos " .. inttostring(prevLevelStates[l][bestPrevInd][2]) ..
            " semistate ".. semiState .. " evenLines " .. eLines ..
            " (g"..(((semiState%8)//4+((semiState%4)//2)+semiState)%2).."%2)["..inttostring(2*blockPos-(semiState%2)*oddOffs).."]\n"
    r:write("    BRecipe=" .. formatLevelBits(levelBits) .. " + BRecipe # " .. info)
    if comLines[l]~='' then
      bestDCache = comLines[l]..bestDCache
    end
    bestDCache,bestPrevInd=levelBits .. "-- " .. info .. bestDCache,prevLevelStates[l][bestPrevInd][5]
  end
  if comLines[1]~='' then
    bestDCache = comLines[1]..bestDCache
  end
  r:write("    recipe+=BRecipe\n")
  dCache=bestDCache
  while dCache~="" do
    g.show(dCache)
    writeDCache()
  end
  d:close()
end

local case=-2
if (case==-2) then --DBCA after hand white leak portion
  readLevels() -- have to be true, true
  levels2Bits(25,-1645, 0) -- have to be (1+8+16,-413,20) and 16, 8 logic changed to other color
elseif (case==1) then --flipping level not required for DBCA ...
  readLevels(true)
elseif (case==2) then -- flip with shift
  readLevels(true,98+923)
else -- FSE DBCA portion (when compiled indepenently on pre switch portion)
  readLevels()
  for ss=0,7 do
    levels2Bits(ss,1, 0,1046)
  end
end