local g = golly() -- for debug messages
local log = io.open("c:\\Golly\\LevelsToECCABitslog.txt", "w")
local logLevel=0
local ECCApositionOdd = 1

local d,dCache,delta,colorOK,phaseOK,dirOK
local fires={{"o","0"},{"e","0"},{"o","1"},{"e","1"},{"o","0"},{"e","0"},{"o","1"},{"e","1"}}  -- {firecolor*1+firephase*2+plusdirection*4}
-- fc  changes color (and fires):  semiState ~= 1
-- p changes phase:  semiState ~= 2
-- cp changes color and phase: semiState ~= 3
-- s changes direction: semiState ~= 4

local function writeLog(msg, level)
  local time = g.millisecs()
  if log and ((not level and logLevel <= 0) or (level and level >= logLevel)) then
    log:write(msg .. "\n")
  end
end

local function inttostring(num)
  return string.sub(num, 1, string.find(num .. ".", "%.") - 1)
end

local function writeDCache()
  --I want all rows except last exactly 80 characters to easily compare the 12 lengths ... I write the bilLen to the end note anyways
  local dExtra=""
  if string.len(dCache)>80 then
    local poscomm = string.find(dCache,'%-%-')
    local poseol = string.find(dCache,'%\n')
    if poscomm then
      --g.note('A')
      if poscomm>80 then
        --g.note('AA')
        if poseol and poseol<80 then
          --g.note('AAA')
          dExtra=string.sub(dCache,poseol+1)
          dCache=string.sub(dCache,1,poseol-1)
          if string.len(dCache)>0 and d then
            --g.note('AAAA')
            --bitLen=bitLen+string.len(dCache)
            d:write(dCache.."\n")
          end
          dCache=dExtra
          return
        end
        --g.note('AA2')
        dExtra=string.sub(dCache,81)
        dCache=string.sub(dCache,1,80)
        if string.len(dCache)>0 and d then
          --g.note('AA2A')
          --bitLen=bitLen+string.len(dCache)
          d:write(dCache.."\n")
        end
        --g.note('AA3')
        dCache=dExtra
        return
      else
        --g.note('AB')
        if poseol and poseol<81 then
          --g.note('ABA')
          dExtra=string.sub(dCache,poseol+1)
          dCache=string.sub(dCache,1,poseol-1)
          if string.len(dCache)>0 and d then
            --g.note('ABAA')
            --bitLen=bitLen+string.len(dCache)
            d:write(dCache.."\n")
          end
          --g.note('ABA2')
          dCache=dExtra
          return
        else
          --g.note('ABB')
          if poscomm>1 then
            --g.note('ABBA')
            dExtra=string.sub(dCache,poscomm)
            dCache=string.sub(dCache,1,poscomm-1)
            if string.len(dCache)>0 and d then
              --g.note('ABBAA')
              --bitLen=bitLen+string.len(dCache)
              d:write(dCache.."\n")
            end
            --g.note('ABBA2')
            dCache=dExtra
            return
          else
            --g.note('ABBB')
            if poseol then
              --g.note('ABBBA')
              dExtra=string.sub(dCache,poseol+1)
              dCache=string.sub(dCache,1,poseol-1)
              if string.len(dCache)>0 and d then
                --g.note('ABBBAA')
                --bitLen=bitLen+string.len(dCache)
                d:write(dCache.."\n")
              end
              --g.note('ABBBA2')
              dCache=dExtra
              return
            else
              --g.note('ABBBB')
              if string.len(dCache)>0 and d then
                --g.note('ABBBBA')
                --bitLen=bitLen+string.len(dCache)
                d:write(dCache.."\n")
              end
              --g.note('ABBBB2')
              dCache=''
              return
            end
          end
        end
      end
    end
    if poseol and poseol<80 then
      --g.note('2A')
      dExtra=string.sub(dCache,poseol+1)
      dCache=string.sub(dCache,1,poseol-1)
      if string.len(dCache)>0 and d then
        --g.note('2AA')
        --bitLen=bitLen+string.len(dCache)
        d:write(dCache.."\n")
      end
      --g.note('2A2')
      dCache=dExtra
      return
    end
    dExtra=string.sub(dCache,81)
    dCache=string.sub(dCache,1,80)
  end
  if string.len(dCache)>0 and d then
    --bitLen=bitLen+string.len(dCache)
    d:write(dCache.."\n")
  end
  dCache=dExtra
end

local function boolean2Bit(b)
  return b and "1" or "2"
end

local function boolMove(b,dist)
  dCache = dCache..boolean2Bit(b)
  if b and dist then delta = delta-dist end
  --codon virtually (+1)
end

local function moves_m4r()
  local inDelta=delta
  while (delta>3) do
    boolMove(true,4)
  end
  boolMove(false)
  writeLog("m4r " .. inDelta .. "->" .. delta,-5)
end

local function move_m2()
  local inDelta=delta
  boolMove((delta>1),2)
  writeLog("m2 " .. inDelta .. "->" .. delta,-5)
end

local function move_m1()
  local inDelta=delta
  boolMove((delta>0),1)
  writeLog("m1 " .. inDelta .. "->" .. delta,-5)
end

local dirCommonPrefix = "c:\\Golly\\"

local function inDelimiters(name, leftDelimiter, rightDelimiter)
  return string.match(name, '%' .. leftDelimiter .. '(.-)%' .. rightDelimiter)
end

local levels={} -- {{phase,periodicity,line}}
local comLines={} -- {lines}
-- if signFlip all ne numbers are considered negated
-- colorFlip all lines are increased by 1 (after possible negation)
local function readLevels()
  local levelFile = io.open(dirCommonPrefix.."ECCAlevels.txt")
  local inputLine = levelFile:read()
  local l=0
  g.show(inputLine)
  local commentLines,comment=''
  while inputLine do
    local pos = string.find(inputLine,"%-%-")
    comment=''
    if pos then
      comment = string.sub(inputLine, pos, -1)
      inputLine = string.sub(inputLine,1, pos-1)
      if string.find(comment,'LastFireLine=') then
        l=l+1
        levels[l]={}
        comLines[l],commentLines=commentLines..comment..'\n',''
        comment=''
      end
    end
    pos=string.find(inputLine,"%(g")
    if pos then
      l=l+1
      levels[l]={}
      comLines[l],commentLines=commentLines,''
      if comLines[l]~='' then
        --g.note('comLines['..l..']='..comLines[l])
      end
      while pos do
        --g.note('A')
        local gp,gm,gl=0+string.sub(inputLine,pos+2,pos+2),0+string.sub(inputLine,pos+4,pos+4),0+inDelimiters(inputLine,'[',']')
        if gm>2 then
          g.show('%'..gm.."? converted to %2!")
          gp,gm=gp%2,2
        end
        levels[l][1+#levels[l]]={gp,gm,gl}
        pos=string.find(inputLine,"%]")
        inputLine = string.sub(inputLine, pos+1)
        pos=string.find(inputLine,"%(g")
        g.show("levels["..l.."]["..#levels[l].."]={"..levels[l][#levels[l]][1]..","..levels[l][#levels[l]][2]..","..levels[l][#levels[l]][3].."}"..inputLine..(pos or "nil"))
      end
      --g.note('B')
    elseif comment~='' then
      commentLines=commentLines..comment..'\n'
      --g.note('commentLines '..commentLines)
    end
    inputLine = levelFile:read()
    g.show(inputLine or "nil")
  end
  comLines[l+1],commentLines=commentLines,''
  --g.note(l.." levels read")
end

-- unless lastFireLine is specified we expect the position of the arm block correspond to firing on first level
local function levels2Bits(startSemiState, lastFireLine)
-- blockPosition before the fireDec
  local prevLevelStates={}
  local oddOffs=-9
  prevLevelStates[1]={}
  local thisLevelStates={}
  for i=1,#levels[1] do
    local line,blockPos=levels[1][i][3]
    if lastFireLine then
      line=lastFireLine
    end
    blockPos = (line + ((line+ECCApositionOdd) % 2)*oddOffs)/2
    local DP_State=startSemiState.."_"..blockPos
    if not thisLevelStates[DP_State] then
      thisLevelStates[DP_State]=1+#prevLevelStates[1]
    end
    prevLevelStates[1][thisLevelStates[DP_State]]={startSemiState,blockPos,"",0,0}
    writeLog("StartState and Pos "..startSemiState.." "..inttostring(blockPos))
    if lastFireLine then
      break
    end
  end
  writeLog("#Levels " .. #levels)
  for l=1,#levels do
    thisLevelStates={}
    prevLevelStates[l+1]={}
    --writeLog("Level " .. l .. "#levels[l] " .. #levels[l])
    if #levels[l]>0 then
      for i=1,#levels[l] do
        local line,phase=levels[l][i][3],(levels[l][i][2]==1) and "2" or levels[l][i][1]..""
        local colorInd=((line+ECCApositionOdd) % 2)
        local color = string.sub("oe",1+colorInd,1+colorInd)
        local blockPos = (line + colorInd * oddOffs)/2
        writeLog("Option " .. i .. " line " .. line .. " phase " .. phase .. " pos " .. inttostring(blockPos) .. " #prevLevelStates[l] " .. #prevLevelStates[l])
        for j=1,#prevLevelStates[l] do
          local semiState = prevLevelStates[l][j][1]
          delta = blockPos - prevLevelStates[l][j][2] + 1 -- +1 to compensate after fire decrement
          local oridelta=delta
          dirOK = (delta==0) or ((delta<0) == (semiState<4))
          delta = math.abs(delta)
          writeLog("abs "..oridelta.."->"..delta,-5)
          colorOK = (color == fires[semiState+1][1])
          phaseOK = ((phase=="2") or (phase == fires[semiState+1][2]))
          dCache=""
          if l>1 then
            boolMove(dirOK) -- dir change signal has inverted logic, delta should be changed outside !!
            if not dirOK then
              semiState=semiState ~ 4
              delta = delta-1
            end
          end
          moves_m4r()
          move_m2()
          move_m1()
          boolMove(phaseOK~=colorOK)
          if phaseOK~=colorOK then
            semiState = semiState ~ 2
          end
          boolMove(not colorOK)
          if not colorOK then
            semiState = semiState ~ 3
          end
          semiState = semiState ~ 1 -- fire
          local DP_State=semiState.."_"..blockPos
          writeLog(dCache)
          local cost=string.len(dCache)+prevLevelStates[l][j][4]
          writeLog("Level " .. l .. " option " .. i .. " prev " .. j
                    .. " pos " .. inttostring(blockPos) .. " prevpos " .. inttostring(prevLevelStates[l][j][2]) .. " oridelta " .. inttostring(oridelta) .. " line " .. line
                    .. " color " .. colorInd .. " phase " .. phase .. " semiState " .. semiState .. " cost " .. cost)
          if thisLevelStates[DP_State] then
            if prevLevelStates[l+1][thisLevelStates[DP_State]][4]>cost then
              prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,dCache,cost,j}
            end
          else
            thisLevelStates[DP_State]=1+#prevLevelStates[l+1]
            prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,dCache,cost,j}
          end
        end
      end
    else
      local pos=string.find(comLines[l],'LastFireLine=')
      local midFireLine = 0+inDelimiters(string.sub(comLines[l],pos),'=',' ')
      local blockPos = (midFireLine + (midFireLine % 2)*oddOffs)/2
      writeLog(comLines[l].."FML="..midFireLine)
      for j=1,#prevLevelStates[l] do
        local semiState = prevLevelStates[l][j][1]
        local DP_State=semiState.."_"..blockPos
        local cost=prevLevelStates[l][j][4]
        if thisLevelStates[DP_State] then
          if prevLevelStates[l+1][thisLevelStates[DP_State]][4]>cost then
            prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,'',cost,j}
          end
        else
          thisLevelStates[DP_State]=1+#prevLevelStates[l+1]
          prevLevelStates[l+1][thisLevelStates[DP_State]]={semiState,blockPos,'',cost,j}
        end
      end
    end
  end
  local nameSuffix=startSemiState..
          ((lastFireLine and "_"..lastFireLine) or "")..".txt"
  d = io.open(dirCommonPrefix.."ECCAbits_"..nameSuffix,"w")
  local r = io.open(dirCommonPrefix.."ECCAbitsR_"..nameSuffix,"w")
  local state=prevLevelStates[#levels+1][1]
  local bestCost,bestDCache,bestPrevInd,semiState,blockPos=
    state[4],state[3],state[5],state[1],state[2]
  for j=2,#prevLevelStates[#levels+1] do
    state=prevLevelStates[#levels+1][j]
    if bestCost>state[4] then
      bestCost,bestDCache,bestPrevInd,semiState,blockPos=
      state[4],state[3],state[5],state[1],state[2]
    end
  end
  local info = (#levels) .. " pos " .. inttostring(blockPos) .. " semistate ".. semiState ..
          " (g"..((semiState//2)%2).."%2)["..inttostring(2*blockPos-((semiState+1)%2)*oddOffs).."]\n"
  r:write("    BRecipe='" .. bestDCache .. "' # " .. info)
  bestDCache = bestDCache .. "-- " .. info
  if comLines[#levels+1] and comLines[#levels+1]~='' then
    bestDCache = bestDCache .. "\n" .. comLines[#levels+1]
  end
  for l=#levels,2,-1 do
    local levelBits,semiState,blockPos=
      prevLevelStates[l][bestPrevInd][3],
      prevLevelStates[l][bestPrevInd][1],
      prevLevelStates[l][bestPrevInd][2]
    local o_m_sqrt_g_p=((semiState%8)//2)%3
    info = (l-1) .. " pos " .. inttostring(prevLevelStates[l][bestPrevInd][2]) ..
            " semistate ".. semiState ..
            " (g"..((semiState//2)%2).."%2)["..inttostring(2*blockPos-((semiState+1)%2)*oddOffs).."]\n"
    r:write("    BRecipe='" .. levelBits .. "' + BRecipe # " .. info)
    if comLines[l]~='' then
      bestDCache = comLines[l]..bestDCache
    end
    bestDCache,bestPrevInd=levelBits .. "-- " .. info .. bestDCache,prevLevelStates[l][bestPrevInd][5]
  end
  if comLines[1]~='' then
    bestDCache = comLines[1]..bestDCache
  end
  r:write("    recipe+=BRecipe\n")
  dCache=bestDCache
  while dCache~="" do
    g.show(dCache)
    writeDCache()
  end
  d:close()
end

readLevels() -- have to be true, true
levels2Bits(7,-600+351-256+64)