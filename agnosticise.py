'''
Rewrite DBCA_recipe.txt in-place to remove unnecessary phase constraints.
'''

import lifelib
lt = lifelib.load_rules('b3s23').lifetree(n_layers=1)

pat = '''x = 216, y = 20, rule = B3/S23
bo$obo$2o15$213b2o$213bobo$213bo!'''
pat = lt.pattern(pat)

target = pat & pat[8]
glider = (pat - target)[-8192]

with open('recipes/DBCA_recipe.txt') as f:
    glider_list = [tuple(map(int, line.strip('(),\n').split(','))) for line in f]

for (i, (x, y, z)) in enumerate(glider_list):
    t2 = (target + glider(0, x)[z])[16384]
    if (y == 2):
        t3 = (target + glider(0, x)[z+1])[16384]
        if (t2 == t3):
            glider_list[i] = (x, 1, 0)
    target = t2
    if target != target[8]:
        raise ValueError('Error on line %d : (%d, %d, %d)' % (i, x, y, z))

with open('recipes/DBCA_recipe.txt', 'w') as f:
    for t in glider_list:
        f.write(str(t) + ',\n')
