#include "object_cache.hpp"

namespace psl {


std::vector<uint64_t> flatcoords(apg::pattern p) {

    uint32_t pop = p.totalPopulation();
    std::vector<int64_t> coords(pop*2);
    p.get_coords(&(coords[0]));
    std::vector<uint64_t> flattened(pop);
    for (uint32_t i = 0; i < pop; i++) {
        uint64_t x = coords[2*i]   + 0x80000000u;
        uint64_t y = coords[2*i+1] + 0x80000000u;
        flattened[i] = (y << 32) + x;
    }

    return flattened;
}



struct Defragmenter {

    sgsalvo orig;
    std::vector<apg::pattern> steps;
    std::vector<std::vector<uint64_t>> edges;
    std::vector<uint64_t> idx2g;
    std::vector<uint64_t> g2idx;
    std::vector<int64_t> ilanes;

    uint64_t N;
    int64_t outputLineShift;

    sgsalvo get_salvo() const {

        sgsalvo res;
        for (auto x : idx2g) { res.push_back(orig[x]); }
        return res;

    }

    apg::pattern get_pattern() {

        return steps[0] + materialise_gliders(steps[0], get_salvo());

    }

    void make_dag() {

        std::cout << "Running " << orig.size() << " gliders..." << std::endl;
        std::map<uint64_t, uint64_t> birthdays;

        for (size_t i = 0; i < orig.size(); i++) {

            if ((i % 100) == 0) { std::cout << i << " gliders completed." << std::endl; }

            apg::pattern x = steps.back();
            apg::pattern y = x + materialise_gliders(x, {orig[i]});
            int64_t bbox[4] = {0}; y.getrect(bbox);
            uint64_t to_run = 4 * bbox[3] + 1024; to_run -= (to_run & 127);
            y = y[to_run];

            if (y != y[8]) {
                std::cerr << "Warning: pattern unstabilised after " << (i + 1) << " gliders." << std::endl;
                return;
            }

            auto deleted = flatcoords(x - y);
            auto created = flatcoords(y - x);

            std::set<uint64_t> jset;

            for (auto dcell : deleted) {
                if (birthdays.count(dcell)) {
                    jset.insert(birthdays[dcell]);
                    birthdays.erase(dcell);
                }
            }

            for (auto ccell : created) {
                birthdays[ccell] = i;
            }

            edges.emplace_back(jset.size());
            int idx = 0;
            for (auto j : jset) { edges.back()[idx++] = j; }

            steps.push_back(y);
        }
    }

    void from_pattern(apg::pattern &x) {

        std::cout << "Separating..." << std::endl;

        apg::pattern initial = x & x[8];
        apg::pattern g = x - initial;
        orig = dematerialise_gliders(g);

        for (uint64_t i = 0; i < orig.size(); i++) {
            g2idx.push_back(i);
            idx2g.push_back(i);
            ilanes.push_back(orig[i].getLane());
        }

        steps.push_back(initial);
        make_dag();
        N = steps.size() - 1;
    }

    std::pair<int64_t, std::pair<uint64_t, uint64_t>> traverse(uint64_t x, int64_t r) const {

        uint64_t maxblen = x;
        int64_t best_saving = 0;
        uint64_t best_blen = 0;
        uint64_t best_clen = 0;

        int64_t maxr = 0;

        for (uint64_t clen = 1; clen <= N - x; clen++) {

            for (auto g : edges[idx2g[x + clen - 1]]) {
                uint64_t idx = g2idx[g];
                if (idx >= x) { continue; }
                maxblen = hh::min(maxblen, x - 1 - idx);
            }

            if (maxblen == 0) { break; }

            // precompute as much stuff as possible before the innermost loop:
            uint64_t z = x + clen;
            int64_t sd1 = ilanes[x] - ilanes[x-1];
            maxr = hh::max(maxr, (ilanes[x] - ilanes[z-1]) * sd1);
            int64_t prec = std::llabs(sd1);
            int64_t target = prec * (prec - r);
            if (z < ilanes.size()) {
                prec += std::llabs(ilanes[z] - ilanes[z-1]);
                prec -= std::llabs(ilanes[z] - ilanes[x-1]);
            }

            // innermost loop:
            for (uint64_t blen = 1; blen <= maxblen; blen++) {
                uint64_t y = x - blen;
                if (maxr + sd1 * (ilanes[y] - ilanes[x-1]) >= target) { break; }
                int64_t saving = prec - std::llabs(ilanes[y] - ilanes[z-1]);
                if (y > 0) {
                    saving += std::llabs(ilanes[y] - ilanes[y-1]);
                    saving -= std::llabs(ilanes[x] - ilanes[y-1]);
                }
                if (saving > best_saving) {
                    best_saving = saving;
                    best_blen = blen;
                    best_clen = clen;
                }
            }
        }

        return std::make_pair(best_saving, std::make_pair(best_blen, best_clen));
    }

    bool try_reorder(uint64_t l, uint64_t x, uint64_t r) {

        std::vector<uint64_t> newseg(r - l);

        for (uint64_t idx = l; idx < x; idx++) {
            newseg[idx - x + (r - l)] = idx2g[idx];
        }

        for (uint64_t idx = x; idx < r; idx++) {
            newseg[idx - x] = idx2g[idx];
        }

        apg::pattern s = steps[l];
        std::vector<apg::pattern> newsteps;
        newsteps.push_back(s);

        for (auto g : newseg) {
            s = s + materialise_gliders(s, {orig[g]});
            int64_t bbox[4] = {0}; s.getrect(bbox);
            uint64_t to_run = 4 * bbox[3] + 1024; to_run -= (to_run & 127);
            s = s[to_run];
            if (s != s[8]) { return false; }
            newsteps.push_back(s);
        }

        if (s != steps[r]) { return false; }

        for (uint64_t i = 0; i < r - l; i++) {
            steps[i + l] = newsteps[i];
            idx2g[i + l] = newseg[i];
            g2idx[newseg[i]] = i + l;
            ilanes[i + l] = orig[idx2g[i + l]].getLane();
        }

        return true;
    }

    int64_t run_pass(uint64_t pass_number) {

        int64_t total_saving = 0;

        for (uint64_t i = 1; i < N; i++) {
            uint64_t x = ((pass_number & 1) ? (N - i) : i);
            auto res = traverse(x, pass_number * 2);
            if (res.first == 0) { continue; }
            uint64_t l = x - res.second.first;
            uint64_t r = x + res.second.second;
            bool succeeded = try_reorder(l, x, r);
            std::cout << "Replacement (" << l << ", " << x << ", " << r;
            if (succeeded) {
                std::cout << ") succeeded; saving = " << res.first << std::endl;
                total_saving += res.first;
            } else {
                std::cout << ") failed" << std::endl;
            }
        }

        std::cout << "Pass completed: saving = " << total_saving << std::endl;

        return total_saving;
    }

    void print_gliders() const {
        auto salvo = get_salvo();
        for (auto sg : salvo) {
            std::cout << sg.getLane() << ":" << sg.getPhase() << " ";
        }
        std::cout << std::endl;
        for (auto sg : salvo) {
            std::cout << "(g" << ((sg.getPhase()+1+4*((1+outputLineShift+sg.getLane())%2))%8) << "%8)[" << (outputLineShift-sg.getLane()) << "]" << std::endl;
        }
    }
};


apg::pattern defragment(apg::pattern &x, int64_t _outputLineShift) {

    Defragmenter df;
    df.from_pattern(x);
    df.outputLineShift = _outputLineShift;

    df.print_gliders();

    int64_t total_saving = 0;
    int64_t saving = 0;
    uint64_t pass_number = 0;
    do {
        saving = df.run_pass(pass_number);
        pass_number += 1;
        total_saving += saving;
    } while (saving > 0);
    std::cout << "Total saving = " << total_saving << std::endl;

    df.print_gliders();

    return df.get_pattern();

}

} // namespace psl


int main(int argc, char* argv[]) {

    int64_t outputLineShift = 1;
    if (argc != 3) {
        if (argc == 4) {
            outputLineShift = std::stoi(argv[3]);
        } else {
            std::cerr << "Usage: ./defrag input.rle output.rle [outputLineShift]" << std::endl;
        }
    }

    std::cout << "Loading..." << std::endl;

    apg::lifetree<uint32_t, 1> lt(1000);
    apg::pattern x(&lt, argv[1]);
    apg::pattern y = psl::defragment(x, outputLineShift);

    {
        std::ofstream f(argv[2]);
        y.write_rle(f);
    }

    return 0;

}
