import golly as g

path='.'.join(("\\"+g.getpath()).split("\\")[-1].split(".")[:-1])
#g.note(path)
pattern = g.getcells(g.getrect())
block = g.parse('2o$2o!')

g.addlayer()
g.new('Work')
g.autoupdate(False)
g.setalgo('HashLife')
g.putcells(pattern,-2,+2)
g.setbase(2)
g.setstep(3)
g.step()
g.putcells(pattern,-2,+2,1,0,0,1,"or")
g.putcells(pattern,-2,+2,1,0,0,1,"xor")
glider_rect=g.getrect()
parity=(glider_rect[0]+glider_rect[1])%2
glider=g.getcells(glider_rect)
g.fit()
#g.note("glider")
g.putcells(pattern,0,0,1,0,0,1,"xor")
initial_pattern=g.getcells(g.getrect())
g.fit()
#g.note("initial pattern")
last_lane = -2544 #-2542 #-2546 exact value is not that important
#while True:
#    #g.note("Run?")
#    g.new("Work")
#    g.putcells(initial_pattern)
#    g.putcells(block,{....})
#    g.putcells(glider_lines,last_lane)
#    g.run(16384)
#    g.putcells(initial_pattern,0,0,1,0,0,1,"xor")
#    g.fit()
#    g.update()
#    if g.getpop()!="9":
#        g.note(f"{last_lane} {g.getpop()}")
#    if g.getpop()=="0":
#        break
#g.note(f"last_lane {last_lane}")

keep_best_n = 256 #128 #256 #1024
glider_lines = 20
last_lane_lines = 9
long_enough = 16384
pop_fact = 1
min_u_fact = 9
delta = 512
dummy_eval=999999999

while keep_best_n<2048:
    best_n = [(initial_pattern, (), last_lane+1, 0, 0)]
    cur_best = best_n[0]
    best_sol = [(),dummy_eval]
    round=0
    rounds = []
    msg=""
    while len(best_n)>0:
        seen = dict()
        round = 1+round
        new_best_n = []
        for i, (pattern, l_recipe, l_lane_dir, l_recipe_cost, l_pattern_estimate_cost) in enumerate(best_n):
            if i==0:
                g.new('Work')
                g.putcells(pattern)
                g.fit()
                g.update()
                #if l_pattern_estimate_cost == 3010:
                #    g.note("Suspect case")
            g.show(f"Round {round}, cur best eval: {cur_best[3]}, {cur_best[4]}. Running {i+1}/{len(best_n)}, Best solution {best_sol[1]}, {msg}")
            minlane = min(map(lambda x:x[0]+x[1], zip(pattern[::2], pattern[1::2])), default=0) - 2 - parity
            minlane = minlane - (minlane%2)
            maxlane = minlane + 2*glider_lines
            if maxlane < l_lane_dir+last_lane_lines:
                maxlane = l_lane_dir+last_lane_lines
            for lane in range(minlane, maxlane, 2):
                g.new('Work')
                g.putcells(pattern)
                g.putcells(glider, lane+parity)
                g.setbase(2)
                g.setstep(14)
                g.step()
                rect = g.getrect()
                pop = int(g.getpop())
                test_hash = g.hash(rect) if rect else 0
                g.setstep(3)
                g.step()
                rect_2 = g.getrect()
                test_hash_2 = g.hash(rect_2) if rect_2 else 0
                if rect==rect_2 and test_hash == test_hash_2:
                    dist = lane - l_lane_dir+(l_lane_dir % 2) - 2
                    lane_dir = lane
                    if dist<0:
                        dist=-dist
                        lane_dir=lane_dir+1
                    if lane_dir % 2 != l_lane_dir % 2:
                        dist=dist-2  #dir change
                    recipe = (lane+parity, l_recipe)
                    recipe_cost=l_recipe_cost+5+(dist//8)
                    if pop==0:
                        if best_sol[1]==dummy_eval or best_sol[1]>recipe_cost:
                            best_sol = [recipe,recipe_cost]
                            #g.fit()
                            #g.update()
                            #g.note(f"sol {recipe_cost}")
                    else:
                        if not test_hash in seen or new_best_n[seen[test_hash]][3]>recipe_cost :
                            test_result = g.getcells(rect)
                            min_u = min(map(lambda x:x[0]+x[1], zip(test_result[::2], test_result[1::2])), default=0)
                            max_u = max(map(lambda x:x[0]+x[1], zip(test_result[::2], test_result[1::2])), default=0)
                            pattern_estimateCost = pop_fact*pop-(min_u_fact*min_u//8)
                            result = (test_result, recipe, lane_dir, recipe_cost, pattern_estimateCost)
                            if best_sol[1]==dummy_eval or recipe_cost<best_sol[1]:
                                if not test_hash in seen:
                                    seen[test_hash]=len(new_best_n)
                                    new_best_n.append(result)
                                else:
                                    new_best_n[seen[test_hash]]=result

        best_n=new_best_n
        if best_sol[1]!=dummy_eval:
            best_n = list(filter(lambda x:x[3]<best_sol[1],best_n))
        mid_len=len(best_n)
        best_n = sorted(best_n, key = lambda x: (x[3]+x[4]))[:keep_best_n]
        msg=f"reduction {len(new_best_n)} -> {mid_len} -> {len(best_n)}"
        if len(best_n)>0:
            cur_best = best_n[0]
            rounds.append((cur_best[3],cur_best[4],best_sol[1],msg))
            with open(f"dump{path}_{keep_best_n}_{glider_lines}_{last_lane_lines}_{pop_fact}_{min_u_fact}.txt", 'w') as f:
                f.write(str(rounds))
                f.write(str(best_sol))

    g.new(f'Recipe{path}_{keep_best_n}_{glider_lines}_{last_lane_lines}_{pop_fact}_{min_u_fact}')

    unfolded = []
    recipe=best_sol[0]
    while recipe:
        lane, recipe = recipe
        unfolded.append((lane))

    g.putcells(initial_pattern)
    cur = 0
    for lane in unfolded[::-1]:
        g.putcells(glider, -cur+lane, cur)
        cur += delta
    g.save(f'Recipe{path}_{keep_best_n}_{glider_lines}_{last_lane_lines}_{pop_fact}.mc','mc',False)

    with open(f"fdump{path}_{keep_best_n}_{glider_lines}_{last_lane_lines}_{pop_fact}_{min_u_fact}.txt", 'w') as f:
        f.write(str(rounds)+msg)
        f.write(str(best_sol))
        f.write(str(unfolded))

    keep_best_n = 2*keep_best_n
