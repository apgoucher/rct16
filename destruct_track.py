import golly as g

path='.'.join(("\\"+g.getpath()).split("\\")[-1].split(".")[:-1])
#g.note(path)
bothSides=True
# corderfleet to destroy NW track from left side / either side (using flipped corership or right)

absorber = g.parse('14b2o$14b2o12$13b2o$12bo2bo$8bo4b2o$7bobo$7bobo$8bo!')
corderships = [g.parse('29bo$28bobo$28b2o4$25bo$24bobo13b2o$25b2o13b2o3$25bo$24bobo$25b2o2$48b2o$48b2o2$53b2o$53bobo$54bo$30b2o$30b2o2$15bo4bo$14bobo2bobo$15b2o3b2o2$9b2o$8bobo$9bo2$56bo$27b2o16bo8b2o$27b2o15bobo8b2o$45b2o3$38bo$37bobo$38bo$14b2o$14b2o2$44b2o$44bobo$45bo6$25b2o$25bobo$26bo!'),g.parse('29bo$28bobo$28b2o4$25bo$24bobo13b2o$25b2o13b2o3$25bo$24bobo$25b2o2$48b2o$48b2o2$53b2o$53bobo$54bo$30b2o$30b2o2$15bo4bo$14bobo2bobo$15b2o3b2o2$9b2o$8bobo$9bo2$55bo$27b2o16bo8bo$27b2o15bobo7b3o$45b2o3$38bo$37bobo$38bo$14b2o$14b2o2$44b2o$44bobo$45bo6$25b2o$25bobo$26bo!')]
cleanCordershipSeed = g.parse('2o$2o!')

initial_pattern=g.getcells(g.getrect())
g.addlayer()
testlayer = g.getlayer()
g.addlayer()
outputlayer = g.getlayer()
g.new('Work')
g.autoupdate(False)
g.setalgo('HashLife')
g.setbase(2)
g.fit()
#g.note("initial pattern")
delta = 64 # multiple of 8
toplane = min(map(lambda x:x[1], zip(initial_pattern[::2], initial_pattern[1::2])), default=0)
botlane = max(map(lambda x:x[1], zip(initial_pattern[::2], initial_pattern[1::2])), default=0)
toplane = ((toplane - delta) // delta) * delta
botlane = (botlane // delta) * delta
leftlane = min(map(lambda x:x[0]-x[1], zip(initial_pattern[::2], initial_pattern[1::2])), default=0)
rightlane = max(map(lambda x:x[0]-x[1], zip(initial_pattern[::2], initial_pattern[1::2])), default=0)
longEnough = (botlane-toplane+delta)*12
logEnough = 1
while longEnough>0:
    logEnough,longEnough=1+logEnough,longEnough//2

#g.note(f"Log enough {logEnough}")
keep_best_n = 1 #1024 #128 #256 #1024
left_lines, left_offset = 4,38 # to test
max_ll=3
right_lines = bothSides and left_lines or 0

while keep_best_n<256:
    best_n = [(initial_pattern, (), 1)]
    cur_best = best_n[0]
    round,rounds,best_sol = 0,[],()
    msg=""
    seen = dict()
    while len(best_n)>0:
        round = 1+round
        new_best_n = []
        for i, (pattern, l_recipe, l_pattern_estimate_cost) in enumerate(best_n):
            if best_sol :
                break
            if 2*i<keep_best_n:
                g.new('Work')
                g.putcells(pattern)
                g.fit()
                g.update()
            elif 2*i==keep_best_n:
                g.new('Best')
                g.putcells(cur_best[0])
                g.fit()
                g.update()
            g.show(f"Round {round}, cur best eval: {cur_best[2]}. Running {i+1}/{len(best_n)}, {msg}")
            leftlane = min(map(lambda x:x[0]-x[1], zip(pattern[::2], pattern[1::2])), default=0)-left_offset
            rightlane = max(map(lambda x:x[0]-x[1], zip(pattern[::2], pattern[1::2])), default=0)+left_offset
            for lane in range(leftlane,leftlane+left_lines, 1):
                if best_sol :
                    break
                for yposPhase in range(0,16):
                    ypos,phase = yposPhase // 2, yposPhase%2
                    g.new(f'Work L{lane-leftlane},{ypos},{phase}%2')
                    g.putcells(pattern)
                    g.putcells(corderships[phase], ypos+lane+botlane+delta, ypos+botlane+delta, 1, 0, 0, 1)
                    g.putcells(absorber, ypos+lane+toplane-delta-1, ypos+toplane-delta-2, 1, 0, 0, 1)
                    g.setbase(2)
                    g.fit()
                    #g.note('debug positioning, debug left_offset')
                    g.setstep(logEnough)
                    g.step()
                    g.putcells(cleanCordershipSeed, ypos+lane+botlane+delta+15, ypos+botlane+delta+36, 1, 0, 0, 1, "xor")
                    rect = g.getrect()
                    #g.note('debug cleanCordershipSeed')
                    if not rect:
                        best_sol = (l_recipe, lane , ypos, phase, 0)
                        break
                    if rect[1]>toplane-delta/2:
                        #absorber and cordership destroyed each other
                        ll = lane - leftlane
                        if ll>max_ll and pop<l_pattern_estimate_cost:
                            max_ll=ll
                            g.note(f"new max_ll {ll} {rect[2]}>{toplane-delta/2}")
                        test_hash = g.hash(rect) if rect else 0
                        g.setstep(1)
                        g.step()
                        rect_2 = g.getrect()
                        test_hash_2 = g.hash(rect_2) if rect_2 else 0
                        if rect==rect_2 and test_hash == test_hash_2:
                            #p2 pattern (I expect condition for rect[2] is sufficient)
                            recipe = (l_recipe, lane , ypos, phase, 0)
                            if not test_hash in seen :
                                test_result = g.getcells(rect)
                                g.new("Eval")
                                for i in range(0,len(test_result),2):
                                    g.setcell((test_result[i]+test_result[i+1])%32,(test_result[i]-test_result[i+1]),1)
                                pop = int(g.getpop())
                                #g.note("debug eval")
                                pattern_estimateCost = pop
                                result = (test_result, recipe, pattern_estimateCost)
                                seen[test_hash]=len(new_best_n)
                                new_best_n.append(result)
            for lane in range(rightlane,rightlane-right_lines, -1):
                if best_sol :
                    break
                for yposPhase in range(0,16):
                    ypos,phase = yposPhase // 2, yposPhase%2
                    g.new(f'Work R{rightlane-lane},{ypos},{phase}%2')
                    g.putcells(pattern)
                    g.putcells(corderships[phase], ypos+lane+botlane+delta, ypos+botlane+delta, 0, 1, 1, 0)
                    g.putcells(absorber, ypos+lane+toplane-delta-2, ypos+toplane-delta-1, 0, 1, 1, 0)
                    g.setbase(2)
                    g.fit()
                    #g.note('debug positioning, debug left_offset')
                    g.setstep(logEnough)
                    g.step()
                    g.putcells(cleanCordershipSeed, ypos+lane+botlane+delta+36, ypos+botlane+delta+15, 0, 1, 1, 0, "xor")
                    rect = g.getrect()
                    #g.note('debug cleanCordershipSeed')
                    if not rect:
                        best_sol = (l_recipe, lane , ypos, phase, 1)
                        break
                    if rect[1]>toplane-delta/2:
                        #absorber and cordership destroyed each other
                        ll = rightlane - lane
                        if ll>max_ll and pop<l_pattern_estimate_cost:
                            max_ll=ll
                            g.note(f"new max_ll {ll} {rect[2]}>{toplane-delta/2}")
                        test_hash = g.hash(rect) if rect else 0
                        g.setstep(1)
                        g.step()
                        rect_2 = g.getrect()
                        test_hash_2 = g.hash(rect_2) if rect_2 else 0
                        if rect==rect_2 and test_hash == test_hash_2:
                            #p2 pattern (I expect condition for rect[2] is sufficient)
                            recipe = (l_recipe, lane , ypos, phase, 1)
                            if not test_hash in seen :
                                test_result = g.getcells(rect)
                                g.new("Eval")
                                for i in range(0,len(test_result),2):
                                    g.setcell((test_result[i]+test_result[i+1])%32,(test_result[i]-test_result[i+1]),1)
                                pop = int(g.getpop())
                                #g.note("debug eval")
                                pattern_estimateCost = pop
                                result = (test_result, recipe, pattern_estimateCost)
                                seen[test_hash]=len(new_best_n)
                                new_best_n.append(result)

        if best_sol:
            break
        best_n=new_best_n
        mid_len=len(best_n)
        best_n = sorted(best_n, key = lambda x: (x[2]))[:keep_best_n]
        msg=f"reduction {len(new_best_n)} -> {mid_len} -> {len(best_n)}"
        if len(best_n)>0:
            cur_best = best_n[0]
            rounds.append((cur_best[2],msg))
            with open(f"dump{path}_{keep_best_n}_{left_lines}_{right_lines}.txt", 'w') as f:
                f.write(str(rounds))

    g.new(f'Recipe{path}_{keep_best_n}_{left_lines}_{right_lines}')

    unfolded = []
    recipe = best_sol
    while recipe:
        (recipe, lane, ypos, phase, side) = recipe
        unfolded.append((lane, ypos, phase, side))

    g.putcells(initial_pattern)
    cur = delta
    wasSide=[False,False]
    prevside=[0,0]
    absorberDeltasS=["",""]
    for ship in unfolded[::-1]:
        lane, ypos, phase, side = ship
        if wasSide[side]:
            g.setlayer(testlayer)
            yp,l,ac=prevside[side]
            deltaac = 0
            ok = False
            while not ok:
                g.new('Test')
                deltaac = deltaac + 8
                if side == 0:
                    g.putcells(corderships[phase], ypos+lane+botlane+delta, ypos+botlane+delta, 1, 0, 0, 1)
                    g.putcells(corderships[phase], yp+l+botlane, yp+botlane, 1, 0, 0, 1)
                    g.putcells(absorber, yp+l+toplane-1, yp+toplane-2, 1, 0, 0, 1)
                    g.putcells(absorber, ypos+lane+toplane-deltaac-1, ypos+toplane-deltaac-2, 1, 0, 0, 1)
                else:
                    #flipped variant
                    g.putcells(corderships[phase], ypos+lane+botlane+delta, ypos+botlane+delta, 0, 1, 1, 0)
                    g.putcells(corderships[phase], yp+l+botlane, yp+botlane, 0, 1, 1, 0)
                    g.putcells(absorber, yp+l+toplane-2, yp+toplane-1, 0, 1, 1, 0)
                    g.putcells(absorber, ypos+lane+toplane-deltaac-2, ypos+toplane-deltaac-1, 0, 1, 1, 0)

                g.setbase(2)
                g.setstep(9)
                g.step()
                g.setstep(6)
                g.step()
                if side == 0:
                    g.putcells(cleanCordershipSeed, ypos+lane+botlane+delta+15, ypos+botlane+delta+36, 1, 0, 0, 1, "xor")
                    g.putcells(cleanCordershipSeed, yp+l+botlane+15, yp+botlane+36, 1, 0, 0, 1, "xor")
                else:
                    #flipped variant
                    g.putcells(cleanCordershipSeed, ypos+lane+botlane+delta+36, ypos+botlane+delta+15, 0, 1, 1, 0, "xor")
                    g.putcells(cleanCordershipSeed, yp+l+botlane+36, yp+botlane+15, 0, 1, 1, 0, "xor")
                g.fit()
                g.update()
                g.setstep(logEnough)
                #g.note("tst")
                g.step()
                ok = len(g.getrect())==0
            abscur = ac+deltaac
            absorberDeltasS[side]=absorberDeltasS[side]+str(ypos-yp+lane-l-deltaac)+";"+str(ypos-yp-deltaac)+"\n"

        else:
            abscur=delta
        g.setlayer(outputlayer)
        if side == 0:
            g.putcells(corderships[phase], ypos+lane+botlane+cur, ypos+botlane+cur, 1, 0, 0, 1)
            g.putcells(absorber, ypos+lane+toplane-abscur-1, ypos+toplane-abscur-2, 1, 0, 0, 1)
        else:
            #flipped variant
            g.putcells(corderships[phase], ypos+lane+botlane+cur, ypos+botlane+cur, 0, 1, 1, 0)
            g.putcells(absorber, ypos+lane+toplane-abscur-2, ypos+toplane-abscur-1, 0, 1, 1, 0)
        wasSide[side] = True
        prevside[side]=[ypos,lane,abscur]
        cur += delta

    absorberDeltas="---L---\n"+absorberDeltasS[0]+"---R---\n"+absorberDeltasS[1]
    g.save(f'Recipe{path}_{keep_best_n}_{left_lines}_{right_lines}.mc','mc',False)
    g.setbase(2)
    g.setstep(9)
    g.step()
    g.setstep(6)
    g.step()
    cur = delta
    for ship in unfolded[::-1]:
        lane, ypos, phase, side = ship
        if side == 0:
            g.putcells(cleanCordershipSeed, ypos+lane+botlane+cur+15, ypos+botlane+cur+36, 1, 0, 0, 1, "xor")
        else:
            #flipped variant
            g.putcells(cleanCordershipSeed, ypos+lane+botlane+cur+36, ypos+botlane+cur+15, 0, 1, 1, 0, "xor")
        cur += delta
    g.save(f'RRecipe{path}_{keep_best_n}_{left_lines}_{right_lines}.mc','mc',False)

    with open(f"fdump{path}_{keep_best_n}_{left_lines}_{right_lines}.txt", 'w') as f:
        f.write(str(rounds)+msg)
        f.write(str(best_sol))
        f.write(str(unfolded))
        f.write(str(absorberDeltas))

    keep_best_n = 2*keep_best_n
