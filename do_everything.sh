#!/bin/bash

git submodule update --init --recursive

set -e

python make_binary.py
python semilate.py
