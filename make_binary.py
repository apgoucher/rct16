# Mostly by Pavel Grankovskiy with some contributions by Adam P. Goucher and Vladan Majerech

#from original_arm import from_string, compile_recipe_greedy
import json
from collections import Counter

ecca_move_counts = Counter()


class BinaryRecipeContainer(object):
    '''
    Binary recipe container with O(1) append() and __len__() complexity.
    '''

    def __init__(self):

        self.fragments = ['']
        self.cumsum_lengths = [0]
        self.tags = []

    def __len__(self):

        return self.cumsum_lengths[-1]

    def append(self, bitstring):

        self.fragments.append(bitstring)
        self.cumsum_lengths.append(len(self) + len(bitstring))

    def tag(self, location, aeon, description, profile=True, **kwargs):
        self.tags.append(dict(
            pos=len(self),
            location=location,
            aeon=aeon,
            description=description,
            profile=profile,
            **kwargs))

    def text_tag(self, tag_txt):
        # formating of the text_tag is rather restricted
        loc_start_pos,loc_mid_pos,loc_end_pos = tag_txt.find("("),tag_txt.find(","),tag_txt.find(")")
        location = (int(tag_txt[1+loc_start_pos:loc_mid_pos]),int(tag_txt[1+loc_mid_pos:loc_end_pos]))
        aeon_start_pos = tag_txt.find(",",loc_end_pos)
        aeon_end_pos = tag_txt.find(",",aeon_start_pos+1)
        aeon = int(tag_txt[1+aeon_start_pos:aeon_end_pos].strip())
        descr_start_pos = tag_txt.find("'")
        descr_end_pos = tag_txt.find("'",descr_start_pos+1)
        description = tag_txt[1+descr_start_pos:descr_end_pos]
        optional_args = {}
        mag_start_pos = tag_txt.find("setmag={")
        if mag_start_pos>=0:
            mag_end_pos = tag_txt.find("}",mag_start_pos+1)
            optional_args["setmag"]=tag_txt[7+mag_start_pos:mag_end_pos+1]
        else:
            mag_start_pos = tag_txt.find("setmag=")
            if mag_start_pos>=0:
                mag_end_pos = tag_txt.find(",",mag_start_pos+1)
                optional_args["setmag"]="{"+tag_txt[7+mag_start_pos:mag_end_pos].strip()+"}"
        offset_start_pos = tag_txt.find("offset=(")
        if offset_start_pos>=0:
            offset_mid_pos,offset_end_pos = tag_txt.find(",",offset_start_pos),tag_txt.find(")",offset_start_pos)
            optional_args["offset"]=(int(tag_txt[8+offset_start_pos:offset_mid_pos]),int(tag_txt[1+offset_mid_pos:offset_end_pos]))
        time_offset=0
        time_offs_start_pos = tag_txt.find("time_offs=")
        if time_offs_start_pos>=0:
            time_offs_end_pos = tag_txt.find(",",time_offs_start_pos)
            time_offset=time_offset+(int(tag_txt[10+time_offs_start_pos:time_offs_end_pos]))
        bit_offs_start_pos = tag_txt.find("bit_offs=")
        if bit_offs_start_pos>=0:
            bit_offs_end_pos = tag_txt.find(",",bit_offs_start_pos)
            time_offset=time_offset+65536*(int(tag_txt[9+bit_offs_start_pos:bit_offs_end_pos]))
        optional_args["time_offset"]=time_offset
        istep_start_pos = tag_txt.find("istep=")
        if istep_start_pos>=0:
            istep_end_pos = tag_txt.find(",",istep_start_pos)
            istep=int(tag_txt[6+istep_start_pos:istep_end_pos])
            optional_args["istep"]=istep
        note_start_pos = tag_txt.find("note='")
        if note_start_pos>=0:
            note_end_pos = tag_txt.find("'",note_start_pos+6)
            optional_args["note"]=tag_txt[6+note_start_pos:note_end_pos]
        priority_start_pos = tag_txt.find("priority=") # required and last
        priority_end_pos = priority_start_pos+11
        priority = (int(tag_txt[9+priority_start_pos:priority_end_pos]))
        profile = priority == 0
        optional_args["priority"]=priority
        self.tag(location, aeon, description, profile=profile, **optional_args)

    def include_binary_file_recipe(self, filename):

        with open(f"bitSequences/{filename}") as f:
            for line in f:
                comment_start = line.find('--')
                binary_line, tag_txt = line, ''
                if comment_start>=0:
                    binary_line = line[:comment_start]
                    tag_end = line.rfind('-----')
                    if (tag_end > comment_start) and (comment_start == line.find('-----')):
                        tag_txt = line[comment_start:tag_end]

                # templates from the fixedBottom arm translation from the commented bit sequeences
                binary_line = binary_line.replace("P","1").replace("p","1").replace("q","1").replace("v","1")
                binary_line = binary_line.replace("B","11").replace("b","11")
                binary_line = binary_line.replace("S","111")
                binary_line = binary_line.replace("E","1111")
                binary_line = binary_line.replace(" ","").replace("\\","").replace("D","").replace("r","").replace("w","")
                binary_line = binary_line.replace("x","").replace("z","").replace("y","").replace("u","").replace("#","")
                binary_line = binary_line.replace("R","").replace("L","").replace("|","").replace(">","").strip()
                # now there should be only 1's and 2's in the binary_line
                if len(binary_line.replace("1","").replace("2",""))>0:
                    print("wrong binary line " + line + "->'" + binary_line + "'")

                self.append(binary_line)
                if len(tag_txt)>0:
                    self.text_tag(tag_txt)

    def join(self):

        joined = ''.join(self.fragments)
        self.fragments = [joined]
        self.cumsum_lengths = [len(joined)]
        return joined

    def ecca_flip_direction(self):

        prev = self.fragments[-1]
        self.fragments[-1] = prev[:-3] + chr(99 - ord(prev[-3])) + prev[-2:]

    def save_bitstring_file(self, filename):

        with open(filename, 'w') as f:
            f.write(self.join() + '\n')

    def append_strings(self, strings):

        self.append(from_string(' '.join(strings)))

    def print_profile(self):

        tags = [x for x in self.tags if x['profile']]
        total_length = len(self)
        starts = [x['pos'] for x in tags]
        ends = starts[1:] + [total_length]
        lengths = [y - x for (x, y) in zip(starts, ends)]
        descriptions = [x['description'] for x in tags]

        for (l, d) in zip(lengths, descriptions):

            print('%8d bits (%5.2f%s) : %s' % (l, (100.0 / total_length) * l, '%', d))

    def save_tags_file(self, filename):

        with open(filename, 'w') as f:
            json.dump(self.tags, f, indent=4)

def dbca_construction(recipes):
    '''
    Build Hippo.69's RISK DBCA using mainly the results of FixedBottom construction arm computations
    '''

    recipes.include_binary_file_recipe('00_CleanupToSStack.txt')
    recipes.include_binary_file_recipe('01_2398_LWSSSeed_LR.txt')
    recipes.include_binary_file_recipe('02_194_GliderSeed.txt')
    recipes.include_binary_file_recipe('03_1296_SyncedGliderPair.txt')
    recipes.include_binary_file_recipe('04_Boat2DBCASeed.txt')
    recipes.include_binary_file_recipe('05_StackReplaceBy3SAndIgnite.txt')

def dbca_steps(recipes):
    recipes.include_binary_file_recipe('06a_WhiteHandmade.txt')
    recipes.include_binary_file_recipe('DBCAbits_25_-1645_1e760.txt ')

def ecca_steps(recipes):
    recipes.include_binary_file_recipe('13a_SwitchingToECCAExtraData.txt')
    recipes.include_binary_file_recipe('ECCAbits_7_-441.txt')
    recipes.include_binary_file_recipe('19_Singularity_1s.txt')

class ECCAcompiler(object):

    def __init__(self, initial_direction):

        self.cur = 0
        self.direction = initial_direction

    def produce_glider(self, glider, recipes):

        phase, lane = {'e':0, 'o':1}[glider[0]], int(glider[1:])
        target = lane - self.cur
        if target%2 == 1:
            target -= 9
        target //= 2
        target *= self.direction
        if target < 0: # then previous instruction needs reversal
            recipes.ecca_flip_direction()
            self.direction *= -1
            target *= -1

        ecca_move_counts[target] += 1

        to_add = ''
        while target >= 7:
            to_add += '222'
            target -= 7
            self.cur += 14*self.direction
        to_add += bin(target^8)[3:].replace('1', '2').replace('0','1')
        self.cur += (target * self.direction-1)*2
        to_add += '1' # reverse bit defaults to 'no reverse'
        to_add += {(0, 0): '11',
                   (0, 1): '12',
                   (1, 1): '21',
                   (1, 0): '22'}[lane%2, phase]
        recipes.append(to_add)

    def compile_recipe(self, gliders, recipes, final_direction=1):

        for glider in gliders.split():
            self.produce_glider(glider, recipes)

        if self.direction != final_direction:
            recipes.ecca_flip_direction()
            self.direction *= -1


def read_recipe_from_file(filename, offset = 0):
    recipe = []
    with open(f"recipes/{filename}") as f:
        for line in f:
            if offset:
                cur = []
                for glider in line.split():
                    parity, lane = glider[0], glider[1:]
                    lane = int(lane) + offset
                    cur.append(f'{parity}{lane}')
                recipe.append(' '.join(cur))
            else:
                recipe.append(line.strip())
    return ' '.join(recipe)


def dbca_epicentre_steps(recipes):

    recipes.tag((0, 0), 0, 'The DBCA constructs some switching circuitry', offset=(768, -1536), setmag=-2)
    slow_gliders = []

    # reposition ECCA elbow switching helper block
    slow_gliders.append('e14 e15')
    # make GPSE-stopping block
    slow_gliders.append('e4 e1 o-6 o-23 o-21 o-33 o-33')
    # make DBCA elbow-switching OTTs, part 1, including SE corner
    slow_gliders.append(read_recipe_from_file('DBCA_elbow_switch_part_1.txt'))
    # transform blinker elbow to block
    slow_gliders.append('e50')
    # everything near p8 reflectors:
    # BSRD switching circuitry, p256 gun shutdown, target block for ECCA,
    # target block for N-glider absorber, target block for DBCA switching part 2,
    # ship for GPSE crash stray glider catching
    slow_gliders.append(read_recipe_from_file('BSRD_p8_switch_otts.txt'))
    recipes.append(dbca.compile_recipe(' '.join(slow_gliders)))

    # build ECCA
    recipes.tag((0, 0), 0, 'The DBCA constructs the extreme compression construction arm (ECCA)', offset=(1024, 0))
    recipes.append(dbca.compile_recipe(read_recipe_from_file('DBCA_build_ECCA.txt')))

    recipes.tag((0, 0), 0, 'The DBCA constructs some additional switching circuitry and jumps southeast', offset=(1024, -1024))
    slow_gliders = []
    # make N-glider absorber (placeholder for now)
    slow_gliders.append(read_recipe_from_file('N_glider_absorber.txt'))
    # make DBCA elbow-switching OTTs, part 2
    slow_gliders.append(read_recipe_from_file('DBCA_elbow_switch_part_2.txt'))
    # activate southeast elbow and remove epicentre elbow
    slow_gliders.append('e153')
    recipes.append(dbca.compile_recipe(' '.join(slow_gliders)))


def dbca_se_steps(recipes):

    dbca = DBCAcompiler()

    slow_gliders = []

    # transform GPSE launch ash into 2 blocks (for BSRD and for corderabsorbers)
    slow_gliders.append(read_recipe_from_file('SE_cleanup.txt'))
    # build SE BSRD part and corderabsorbers
    slow_gliders.append(read_recipe_from_file('SE_BSRD_build.txt', offset=68+6))
    slow_gliders.append(read_recipe_from_file('SE_corderabsorbers.txt', offset=192+6))
    # build longboat assisting SE elbow deletion
    slow_gliders.append(read_recipe_from_file('SE_longboat.txt', offset=192+6))

    recipes.append(dbca.compile_recipe(' '.join(slow_gliders)))

    # and activate it with odd-odd glider
    recipes.tag((0, 0), 0, 'The last glider-emission instruction activates switching circuitry to redirect future bits to the BSRD', offset=(768, -1536), profile=False)
    recipes.append(dbca.compile_recipe('o1775'))

def ecca_epicentre_steps(recipes):

    ecca = ECCAcompiler(1)

    recipes.tag((0, 0), 8, 'The ECCA destroys the remnants of the DBCA', offset=(512, -1024), setmag=-3)

    # remove eater, bi-block, p8 reflectors, and 4 uncomfortably close blocks
    slow_gliders = ['e310 o312 o328 e328 o318 e2640 e2658 e2666 e2674']
    slow_gliders.append(read_recipe_from_file('ECCA_DBCA_cleanup.txt'))
    ecca.compile_recipe(' '.join(slow_gliders), recipes)

    recipes.tag((0, 0), 8, 'The ECCA fires a glider to switch the direction of subsequent gliders')
    ecca.compile_recipe('e9', recipes)

    def do_it(filename, description, **kwargs):
        recipes.tag((0, 0), 8, description, **kwargs)
        ecca.compile_recipe(read_recipe_from_file(filename), recipes)

    do_it('ECCA_C_ash_cleanup.txt', 'The ECCA eliminates ash from the GPSE collision', offset=(0, 0), setmag=0)
    do_it('ECCA_produce_NW_elbow.txt', 'The ECCA builds an elbow in the far northwest')

    # SE corderships should start before SW cleanup and corderabsorbers with current recipes
    # otherwise we waste whopping 7 gliders on cleanup
    recipes.tag((1, 1), 20, 'The Corderships begin colliding with the SE Corderabsorbers', profile=False, setmag=-2)
    do_it('ECCA_SE_corderships.txt', 'The ECCA launches a salvo of Corderships to clean the south-east trail', setmag=0)
    recipes.tag((1, 1), 20, 'The Corderships have finished colliding with the SE Corderabsorbers', profile=False)

    recipes.tag((-1, 1), 12, 'The Corderabsorbers commence construction using slow^2 gliders', profile=False, setmag=0, offset=(400, -400))
    do_it('ECCA_SW_cleanup_and_corderabsorbers.txt', 'The ECCA builds some Corderabsorbers in the far south-west')
    recipes.tag((-1, 1), 12, 'The Corderabsorbers are fully complete', profile=False)

    recipes.tag((-1, 1), 20, 'The Corderships begin colliding with the SW Corderabsorbers', profile=False)
    do_it('ECCA_SW_corderships.txt', 'The ECCA launches a salvo of Corderships to clean the south-west trail', setmag=-2)

    recipes.tag((-1, -1), 20, 'The Corderships begin colliding with the NW Corderabsorbers', profile=False, setmag=-2)
    recipes.tag((0, 0), 24, 'The ECCA awaits the return glider before self-destructing and emitting an MWSS', profile=False, setmag=-2, offset=(1024, 0))
    do_it('ECCA_NW_corderships.txt', 'The ECCA launches a salvo of Corderships to clean the north-west trail')
    recipes.tag((-1, -1), 20, 'The last NW Cordership sends a return signal back to destroy the ECCA', profile=False, setmag=-30)
    recipes.tag((0, 0), 24, 'The MWSS triggers the ATBC seed', profile=False, setmag=-3, offset=(0, 0))

    # prepare elbow destroyer and build ATBC
    do_it('ECCA_elbow_destroyer.txt', 'The ECCA prepares circuitry to destroy its own elbow')
    do_it('ATBC.txt', 'The ECCA builds a seed for the ATBC')

    # destroy elbow
    ecca.compile_recipe('e320', recipes, -1)


def ecca_nw_steps(recipes):

    slow_gliders = []

    slow_gliders.append(read_recipe_from_file('ECCA_NW_cleanup_and_corderabsorbers.txt'))

    ecca = ECCAcompiler(-1)
    ecca.compile_recipe(' '.join(slow_gliders), recipes, -1)

if __name__ == '__main__':

    print('Compiling binary recipe together...')

    recipes = BinaryRecipeContainer()

    dbca_construction(recipes)
    dbca_steps(recipes)
    ecca_steps(recipes)
    #dbca_epicentre_steps(recipes)

    #recipes.tag((0, 0), 0, 'The DBCA constructs a BSRD and some Corderabsorbers in the far south-east')
    #recipes.tag((1, 1), 4, 'The BSRD and Corderabsorbers commence construction in the far south-east', profile=False, setmag=-2)
    #dbca_se_steps(recipes)
    #recipes.tag((1, 1), 4, 'The BSRD in the far south-east begins reflecting gliders', profile=False, offset=(768, -768), setmag=-3)

    #recipes.tag((0, 0), 0, 'The p8 reflectors are activated and subsequent gliders are rerouted to the BSRD', setmag=0, offset=(1024, -1024))
    #recipes.append('12')

    #recipes.tag((0, 0), 8, 'The ECCA reflects the first glider back to destroy the BSRD', setmag=-2, offset=(1024, 0))
    #recipes.append('1') # remove BSRD

    #recipes.tag((1, 1), 4, 'The BSRD continues reflecting gliders (grab a coffee and come back in an hour; we will wait for you)', profile=False, offset=(768, -768), setmag=-4)
    #ecca_epicentre_steps(recipes)

    #recipes.tag((0, 0), 8, 'The ECCA constructs Corderabsorbers in the far north-west')
    #recipes.tag((-1, -1), 12, 'The Corderabsorbers commence construction in the far north-west', setmag=-2, profile=False)
    #ecca_nw_steps(recipes)
    #recipes.tag((0, 0), 12, 'The Corderships continue cleaning the ash trails', setmag=-30, profile=False)

    # retract elbow and leave ECCA in known state for self-destruction
    #recipes.tag((0, 0), 8, 'The ECCA returns to a known state for self-destruction')
    #recipes.append('21221')

    #recipes.tag((1, 1), 4, 'The recipe returns from the BSRD to the ECCA', profile=False, setmag=-30, note='The BSRD finishes reflecting gliders! Press OK when ready')
    #recipes.tag((0, 0), 8, 'The Corderships embark on their journey to clean up the ash trails', profile=False, setmag=-30)

    print('Recipe length: %d bits' % len(recipes))

    #print(dict(sorted(ecca_move_counts.items())))

    recipes.print_profile()

    recipes.save_bitstring_file('binary.txt')
    recipes.save_tags_file('tags.json')