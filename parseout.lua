local g = golly()
local workDir="c:\\Golly\\Fleet\\"

local function inttostring(num)
  return string.sub(num,1,string.find(num..".","%.")-1)
end

local function setbox(dx,dy)
  for x=dx,dx+1 do
    for y=dy,dy+1 do
      g.setcell(x,y,g.getcell(x,y)+4)
    end
  end
end

local function slurm(dirname)
  g.show("slurm")
  local i = io.open(workDir..'slurm.out',"r")
  local level = 0
  local inputLine = i:read()
  local a = io.open(workDir..'stat.txt',"w")
  local lastGliders=0
  while inputLine do
    if string.find(inputLine,dirname) then
      a:write("\n")
    end
    if string.find(inputLine,'search complete') then
      a:write(lastGliders..";")
      lastGliders=0
    end
    local solPos=string.find(inputLine,'Found solution with ')
    if solPos then
      local glidersPos=string.find(inputLine,' gliders.')
      lastGliders=string.sub(inputLine,solPos+string.len('Found solution with '),glidersPos-1)
    end
    inputLine = i:read()
  end
  i:close()
  a:close()
end

local function out2BlinkerLevel(iname,oname)
  g.show(iname.."->"..oname)
  local i = io.open(iname,"r")
  if i then
    local o = io.open(oname,"w")
    local inputLine = i:read()
    local started=false
    while inputLine do
      if started then
        if not string.find(inputLine,"%[") then
          break
        end
        o:write(inputLine.."\n")
      else
        if string.find(inputLine,"Total saving = 0") then
          inputLine = i:read() -- skipping one line description
          started=true
        end
      end
      inputLine = i:read()
    end
    o:close()
    i:close()
  end
end

local Iprefix,Isuffix="c:\\cygwin64\\home\\maj\\workdir\\CSEL","_1.txt"
local Oprefix,Osuffix="c:\\Golly\\rct16Local\\15_ECCACleanup\\SEFleet\\BlinkerLevels",".txt"
local function FleetBlinkerLevel(level,outputlevel)
  outputlevel = outputlevel or level
  local iname,oname = Iprefix..inttostring(level)..Isuffix,Oprefix..inttostring(outputlevel)..Osuffix
  out2BlinkerLevel(iname,oname)
end

local function FleetBlinkerLevels(size,shipname,fleetname,outputoffset)
  outputoffset=outputoffset or 0
  Iprefix,Oprefix="c:\\cygwin64\\home\\maj\\workdir\\"..shipname,"c:\\Golly\\rct16Local\\15_ECCACleanup\\"..fleetname.."\\BlinkerLevels"
  for i=1,size do
    FleetBlinkerLevel(i, outputoffset+i)
  end
end

local function PatternTries()
  Iprefix,Oprefix="c:\\cygwin64\\home\\maj\\workdir\\Pattern","c:\\Golly\\rct16Local\\16_PatternSeed\\BlinkerLevels"
  Isuffix,Osuffix="Seed_1.txt",".txt"
  local tries="AEMQUWY"
  for i=1,string.len(tries) do
    local try=string.sub(tries,i,i)
    out2BlinkerLevel(Iprefix..try..Isuffix,Oprefix..try..Osuffix)
  end
end

local direction, side = "SW","L" --"CSEL","L"
local shipname, fleetname = "C"..direction..side, direction.."Fleet"
--slurm(shipname)
--out2BlinkerLevel("c:\\cygwin64\\home\\maj\\workdir\\SEAbsorbers_2.txt","c:\\Golly\\rct16Local\\12_FSECorderabsorbers\\BlinkerLevels.txt")
--FleetBlinkerLevels(17,shipname,fleetname)
--Isuffix = "_2.txt"
--FleetBlinkerLevels(22,shipname,fleetname)
local side = "L"
local shipname = "C"..direction..side
--slurm(shipname)
--FleetBlinkerLevels(18,shipname,fleetname)
PatternTries()


--slurm("FSWCorderabsorber")