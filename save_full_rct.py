
import os
import gzip
import lifelib

lt = lifelib.load_rules('b3s23').lifetree(n_layers=1, memory=4000)

nw="""719$645bobo$646b2o$646bo$649b2o$650b2o$649bo$656b3o$656bo$657bo144$785b3o$785bo$786bo!"""
sw="""77$708bo$706bobo$707b2o150$1043bo$1043bobo$1043b2o47$844bo$843bo$843b3o$836bo$837b2o$836b2o$833bo$833b2o$832bobo222$1041bo$1040b2o$1040bobo2$765b2o$766b2o$765bo!"""
se="""184$706bobo$706b2o$707bo236$492bo$493bo$491b3o$568bo$566b2o$567b2o69$571bo$570b2o$570bobo!"""

transformer = lambda x: lt.pattern(x)(-600, -600)

nw, sw, se = map(transformer, (nw, sw, se))

sw = sw(-480, 480)
se = se(160, 160)
nw = nw(-288, -288)


def rewind_gliders(p, n):
    '''
    If p is a pattern consisting exclusively of gliders, then this will
    rewind them by n generations.
    '''

    from functools import reduce
    c = p.components()
    c = [a[-n] for a in c]
    return reduce((lambda x, y : x + y), c)

# to ensure RCT16-equivalent collision site:
nw = rewind_gliders(nw, 768)(192, 192)
sw = rewind_gliders(sw, 1920)(480, -480)


def return_rct15(number):

    return nw(-number, -number) + se(number, number) + sw(-number, number)

def get_number(recipe):
    
    recipe += '111'
    
    number = '0b' + recipe.replace('2', '0')[::-1]
    number = int(number, base=2) * 96
    
    return number


def build_rct_mc(recipe, filename):

    number = get_number(recipe)
    
    mc_level = 14
    
    offset = number & ((1<<(mc_level-1))-1)
    small_patt = return_rct15(offset)
    
    temp_filename = 'temp.mc'
    small_patt.save(temp_filename)
    with open(temp_filename) as f:
        mc_start = list(filter(lambda x: x.strip(), f.readlines()))
    os.remove(temp_filename)
    
    mc_level, *mc_indices = map(int, mc_start[-1].split())
    assert(mc_indices[1] == 0)
    
    print('Writing', filename)
    with gzip.open(filename, 'wb', compresslevel=9) as f:
        
        def write(string):
            f.write(string.encode('utf-8'))
        
        write('[M2] (save_full_rct.py 1.0)\n')
        for line in mc_start[1:-1]:
            write(line)
        
        number >>= (mc_level-1)
        cur_index = max(mc_indices)+1
        
        while number:
            print(f'Bits left: {number.bit_length():<15}', end='\r')
            if number&1:
                nw_cell = (mc_level, mc_indices[0], 0, 0, 0)
                ne_cell = 0
                sw_cell = (mc_level, 0, 0, mc_indices[2], 0)
                se_cell = (mc_level, 0, 0, 0, mc_indices[3])
            else:
                nw_cell = (mc_level, 0, 0, 0, mc_indices[0])
                ne_cell = 0
                sw_cell = (mc_level, 0, mc_indices[2], 0, 0)
                se_cell = (mc_level, mc_indices[3], 0, 0, 0)
            for index, cell in enumerate((nw_cell, ne_cell, sw_cell, se_cell)):
                if cell:
                    write(' '.join(map(str, cell)) + '\n')
                    mc_indices[index] = cur_index
                    cur_index += 1
            number >>= 1
            mc_level += 1
        print()
        write(' '.join(map(str, [mc_level] + mc_indices)) + '\n')

def build_rct_lif(recipe, filename):
    
    number = get_number(recipe)
    
    print('Writing', filename)
    with gzip.open(filename, 'wb', compresslevel=9) as f:
        
        def write(string):
            f.write(string.encode('utf-8'))
        
        write('#Life 1.06\n')
        
        total = nw.population + sw.population + se.population
        cur = 0
        print(f"Wrote {cur}/{total}", end='\r')
        for x, y in nw.coords():
            x -= number
            y -= number
            write(f"{x} {y}\n")
            cur += 1
            print(f"Wrote {cur}/{total}", end='\r')
        for x, y in sw.coords():
            x -= number
            y += number
            write(f"{x} {y}\n")
            cur += 1
            print(f"Wrote {cur}/{total}", end='\r')
        for x, y in se.coords():
            x += number
            y += number
            write(f"{x} {y}\n")
            cur += 1
            print(f"Wrote {cur}/{total}", end='\r')
        print()


if __name__ == '__main__':

    with open('binary.txt') as f:
        recipe = f.read().strip()
    
    build_rct_mc(recipe, 'rct15_full.mc.gz')
    build_rct_lif(recipe, 'rct15_full.lif.gz')
