
import json
import lifelib

lt = lifelib.load_rules('b3s23').lifetree(n_layers=1, memory=4000)

nw="""719$645bobo$646b2o$646bo$649b2o$650b2o$649bo$656b3o$656bo$657bo144$785b3o$785bo$786bo!"""
ne="""577$615bo$614bo$614b3o!"""
sw="""77$708bo$706bobo$707b2o150$1043bo$1043bobo$1043b2o47$844bo$843bo$843b3o$836bo$837b2o$836b2o$833bo$833b2o$832bobo222$1041bo$1040b2o$1040bobo2$765b2o$766b2o$765bo!"""
se="""184$706bobo$706b2o$707bo236$492bo$493bo$491b3o$568bo$566b2o$567b2o69$571bo$570b2o$570bobo!"""

transformer = lambda x : lt.pattern(x)(-600, -600)

nw, ne, sw, se = map(transformer, (nw, ne, sw, se))

sw = sw(-480, 480)
se = se(160, 160)
nw = nw(-288, -288)

corner = lt.pattern('''x = 263, y = 262, rule = B3/S23
2bo$o3bo256bo$5bo254bo$o4bo254b3o$b5o$93b5o$92bo4bo$97bo$92bo3bo$94bo
161$257bo$256b3o$255b2obo$255b3o$255b3o$256b2o81$254bo$253b3o$253bob2o
$254b3o$254b3o$254b2o!''')

w1 = ne.replace(corner[200:, :60], corner[: 60, :])
s1 = ne.replace(corner[200:, :60], corner[:, 200:])
w2 = ne.replace(corner[200:, :60], corner[:160, :])
s2 = ne.replace(corner[200:, :60], corner[:, 100:])

transformer = lambda x : x[-1920]

w1, s1, w2, s2 = map(transformer, (w1, s1, w2, s2))


def rewind_gliders(p, n):
    '''
    If p is a pattern consisting exclusively of gliders, then this will
    rewind them by n generations.
    '''

    from functools import reduce
    c = p.components()
    c = [a[-n] for a in c]
    return reduce((lambda x, y : x + y), c)


# to ensure RCT16-equivalent collision site:
nw = rewind_gliders(nw, 768)(192, 192)
sw = rewind_gliders(sw, 1920)(480, -480)


def return_rct15(number):

    return nw(-number, -number) + se(number, number) + sw(-number, number)


#print(return_rct15(63 * 96).rle_string())
#exit(0)


def synthseg(x, log2spacing, gseg):

    if x in gseg:
        return gseg[x]

    l = len(x) >> 1
    d = l << (1 + log2spacing)

    inner = synthseg(x[:l], log2spacing, gseg)
    outer = synthseg(x[l:], log2spacing, gseg)

    p = (inner[0] + outer[0](-d, 0), inner[1] + outer[1](0, d))
    gseg[x] = p
    return p


def make_rct_semilator(recipe):

    # These three last bits don't get released, because the switch-engines
    # have already crashed at this point:
    recipe = recipe + '111'

    room = (len(recipe) + 8) * 65536

    for i in range(5, len(recipe)):

        number = int('0b' + (recipe[:2] + recipe[-i:]).replace('2', '0')[::-1], base=2) * 96

        if (number > room):
            recipe = recipe[2:-i]
            print('First %d and final %d bits are real; remaining %d are emulated' % (2, i - 3, len(recipe)))
            break

    log2spacing = 14

    translation = (number * 5) + 100000
    translation -= (translation & 4095)

    gseg = {'1': (w1(-translation, 0), s1(0, translation)),
            '2': (w2(-translation, 0), s2(0, translation))}

    print('# number = %d' % number)
    print('# log2spacing = %d' % log2spacing)
    print('# %d recipe bits emulated' % len(recipe))

    rct = lt.pattern('')
    segsize = 256

    for i in range(0, len(recipe), segsize):

        if ((i & 4095) == 0):
            print('%d / %d' % (i, len(recipe)), end='\r')

        d = (i << (log2spacing + 1))
        q = synthseg(recipe[i:i+segsize], log2spacing, gseg)
        rct += (q[0](-d, 0) + q[1](0, d))
    
    print('%d / %d' % (len(recipe), len(recipe)))

    rct += return_rct15(number)

    return rct, number

base_python = '''
number = %d
import golly as g
from time import sleep

g.setbase(2)
g.setpos("0","0")

false_literal = False
true_literal = True
g.autoupdate(true_literal)

def smoothmag(imag, fmag):

    while (imag != fmag):
        if imag < fmag:
            imag += 1
        else:
            imag -= 1
        g.setmag(imag)
        sleep(0.2)
    return imag

def run_for(n, istep=None, fstep=11, mags=None):

    if istep is None:
        istep = min(30, len(bin(n)) - 9)

    g.setstep(istep)
    if mags:
        currmag = mags[0]
        g.setmag(currmag)
        mags = mags[1:]

    while (n > 0):
        if (istep > fstep) and ((n >> istep) < 4):
            istep -= 4
            g.setstep(istep)
            if mags:
                currmag = smoothmag(currmag, mags[0])
                mags = mags[1:]
        n -= (1 << istep)
        g.step()

g.show("Fast-forwarding to first collision of gliders from GPSEs...")
run_for(number * 4 + 1280, 30, 6, [-30, -25, -20, -15, -10, -5, 0])
run_for(2560, 6, 6)
g.show("Fast-forwarding to first bit-reading gliders...")
run_for(number * 6 + 40000, 30)
'''


base_lua = '''
local number = %d
local g = golly()

g.setbase(2)
g.setpos("0","0")

local false_literal = false
local true_literal = true
g.autoupdate(true_literal)

local function smoothmag(imag, fmag)
    while imag ~= fmag do
        if imag < fmag then
            imag = imag + 1
        else
            imag = imag - 1
        end
        g.setmag(imag)
        g.sleep(200)
    end
    return imag
end

local function run_for(n, istep, fstep, mags)
    istep = istep or math.min(30, math.floor(math.log(n)/math.log(2))-6)
    fstep = fstep or 11
    g.setstep(istep)
    if mags then
        magindex = 1
        currmag = mags[magindex]
        g.setmag(currmag)
        magindex = magindex + 1
    end
    
    while n > 0 do
        if (istep > fstep) and (n/(2^istep) < 4) then
            istep = istep - 4
            g.setstep(istep)
            if mags and magindex <= #mags then
                currmag = smoothmag(currmag, mags[magindex])
                magindex = magindex + 1
            end
        end
        n = n - (2^istep)
        g.step()
    end
end

g.show("Fast-forwarding to first collision of gliders from GPSEs...")
run_for(number * 4 + 1280, 30, 6, {-30, -25, -20, -15, -10, -5, 0})
run_for(2560, 6, 6)
g.show("Fast-forwarding to first bit-reading gliders...")
run_for(number * 6 + 40000, 30)
'''


def write_script(filename, number, tags):

    initial_offset = number * 10 + 43840

    aeon_length = number + 96

    tags = sorted([(t['aeon'] * aeon_length + t['pos'] * 65536, i, t) for (i, t) in enumerate(tags)])

    language = filename.split('.')[-1]

    thing = {'py': base_python, 'lua': base_lua}[language]
    comment = {'py': '#', 'lua': '--'}[language]

    with open(filename, 'w') as f:
        f.write(thing % number)

        curr_ts = 0
        curr_loc = (0, 0)

        for (ts, _, tag) in tags:

            ts_diff = ts - curr_ts
            curr_ts = ts

            if ts_diff > 0:
                if 'note' in tag:
                    f.write('run_for(%d, 24)\n' % ts_diff)
                else:
                    f.write('run_for(%d)\n' % ts_diff)

            f.write(comment + ' generation ' + str(initial_offset + ts) + '\n')

            if 'note' in tag:
                f.write('g.note("%s", false_literal)\n' % tag['note'])

            if 'setmag' in tag:
                f.write('g.setmag(%d)\n' % tag['setmag'])

            new_loc = tuple(tag['location'])
            if (new_loc != curr_loc) or ('offset' in tag):
                coords = tuple([x * aeon_length + y for (x, y) in zip(new_loc, tag.get('offset', (0, 0)))])
                f.write('g.setpos("%d", "%d")\n' % coords)

            curr_loc = new_loc

            f.write('g.show("%s")\n' % tag['description'])

if __name__ == '__main__':

    with open('binary.txt') as f:
        recipe = f.read().strip()
    recipe = ''.join([c for c in recipe if c in '12'])

    rct, number = make_rct_semilator(recipe)

    print('Loading tags')
    with open('tags.json') as f:
        tags = json.load(f)

    print('Writing rct15.mc')
    rct.save('rct15.mc')

    print('Creating Golly scripts')
    write_script('dvgrn.py', number, tags)
    write_script('dvgrn.lua', number, tags)

