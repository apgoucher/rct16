
import json
import lifelib

lt = lifelib.load_rules('b3s23').lifetree(n_layers=1, memory=4000)

nw="""719$645bobo$646b2o$646bo$649b2o$650b2o$649bo$656b3o$656bo$657bo144$785b3o$785bo$786bo!"""
ne="""577$615bo$614bo$614b3o!"""
sw="""77$708bo$706bobo$707b2o150$1043bo$1043bobo$1043b2o47$844bo$843bo$843b3o$836bo$837b2o$836b2o$833bo$833b2o$832bobo222$1041bo$1040b2o$1040bobo2$765b2o$766b2o$765bo!"""
se="""184$706bobo$706b2o$707bo236$492bo$493bo$491b3o$568bo$566b2o$567b2o69$571bo$570b2o$570bobo!"""

transformer = lambda x : lt.pattern(x)(-600, -600)

nw, ne, sw, se = map(transformer, (nw, ne, sw, se))

sw = sw(-480, 480)
se = se(160, 160)
nw = nw(-288, -288)

corner = lt.pattern('''x = 263, y = 262, rule = B3/S23
2bo$o3bo256bo$5bo254bo$o4bo254b3o$b5o$93b5o$92bo4bo$97bo$92bo3bo$94bo
161$257bo$256b3o$255b2obo$255b3o$255b3o$256b2o81$254bo$253b3o$253bob2o
$254b3o$254b3o$254b2o!''')

w1 = ne.replace(corner[200:, :60], corner[: 60, :])
s1 = ne.replace(corner[200:, :60], corner[:, 200:])
w2 = ne.replace(corner[200:, :60], corner[:160, :])
s2 = ne.replace(corner[200:, :60], corner[:, 100:])

transformer = lambda x : x[-1920]

w1, s1, w2, s2 = map(transformer, (w1, s1, w2, s2))


def rewind_gliders(p, n):
    '''
    If p is a pattern consisting exclusively of gliders, then this will
    rewind them by n generations.
    '''

    from functools import reduce
    c = p.components()
    c = [a[-n] for a in c]
    return reduce((lambda x, y : x + y), c)


# to ensure RCT16-equivalent collision site:
nw = rewind_gliders(nw, 768)(192, 192)
sw = rewind_gliders(sw, 1920)(480, -480)


def return_rct15(number):

    return nw(-number, -number) + se(number, number) + sw(-number, number)


#print(return_rct15(63 * 96).rle_string())
#exit(0)


def synthseg(x, log2spacing, gseg):

    #print('# len x = %d' % len(x))
    if x in gseg:
        return gseg[x]

    l = len(x) >> 1
    d = l << (1 + log2spacing)

    inner = synthseg(x[:l], log2spacing, gseg)
    outer = synthseg(x[l:], log2spacing, gseg)

    p = (inner[0] + outer[0](-d, 0), inner[1] + outer[1](0, d))
    gseg[x] = p
    return p


def make_rct_semilator(recipe):

    room = (len(recipe) + 8) * 65536

    for i in range(1, len(recipe)):

        number = int('0b' + (recipe[:2] + recipe[-i:]).replace('2', '0')[::-1], base=2) * 96

        if (number > room):
            recipe = recipe[2:-i]
            break

    log2spacing = 14

    translation = (number * 5) + 100000
    translation -= (translation & 4095)

    gseg = {'1': (w1(-translation, 0), s1(0, translation)),
            '2': (w2(-translation, 0), s2(0, translation))}

    print('# number = %d' % number)
    print('# log2spacing = %d' % log2spacing)
    print('# %d recipe bits emulated' % len(recipe))

    rct = lt.pattern('')
    segsize = 256

    for i in range(0, len(recipe), segsize):

        if ((i & 4095) == 0):
            print('%d / %d' % (i, len(recipe)), end='\r')

        d = (i << (log2spacing + 1))
        q = synthseg(recipe[i:i+segsize], log2spacing, gseg)
        off=0
        #64 would correspond to start by bit 2 what is forbidden
        rct += (q[0](-d-off, 0) + q[1](0, d+off))
    
    print('%d / %d' % (len(recipe), len(recipe)))

    rct += return_rct15(number)

    return rct, number

base_python = '''
number = %d
import golly as g
from time import sleep

g.setbase(2)
g.setpos("0","0")

false_literal = False
true_literal = True
g.autoupdate(true_literal)

def smoothmag(imag, fmag):

    while (imag != fmag):
        if imag < fmag:
            imag += 1
        else:
            imag -= 1
        g.setmag(imag)
        sleep(0.2)
    return imag

def run_for(n, istep=None, fstep=11, mags=None):

    if istep is None:
        istep = min(30, len(bin(n)) - 9)

    g.setstep(istep)
    if mags:
        currmag = mags[0]
        g.setmag(currmag)
        mags = mags[1:]

    while (n > 0):
        if (istep > fstep) and ((n >> istep) < 4):
            istep -= 4
            g.setstep(istep)
            if mags:
                currmag = smoothmag(currmag, mags[0])
                mags = mags[1:]
        n -= (1 << istep)
        g.step()

g.show("Fast-forwarding to first collision of gliders from GPSEs...")
run_for(number * 4 + 1280, 30, 6, [-30, -25, -20, -15, -10, -5, 0])
run_for(2560, 6, 6)
g.show("Fast-forwarding to first bit-reading gliders...")
run_for(number * 6 + 40000, 30)
'''


base_lua = '''
local showtext, showtextbase = "", ""
local g = golly()

-----------------------------------------------------
local RADIX_LEN = 6 ;
local RADIX_FIL = "000000" ; -- length >= RADIX_LEN
local RADIX = 10^RADIX_LEN ;

BigNum = {} ;
BigNum.mt = {} ;

function BigNum.new( num )
   local bignum = {} ;
   setmetatable( bignum , BigNum.mt ) ;
   BigNum.change( bignum , num ) ;
   return bignum ;
end

function BigNum.mt.sub( num1 , num2 )
   local temp = BigNum.new() ;
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   BigNum.sub( bnum1 , bnum2 , temp ) ;
   return temp ;
end

function BigNum.mt.add( num1 , num2 )
   local temp = BigNum.new() ;
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   BigNum.add( bnum1 , bnum2 , temp ) ;
   return temp ;
end

function BigNum.mt.tostring( bnum )
   local i = 0
   local j
   local temp = ""
   local str
   local dotpos
   if bnum == nil then
      return "nil"
   elseif bnum.len > 0 then
      for i = bnum.len - 2 , 0 , -1  do
         str = tostring(bnum[i])
         dotpos = string.find(str..".","%%.")
         temp = temp .. string.sub(RADIX_FIL..string.sub(str,1,dotpos-1),-RADIX_LEN)
      end
      str = tostring(bnum[bnum.len - 1])
      dotpos = string.find(str..".","%%.")
      temp = string.sub(str,1,dotpos-1) .. temp
      if bnum.signal == '-' then
         temp = bnum.signal .. temp
      end
      return temp
   else
      return ""
   end
end

function BigNum.mt.eq( num1 , num2 )
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   return BigNum.eq( bnum1 , bnum2 ) ;
end

function BigNum.mt.lt( num1 , num2 )
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   return BigNum.lt( bnum1 , bnum2 ) ;
end

function BigNum.mt.le( num1 , num2 )
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   return BigNum.le( bnum1 , bnum2 ) ;
end

function BigNum.mt.unm( num )
   local ret = BigNum.new( num )
   if ret.signal == "+" then
      ret.signal = "-"
   else
      ret.signal = "+"
   end
   return ret
end

function BigNum.mt.half( num )
   local ret = BigNum.new()
   local bnum = BigNum.new( num ) ;
   BigNum.half( bnum , ret ) ;
   return ret ;
end

BigNum.mt.__metatable = "hidden"
BigNum.mt.__tostring  = BigNum.mt.tostring ;
BigNum.mt.__add = BigNum.mt.add ;
BigNum.mt.__sub = BigNum.mt.sub ;
BigNum.mt.__unm = BigNum.mt.unm ;
BigNum.mt.__eq = BigNum.mt.eq   ;
BigNum.mt.__le = BigNum.mt.le   ;
BigNum.mt.__lt = BigNum.mt.lt   ;

setmetatable( BigNum.mt, { __index = "inexistent field", __newindex = "not available", __metatable="hidden" } ) ;

function BigNum.add( bnum1 , bnum2 , bnum3 )
   local maxlen = 0 ;
   local i = 0 ;
   local carry = 0 ;
   local signal = "+" ;
   local old_len = 0 ;
   if bnum1 == nil or bnum2 == nil or bnum3 == nil then
      error("Function BigNum.add: parameter nil") ;
   elseif bnum1.signal == "-" and bnum2.signal == "+" then
      bnum1.signal = "+" ;
      BigNum.sub( bnum2 , bnum1 , bnum3 ) ;

      if not rawequal(bnum1, bnum3) then
         bnum1.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "+" and bnum2.signal == "-" then
      bnum2.signal = "+" ;
      BigNum.sub( bnum1 , bnum2 , bnum3 ) ;
      if not rawequal(bnum2, bnum3) then
         bnum2.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "-" and bnum2.signal == "-" then
      signal = "-" ;
   end
   old_len = bnum3.len ;
   if bnum1.len > bnum2.len then
      maxlen = bnum1.len ;
   else
      maxlen = bnum2.len ;
      bnum1 , bnum2 = bnum2 , bnum1 ;
   end
   for i = 0 , maxlen - 1 do
      if bnum2[i] ~= nil then
         bnum3[i] = bnum1[i] + bnum2[i] + carry ;
      else
         bnum3[i] = bnum1[i] + carry ;
      end
      if bnum3[i] >= RADIX then
         bnum3[i] = bnum3[i] - RADIX ;
         carry = 1 ;
      else
         carry = 0 ;
      end
   end
   if carry == 1 then
      bnum3[maxlen] = 1 ;
   end
   bnum3.len = maxlen + carry ;
   bnum3.signal = signal ;
   for i = bnum3.len, old_len do
      bnum3[i] = nil ;
   end
   return 0 ;
end

function BigNum.sub( bnum1 , bnum2 , bnum3 )
   local maxlen = 0 ;
   local i = 0 ;
   local carry = 0 ;
   local old_len = 0 ;

   if bnum1 == nil or bnum2 == nil or bnum3 == nil then
      error("Function BigNum.sub: parameter nil") ;
   elseif bnum1.signal == "-" and bnum2.signal == "+" then
      bnum1.signal = "+" ;
      BigNum.add( bnum1 , bnum2 , bnum3 ) ;
      bnum3.signal = "-" ;
      if not rawequal(bnum1, bnum3) then
         bnum1.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "-" and bnum2.signal == "-" then
      bnum1.signal = "+" ;
      bnum2.signal = "+" ;
      BigNum.sub( bnum2, bnum1 , bnum3 ) ;
      if not rawequal(bnum1, bnum3) then
         bnum1.signal = "-" ;
      end
      if not rawequal(bnum2, bnum3) then
         bnum2.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "+" and bnum2.signal == "-" then
      bnum2.signal = "+" ;
      BigNum.add( bnum1 , bnum2 , bnum3 ) ;
      if not rawequal(bnum2, bnum3) then
         bnum2.signal = "-" ;
      end
      return 0 ;
   end
   if BigNum.compareAbs( bnum1 , bnum2 ) == 2 then
      BigNum.sub( bnum2 , bnum1 , bnum3 ) ;
      bnum3.signal = "-" ;
      return 0 ;
   else
      maxlen = bnum1.len ;
   end
   old_len = bnum3.len ;
   bnum3.len = 0 ;
   for i = 0 , maxlen - 1 do
      if bnum2[i] ~= nil then
         bnum3[i] = bnum1[i] - bnum2[i] - carry ;
      else
         bnum3[i] = bnum1[i] - carry ;
      end
      if bnum3[i] < 0 then
         bnum3[i] = RADIX + bnum3[i] ;
         carry = 1 ;
      else
         carry = 0 ;
      end

      if bnum3[i] ~= 0 then
         bnum3.len = i + 1 ;
      end
   end
   bnum3.signal = "+" ;
   if bnum3.len == 0 then
      bnum3.len = 1 ;
      bnum3[0]  = 0 ;
   end
   if carry == 1 then
      error( "Error in function sub" ) ;
   end
   for i = bnum3.len , max( old_len , maxlen - 1 ) do
      bnum3[i] = nil ;
   end
   return 0 ;
end

function BigNum.half( bnum1 , bnum2 )
   local maxlen = bnum1.len ;
   local i = 0 ;
   if bnum1 == nil or bnum2 == nil then
      error("Function BigNum.half: parameter nil") ;
   end
   for i = maxlen, bnum2.len-1, 1 do
      bnum2[i] = nil ;
   end
   bnum2.len = maxlen
   local carry = 0
   if bnum1[maxlen-1]<2 then
     bnum2[maxlen-1]=nil
     bnum2.len=maxlen-1
     carry = bnum1[maxlen-1] %% 2
   end
   for i = bnum2.len - 1,0,-1 do
     bnum2[i] = (bnum1[i] + carry * RADIX)//2
     carry = bnum1[i] %% 2
   end
   bnum2.signal = bnum1.signal ;
   if bnum2.len == 0 then
      bnum2.len = 1 ;
      bnum2[0]  = 0 ;
   end
   return carry == 1 ;
end

function BigNum.eq( bnum1 , bnum2 )
   if BigNum.compare( bnum1 , bnum2 ) == 0 then
      return true ;
   else
      return false ;
   end
end

function BigNum.lt( bnum1 , bnum2 )
   if BigNum.compare( bnum1 , bnum2 ) == 2 then
      return true ;
   else
      return false ;
   end
end

function BigNum.le( bnum1 , bnum2 )
   local temp = -1 ;
   temp = BigNum.compare( bnum1 , bnum2 )
   if temp == 0 or temp == 2 then
      return true ;
   else
      return false ;
   end
end

function BigNum.compareAbs( bnum1 , bnum2 )
   if bnum1 == nil or bnum2 == nil then
      error("Function compare: parameter nil") ;
   elseif bnum1.len > bnum2.len then
      return 1 ;
   elseif bnum1.len < bnum2.len then
      return 2 ;
   else
      local i ;
      for i = bnum1.len - 1 , 0 , -1 do
         if bnum1[i] > bnum2[i] then
            return 1 ;
         elseif bnum1[i] < bnum2[i] then
            return 2 ;
         end
      end
   end
   return 0 ;
end

function BigNum.compare( bnum1 , bnum2 )
   local signal = 0 ;

   if bnum1 == nil or bnum2 == nil then
      error("Funtion BigNum.compare: parameter nil") ;
   elseif bnum1.signal == "+" and bnum2.signal == "-" then
      return 1 ;
   elseif bnum1.signal == "-" and bnum2.signal == "+" then
      return 2 ;
   elseif bnum1.signal == "-" and bnum2.signal == "-" then
      signal = 1 ;
   end
   if bnum1.len > bnum2.len then
      return 1 + signal ;
   elseif bnum1.len < bnum2.len then
      return 2 - signal ;
   else
      local i ;
      for i = bnum1.len - 1 , 0 , -1 do
         if bnum1[i] > bnum2[i] then
            return 1 + signal ;
	 elseif bnum1[i] < bnum2[i] then
	    return 2 - signal ;
	 end
      end
   end
   return 0 ;
end

function BigNum.copy( bnum1 , bnum2 )
   if bnum1 ~= nil and bnum2 ~= nil then
      local i ;
      for i = 0 , bnum1.len - 1 do
         bnum2[i] = bnum1[i] ;
      end
      bnum2.len = bnum1.len ;
   else
      error("Function BigNum.copy: parameter nil") ;
   end
end

function BigNum.change( bnum1 , num )
   local j = 0 ;
   local len = 0  ;
   local num = num ;
   local l ;
   local oldLen = 0 ;
   if bnum1 == nil then
      error( "BigNum.change: parameter nil" ) ;
   elseif type( bnum1 ) ~= "table" then
      error( "BigNum.change: parameter error, type unexpected" ) ;
   elseif num == nil then
      bnum1.len = 1 ;
      bnum1[0] = 0 ;
      bnum1.signal = "+";
   elseif type( num ) == "table" and num.len ~= nil then  --check if num is a big number
      for i = 0 , num.len do
         bnum1[i] = num[i] ;
      end
      if num.signal ~= "-" and num.signal ~= "+" then
         bnum1.signal = "+" ;
      else
         bnum1.signal = num.signal ;
      end
      oldLen = bnum1.len ;
      bnum1.len = num.len ;
   elseif type( num ) == "string" or type( num ) == "number" then
      if string.sub( num , 1 , 1 ) == "+" or string.sub( num , 1 , 1 ) == "-" then
         bnum1.signal = string.sub( num , 1 , 1 ) ;
         num = string.sub(num, 2) ;
      else
         bnum1.signal = "+" ;
      end
      num = string.gsub( num , " " , "" )
      local sf = string.find( num , "e" )
      if sf ~= nil then
         local e = string.sub( num , sf + 1 )
         num = string.sub( num , 1 , sf - 1 )
         e = tonumber(e) ;
         if e ~= nil and e > 0 then
            e = tonumber(e) ;
         else
            error( "Function BigNum.change: string is not a valid number" ) ;
         end
         local dotpos=string.find(num..".","%%.")
         num = string.gsub( num , "%%." , "" )
         local dotshift = string.len(num)+1-dotpos
         for i = 1 , e do
            num = num .. "0"
         end
         if dotshift>0 then num = string.sub(num,1,-dotshift-1) end
      else
         sf = string.find( num , "%%." ) ;
         if sf ~= nil then
            num = string.sub( num , 1 , sf - 1 ) ;
         end
      end

      l = string.len( num ) ;
      oldLen = bnum1.len ;
      if (l > RADIX_LEN) then
         local mod = l-( math.floor( l / RADIX_LEN ) * RADIX_LEN ) ;
         for i = 1 , l-mod, RADIX_LEN do
            bnum1[j] = tonumber( string.sub( num, -( i + RADIX_LEN - 1 ) , -i ) );
            --Check if string dosn't represents a number
            if bnum1[j] == nil then
               error( "Function BigNum.change: string is not a valid number" ) ;
               bnum1.len = 0 ;
               return 1 ;
            end
            j = j + 1 ;
            len = len + 1 ;
         end
         if (mod ~= 0) then
            bnum1[j] = tonumber( string.sub( num , 1 , mod ) ) ;
            bnum1.len = len + 1 ;
         else
            bnum1.len = len ;
         end
         for i = bnum1.len - 1 , 1 , -1 do
            if bnum1[i] == 0 then
               bnum1[i] = nil ;
               bnum1.len = bnum1.len - 1 ;
            else
               break ;
            end
         end

      else
         bnum1[j] = tonumber( num ) ;
         bnum1.len = 1 ;
      end
   else
      error( "Function BigNum.change: parameter error, type unexpected" ) ;
   end

   if oldLen ~= nil then
      for i = bnum1.len , oldLen do
         bnum1[i] = nil ;
      end
   end

   return 0 ;
end

function BigNum.put( bnum , int , pos )
   if bnum == nil then
      error("Function BigNum.put: parameter nil") ;
   end
   local i = 0 ;
   for i = 0 , pos - 1 do
      bnum[i] = 0 ;
   end
   bnum[pos] = int ;
   for i = pos + 1 , bnum.len do
      bnum[i] = nil ;
   end
   bnum.len = pos ;
   return 0 ;
end

function printraw( bnum )
   local i = 0 ;
   if bnum == nil then
      error( "Function printraw: parameter nil" ) ;
   end
   while 1 == 1 do
      if bnum[i] == nil then
         io.write( " len "..bnum.len ) ;
         if i ~= bnum.len then
            io.write( " ERROR! " ) ;
         end
         io.write( "\\n" ) ;
         return 0 ;
      end
      io.write( "r"..bnum[i] ) ;
      i = i + 1 ;
   end
end

function max( int1 , int2 )
   if int1 > int2 then
      return int1 ;
   else
      return int2 ;
   end
end

function decr( bnum1 )
   local temp = {} ;
   temp = BigNum.new( "1" ) ;
   BigNum.sub( bnum1 , temp , bnum1 ) ;
   return 0 ;
end
---------------------------------------------

g.setbase(2)
g.setpos("0","0")

local false_literal = false
local true_literal = true
local bnParsec = BigNum.new("%d")
local bnParsecHalf = BigNum.mt.half(bnParsec)
local bnParsecQuarter = BigNum.mt.half(bnParsecHalf)
local bnAeon=bnParsec                   -- /c
local bnAeonHalf = bnParsecHalf         --/c  (c=1)
local bnTwoAeons=bnAeon+bnAeon          -- maybe I have to implement multiplication and division:)
local bnFourAeons=bnTwoAeons+bnTwoAeons -- -"-
local startGen = g.getgen()
local bnZero = BigNum.new(0)
local currentAeon = 0
local bnCurrentAeonBase = bnZero
local bnCurrentOffset
local nrGliderGPSECollisionsToShow = 10 -- 26 is maximum displayed, whatever bigger will be interrupted (there are 28 bits in total)
local lastfstep=11

while startGen>bnCurrentAeonBase + bnAeonHalf do
  bnCurrentAeonBase = bnCurrentAeonBase + bnAeon
  currentAeon = currentAeon + 1
end
local startAeon = currentAeon

g.autoupdate(true_literal)

local function smoothmag(imag, fmag)
    while imag ~= fmag do
        if imag < fmag then
            imag = imag + 1
        else
            imag = imag - 1
        end
        g.setmag(imag)
        g.sleep(200)
    end
    return imag
end

local function run_for_to(n, targetAeon, targetgen, istep, fstep, mags, note)
    local bnTargetGen
    local bnN
    local magindex
    local enterAeon = currentAeon
    fstep = fstep or 11
    if targetAeon~=nil then
      if targetAeon+0>100 then
        targetAeon = nil
      end
    end
    if (targetgen ~= nil) and targetAeon ~= nil then
    	bnTargetGen = BigNum.new(targetgen)
        if targetAeon<currentAeon then
--          g.note(showtext.." - target Aeon smaller than current one ")
            return
        end
        while currentAeon < targetAeon do
            bnCurrentAeonBase = bnCurrentAeonBase + bnAeon
            currentAeon = currentAeon + 1
        end
    end
    if (targetgen == nil or targetAeon==nil) and (n == nil) then
        g.note(showtext.." - run_for_to with unknown duration")
        return
    end
    bnCurrentOffset=BigNum.new(g.getgen()) - bnCurrentAeonBase
    local bnStartGen=startGen - bnCurrentAeonBase
    local bnEnterGen=bnCurrentOffset
    if n ~= nil then
        bnN = BigNum.new(n)
    end
    if (targetgen ~= nil) and (targetAeon~=nil) and (n ~= nil) then
       if bnN+bnEnterGen ~= bnTargetGen  then
          if bnStartGen > bnTargetGen then return end -- skipping milestones to restart
          if bnStartGen + bnN < bnTargetGen then -- must be script error
             if bnN+bnEnterGen > bnTargetGen+BigNum.new(2^lastfstep) then
            	g.note(showtext.."("..currentAeon..") : - run_for_to for not matching to "..BigNum.mt.tostring(bnEnterGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen))
            	return
             end
          end
          --g.note(showtext.."("..currentAeon..") : "..BigNum.mt.tostring(bnGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen).." running to first milestone after restart")
          bnN = bnTargetGen - bnEnterGen
       end
    end
    if (n==nil) then -- helper for editting at the end
	bnN = bnTargetGen - bnEnterGen
      --g.note(showtext.." : "..BigNum.mt.tostring(bnGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen))
    end
    if (targetgen == nil or targetAeon == nil) then -- helper for editting
      bnTargetGen = bnN + bnEnterGen
      while bnTargetGen>bnAeonHalf do
        bnCurrentAeonBase = bnCurrentAeonBase + bnAeon
        currentAeon = currentAeon + 1
        bnTargetGen = bnTargetGen - bnAeon
        bnEnterGen = bnEnterGen - bnAeon
      end
      g.setclipstr(\'run_for_to("\'..BigNum.mt.tostring(bnN)..\'", \'..currentAeon..\', "\'..BigNum.mt.tostring(bnTargetGen)..\'"\')
      g.note(showtext.."("..currentAeon..") : "..BigNum.mt.tostring(bnEnterGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen))
    end
    local bnGen
    local sTargetGen = BigNum.mt.tostring(bnTargetGen)
    if bnN<=0 then return end
    local nn=BigNum.mt.tostring(bnN)
    istep = istep or math.min(30, math.floor(math.log(nn)/math.log(2))-6)
    local cstep = g.getstep()
    if cstep<fstep then cstep = fstep end
    if cstep>istep then cstep = istep end
    if mags then
        magindex = 1
        currmag = mags[magindex]
        g.setmag(currmag)
        magindex = magindex + 1
    end
    local bn3Eistep = BigNum.new(3*2^istep)
    g.setstep(cstep)
    while bnN > bnZero do
        g.show(showtext.."("..currentAeon..") : "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen).." i"..istep.." f"..fstep)

        while (istep > fstep) and (bnN < bn3Eistep) do
            istep = istep - 2
            if istep < 0 then istep = 0 end
            bn3Eistep = BigNum.new(3*2^istep)
            cstep = istep
            if mags and magindex <= #mags then
                currmag = smoothmag(currmag, mags[magindex])
                magindex = magindex + 1
            end
            g.setstep(cstep)
        end
        g.step()
        bnGen=BigNum.new(g.getgen()) - bnCurrentAeonBase
        bnN = bnTargetGen - bnGen
        cstep = g.getstep()
        if (cstep<istep) then -- owerwriting manual slowdown
          cstep = cstep+1
          g.setstep(cstep)
        end
        if (cstep>istep) then -- owerwriting manual speedup
          cstep = cstep-1
          g.setstep(cstep)
        end
    end
    lastfstep = fstep
    if (n==nil) and (enterAeon==currentAeon) then
      -- no explicit duration when Aeon boundary is crossed
      bnN=bnGen-bnEnterGen
      g.setclipstr(\'run_for_to("\'..BigNum.mt.tostring(bnN)..\'", \'..currentAeon..\', "\'..BigNum.mt.tostring(bnTargetGen)..\'"\')
      g.note(showtext.."("..currentAeon..") : ".." final generation: "..BigNum.mt.tostring(bnEnterGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnGen))
    end
    if note~=nil then
      g.note(note)
    end
end

local function run_from_to(bnN, targetAeon, bnTargetGen, istep, fstep, mags)
--bnTargetgen big number not nil (bnN==nil if to new Aeon)
  if bnN==nil then
    --g.note("T:"..BigNum.mt.tostring(bnTargetGen).." D:nil")
    run_for_to(nil, targetAeon, BigNum.mt.tostring(bnTargetGen), istep, fstep, mags)
  else
    local bnDuration = bnTargetGen - bnN
    --g.note("T:"..BigNum.mt.tostring(bnTargetGen).." D:"..BigNum.mt.tostring(bnDuration))
    run_for_to(BigNum.mt.tostring(bnDuration), targetAeon, BigNum.mt.tostring(bnTargetGen), istep, fstep, mags)
  end
end

local function distance_bits(prevTime)
  -- somehow when interrupted, restart skips to "remaining bits read"
  -- ... I should check why it happens, but it is method how to speed the show up,
  -- so it looks like desired feature.
  local bngcCollisionPos = bnParsecQuarter+0
  local bngcCollisionTime = -bnAeon-bnTwoAeons
  local collisionAeon=10 -- 3rd collision only
  local bnReadTime = -bnTwoAeons
  local gnPrevTime = BigNum.new(prevTime)
  local bngcCollisionTimeOffs
  local bnReadTimeOffs
  local i
  -- 12-8 12-4 12-2   12-1     12-0.5
  -- 12-6 12-3 12-1.5 12-0.75
  for i=3,nrGliderGPSECollisionsToShow,1 do
    bngcCollisionPos=BigNum.mt.half(bngcCollisionPos)
    bngcCollisionTime=BigNum.mt.half(bngcCollisionTime)
    bnReadTime=BigNum.mt.half(bnReadTime)
    if bnReadTime>-22000 then
      -- remaining bits should be processed extra
      break
    end
    bngcCollisionTimeOffs=bngcCollisionTime+bnAeon
    bnReadTimeOffs=bnReadTime+bnAeon
    if i==3 then -- Aeon exception
      bngcCollisionTimeOffs=bngcCollisionTimeOffs+bnAeon
    end
    g.setpos(BigNum.mt.tostring(bngcCollisionPos), BigNum.mt.tostring(bngcCollisionPos))
    showtext="Fast-forwarding to FSE glider with GPSE collision nr. "..i
    run_from_to(gnPrevTime, collisionAeon, bngcCollisionTimeOffs+2048, 30, 0, {-30,-27,-24,-21,-18,-16,-14,-12,-10,-8,-6,-4,-2,0})
    gnPrevTime=bngcCollisionTimeOffs+4096
    run_for_to("2048", collisionAeon, gnPrevTime, nil, 0, {0})
    g.setpos(0, 0)
    showtext="Fast-forwarding to distance bit read nr. "..i
    if i==3 then
      gnPrevTime=nil
      collisionAeon=11
    end
    run_from_to(gnPrevTime, collisionAeon, bnReadTimeOffs+2048, 30, 0, {-30, -2})
    showtext="Note the gliders going from the center ..."
    gnPrevTime=bnReadTimeOffs+4128
    run_for_to(2080, collisionAeon, gnPrevTime, nil, 0, {0})
  end
end

showtext="Starting crash FSE"
g.setpos(BigNum.mt.tostring(bnParsec), BigNum.mt.tostring(bnParsec))
run_for_to("1024",0,"1024",4,4,{-1})
showtext="Starting crash FNW"
g.setpos(BigNum.mt.tostring(-bnParsec), BigNum.mt.tostring(-bnParsec))
run_for_to("1024",0,"2048",4,4,{-1})
showtext="Fast-forwarding to first collision of gliders from GPSEs..."
g.setpos(0,0)
run_for_to(nil, 4, "960", 30, 6, {-30,-27,-24,-21,-18,-16,-14,-12,-10,-8,-6,-4,-2,0})
showtext="Note the gliders going  from the center"
run_for_to("992", 4, "1952", 4, 4, {0})
g.setpos(BigNum.mt.tostring(bnParsecHalf), BigNum.mt.tostring(bnParsecHalf))
showtext="Fast-forwarding to first FSE glider with GPSE collision..."
run_for_to(nil, 6, "448", 30, 2, {-30,-27,-24,-21,-18,-16,-14,-12,-10,-8,-6,-4,-2,0})
g.setpos("-"..BigNum.mt.tostring(bnParsecHalf), "-"..BigNum.mt.tostring(bnParsecHalf))
showtext="Meanwhile first FNW glider with GPSE collision..."
run_for_to("1024", 6, "1472")
g.setpos(BigNum.mt.tostring(bnParsecHalf), BigNum.mt.tostring(bnParsecHalf))
run_for_to("1024", 6, "2496")
g.setpos(0, 0)
showtext="Fast-forwarding to first distance bit read ..."
run_for_to(nil,8,"1280",30, 6, {-2})
showtext="Note the gliders going  from the center ... NW one will return (as the next bit is 1) as a '11' echo signal in next read bit, the SE near the glider line is the real signal"
run_for_to("1024", 8, "2304", 4, 4, {0})
g.setpos(BigNum.mt.tostring(bnParsecQuarter), BigNum.mt.tostring(bnParsecQuarter))
showtext="Fast-forwarding to second FSE glider with GPSE collision..."
run_for_to(nil, 9, "1184", 30,2,{-30,-27,-24,-21,-18,-16, -14, -12, -10, -8, -6, -4, -2, 0})
g.setpos("-"..BigNum.mt.tostring(bnParsecQuarter), "-"..BigNum.mt.tostring(bnParsecQuarter))
showtext="Meanwhile second FNW glider with GPSE collision..."
run_for_to("1024", 9, "2208")
g.setpos(BigNum.mt.tostring(bnParsecQuarter), BigNum.mt.tostring(bnParsecQuarter))
run_for_to("1024", 9, "3032")
g.setpos(0, 0)
showtext="Fast-forwarding to second distance bit read ..."
run_for_to(nil,10, "1088", 30, 6, {-2})
showtext="Note the gliders going  from the center ... NW one will return (as next bit is 1) as a '11' echo signal in next read bit, the SE near the glider line is the real signal, the further SE glider is the '11' echo which will modify the GPSE trash with no more consequences"
run_for_to("2048", 10, "3168", 4, 4, {0})
showtext="Fast-forwarding to first inserted MWSS bits ... here only 28 bits are encoded in pattern size"
run_for_to("225672", 10, "228840", nil, 4, {-24, -16, -8, -4, -2, 0})
showtext="Each inserted bit reduces pattern dimensions to half, there will be no chance to wait for result without the reduction"
run_for_to("17984", 10, "246824", nil, nil, {0})
'''


def write_script(filename, number, tags):

    aeon_length = number + 96
    half_parsec = aeon_length // 2
    # 199720 replaced by 68648 I bet first two gliders should not be counted
    pos_offset = 68648 # 199720
    start_offset = 246824
    tags = sorted([((t['aeon']+10) * aeon_length + t['pos'] * 65536 + pos_offset + (t['time_offset'] if 'time_offset' in t else 0) , i, t) for (i, t) in enumerate(tags)])
    lang = filename.split('.')[-1]
    thing = {'py': base_python, 'lua': base_lua}[lang]
    currmag = "0"

    with open(filename, 'w') as f:
        if lang == 'lua':
            f.write(thing % aeon_length)
            #f.write(thing)
        else:
            f.write(thing % number)

        curr_ts = 10*aeon_length + start_offset
        curr_loc = (0, 0)

        for (ts, _, tag) in tags:

            ts_diff = ts - curr_ts
            curr_ts = ts
            curr_aeon = (ts+half_parsec)//aeon_length
            aeon_offset = curr_aeon * aeon_length

            if lang == 'lua':

                if tag['priority']<2:
                    f.write('showtextbase="%s"\n' % tag['description'])
                    f.write('showtext=showtextbase\n')
                else:
                    f.write('showtext=showtextbase.." - %s"\n' % tag['description'])

                mags='nil'
                if 'setmag' in tag:
                    tagmags = tag['setmag'][1:-1]
                    mags = '{%s,%s}' % (currmag,tagmags)
                    currmag = tagmags[(","+tagmags).rfind(","):]
                note = 'nil'
                if 'note' in tag:
                    note="'%s'" % (tag['note'])
                istep = 'nil'
                if 'istep' in tag:
                    istep = tag['istep']

                if ts_diff > 0:
                    f.write('run_for_to("%d",%d,"%d",%s,nil,%s,%s)\n' % (ts_diff, curr_aeon, curr_ts-aeon_offset, istep, mags, note))

            else:
                if ts_diff > 0:
                    f.write('run_for(%d)\n' % ts_diff)

                if 'note' in tag:
                    f.write('g.note("%s", false_literal)\n' % tag['note'])

                if 'setmag' in tag:
                    f.write('g.setmag(%d)\n' % tag['setmag'])

                f.write('g.show("%s")\n' % tag['description'])
            new_loc = tuple(tag['location'])
            if (new_loc != curr_loc) or ('offset' in tag):
                coords = tuple([x * aeon_length + y for (x, y) in zip(new_loc, tag.get('offset', (0, 0)))])
                f.write('g.setpos("%d", "%d")\n' % coords)

            curr_loc = new_loc

if __name__ == '__main__':

    with open('binary.txt') as f:
        dbca = f.read().replace('\n', '').strip()

    # These three last bits don't get released, because the switch-engines
    # have already crashed at this point:
    rct, number = make_rct_semilator(dbca + '111')

    print('Loading tags')
    with open('tags.json') as f:
        tags = json.load(f)

    print('Writing rct15.mc')
    rct.save('rct15.mc')

    print('Creating Golly scripts')
    # write_script('show_rct15.py', number, tags)
    write_script('show_rct15.lua', number, tags)

