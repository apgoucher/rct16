
local showtext, showtextbase = "", ""
local g = golly()

-----------------------------------------------------
local RADIX_LEN = 6 ;
local RADIX_FIL = "000000" ; -- length >= RADIX_LEN
local RADIX = 10^RADIX_LEN ;

BigNum = {} ;
BigNum.mt = {} ;

function BigNum.new( num )
   local bignum = {} ;
   setmetatable( bignum , BigNum.mt ) ;
   BigNum.change( bignum , num ) ;
   return bignum ;
end

function BigNum.mt.sub( num1 , num2 )
   local temp = BigNum.new() ;
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   BigNum.sub( bnum1 , bnum2 , temp ) ;
   return temp ;
end

function BigNum.mt.add( num1 , num2 )
   local temp = BigNum.new() ;
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   BigNum.add( bnum1 , bnum2 , temp ) ;
   return temp ;
end

function BigNum.mt.tostring( bnum )
   local i = 0
   local j
   local temp = ""
   local str
   local dotpos
   if bnum == nil then
      return "nil"
   elseif bnum.len > 0 then
      for i = bnum.len - 2 , 0 , -1  do
         str = tostring(bnum[i])
         dotpos = string.find(str..".","%.")
         temp = temp .. string.sub(RADIX_FIL..string.sub(str,1,dotpos-1),-RADIX_LEN)
      end
      str = tostring(bnum[bnum.len - 1])
      dotpos = string.find(str..".","%.")
      temp = string.sub(str,1,dotpos-1) .. temp
      if bnum.signal == '-' then
         temp = bnum.signal .. temp
      end
      return temp
   else
      return ""
   end
end

function BigNum.mt.eq( num1 , num2 )
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   return BigNum.eq( bnum1 , bnum2 ) ;
end

function BigNum.mt.lt( num1 , num2 )
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   return BigNum.lt( bnum1 , bnum2 ) ;
end

function BigNum.mt.le( num1 , num2 )
   local bnum1 = BigNum.new( num1 ) ;
   local bnum2 = BigNum.new( num2 ) ;
   return BigNum.le( bnum1 , bnum2 ) ;
end

function BigNum.mt.unm( num )
   local ret = BigNum.new( num )
   if ret.signal == "+" then
      ret.signal = "-"
   else
      ret.signal = "+"
   end
   return ret
end

function BigNum.mt.half( num )
   local ret = BigNum.new()
   local bnum = BigNum.new( num ) ;
   BigNum.half( bnum , ret ) ;
   return ret ;
end

BigNum.mt.__metatable = "hidden"
BigNum.mt.__tostring  = BigNum.mt.tostring ;
BigNum.mt.__add = BigNum.mt.add ;
BigNum.mt.__sub = BigNum.mt.sub ;
BigNum.mt.__unm = BigNum.mt.unm ;
BigNum.mt.__eq = BigNum.mt.eq   ;
BigNum.mt.__le = BigNum.mt.le   ;
BigNum.mt.__lt = BigNum.mt.lt   ;

setmetatable( BigNum.mt, { __index = "inexistent field", __newindex = "not available", __metatable="hidden" } ) ;

function BigNum.add( bnum1 , bnum2 , bnum3 )
   local maxlen = 0 ;
   local i = 0 ;
   local carry = 0 ;
   local signal = "+" ;
   local old_len = 0 ;
   if bnum1 == nil or bnum2 == nil or bnum3 == nil then
      error("Function BigNum.add: parameter nil") ;
   elseif bnum1.signal == "-" and bnum2.signal == "+" then
      bnum1.signal = "+" ;
      BigNum.sub( bnum2 , bnum1 , bnum3 ) ;

      if not rawequal(bnum1, bnum3) then
         bnum1.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "+" and bnum2.signal == "-" then
      bnum2.signal = "+" ;
      BigNum.sub( bnum1 , bnum2 , bnum3 ) ;
      if not rawequal(bnum2, bnum3) then
         bnum2.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "-" and bnum2.signal == "-" then
      signal = "-" ;
   end
   old_len = bnum3.len ;
   if bnum1.len > bnum2.len then
      maxlen = bnum1.len ;
   else
      maxlen = bnum2.len ;
      bnum1 , bnum2 = bnum2 , bnum1 ;
   end
   for i = 0 , maxlen - 1 do
      if bnum2[i] ~= nil then
         bnum3[i] = bnum1[i] + bnum2[i] + carry ;
      else
         bnum3[i] = bnum1[i] + carry ;
      end
      if bnum3[i] >= RADIX then
         bnum3[i] = bnum3[i] - RADIX ;
         carry = 1 ;
      else
         carry = 0 ;
      end
   end
   if carry == 1 then
      bnum3[maxlen] = 1 ;
   end
   bnum3.len = maxlen + carry ;
   bnum3.signal = signal ;
   for i = bnum3.len, old_len do
      bnum3[i] = nil ;
   end
   return 0 ;
end

function BigNum.sub( bnum1 , bnum2 , bnum3 )
   local maxlen = 0 ;
   local i = 0 ;
   local carry = 0 ;
   local old_len = 0 ;

   if bnum1 == nil or bnum2 == nil or bnum3 == nil then
      error("Function BigNum.sub: parameter nil") ;
   elseif bnum1.signal == "-" and bnum2.signal == "+" then
      bnum1.signal = "+" ;
      BigNum.add( bnum1 , bnum2 , bnum3 ) ;
      bnum3.signal = "-" ;
      if not rawequal(bnum1, bnum3) then
         bnum1.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "-" and bnum2.signal == "-" then
      bnum1.signal = "+" ;
      bnum2.signal = "+" ;
      BigNum.sub( bnum2, bnum1 , bnum3 ) ;
      if not rawequal(bnum1, bnum3) then
         bnum1.signal = "-" ;
      end
      if not rawequal(bnum2, bnum3) then
         bnum2.signal = "-" ;
      end
      return 0 ;
   elseif bnum1.signal == "+" and bnum2.signal == "-" then
      bnum2.signal = "+" ;
      BigNum.add( bnum1 , bnum2 , bnum3 ) ;
      if not rawequal(bnum2, bnum3) then
         bnum2.signal = "-" ;
      end
      return 0 ;
   end
   if BigNum.compareAbs( bnum1 , bnum2 ) == 2 then
      BigNum.sub( bnum2 , bnum1 , bnum3 ) ;
      bnum3.signal = "-" ;
      return 0 ;
   else
      maxlen = bnum1.len ;
   end
   old_len = bnum3.len ;
   bnum3.len = 0 ;
   for i = 0 , maxlen - 1 do
      if bnum2[i] ~= nil then
         bnum3[i] = bnum1[i] - bnum2[i] - carry ;
      else
         bnum3[i] = bnum1[i] - carry ;
      end
      if bnum3[i] < 0 then
         bnum3[i] = RADIX + bnum3[i] ;
         carry = 1 ;
      else
         carry = 0 ;
      end

      if bnum3[i] ~= 0 then
         bnum3.len = i + 1 ;
      end
   end
   bnum3.signal = "+" ;
   if bnum3.len == 0 then
      bnum3.len = 1 ;
      bnum3[0]  = 0 ;
   end
   if carry == 1 then
      error( "Error in function sub" ) ;
   end
   for i = bnum3.len , max( old_len , maxlen - 1 ) do
      bnum3[i] = nil ;
   end
   return 0 ;
end

function BigNum.half( bnum1 , bnum2 )
   local maxlen = bnum1.len ;
   local i = 0 ;
   if bnum1 == nil or bnum2 == nil then
      error("Function BigNum.half: parameter nil") ;
   end
   for i = maxlen, bnum2.len-1, 1 do
      bnum2[i] = nil ;
   end
   bnum2.len = maxlen
   local carry = 0
   if bnum1[maxlen-1]<2 then
     bnum2[maxlen-1]=nil
     bnum2.len=maxlen-1
     carry = bnum1[maxlen-1] % 2
   end
   for i = bnum2.len - 1,0,-1 do
     bnum2[i] = (bnum1[i] + carry * RADIX)//2
     carry = bnum1[i] % 2
   end
   bnum2.signal = bnum1.signal ;
   if bnum2.len == 0 then
      bnum2.len = 1 ;
      bnum2[0]  = 0 ;
   end
   return carry == 1 ;
end

function BigNum.eq( bnum1 , bnum2 )
   if BigNum.compare( bnum1 , bnum2 ) == 0 then
      return true ;
   else
      return false ;
   end
end

function BigNum.lt( bnum1 , bnum2 )
   if BigNum.compare( bnum1 , bnum2 ) == 2 then
      return true ;
   else
      return false ;
   end
end

function BigNum.le( bnum1 , bnum2 )
   local temp = -1 ;
   temp = BigNum.compare( bnum1 , bnum2 )
   if temp == 0 or temp == 2 then
      return true ;
   else
      return false ;
   end
end

function BigNum.compareAbs( bnum1 , bnum2 )
   if bnum1 == nil or bnum2 == nil then
      error("Function compare: parameter nil") ;
   elseif bnum1.len > bnum2.len then
      return 1 ;
   elseif bnum1.len < bnum2.len then
      return 2 ;
   else
      local i ;
      for i = bnum1.len - 1 , 0 , -1 do
         if bnum1[i] > bnum2[i] then
            return 1 ;
         elseif bnum1[i] < bnum2[i] then
            return 2 ;
         end
      end
   end
   return 0 ;
end

function BigNum.compare( bnum1 , bnum2 )
   local signal = 0 ;

   if bnum1 == nil or bnum2 == nil then
      error("Funtion BigNum.compare: parameter nil") ;
   elseif bnum1.signal == "+" and bnum2.signal == "-" then
      return 1 ;
   elseif bnum1.signal == "-" and bnum2.signal == "+" then
      return 2 ;
   elseif bnum1.signal == "-" and bnum2.signal == "-" then
      signal = 1 ;
   end
   if bnum1.len > bnum2.len then
      return 1 + signal ;
   elseif bnum1.len < bnum2.len then
      return 2 - signal ;
   else
      local i ;
      for i = bnum1.len - 1 , 0 , -1 do
         if bnum1[i] > bnum2[i] then
            return 1 + signal ;
	 elseif bnum1[i] < bnum2[i] then
	    return 2 - signal ;
	 end
      end
   end
   return 0 ;
end

function BigNum.copy( bnum1 , bnum2 )
   if bnum1 ~= nil and bnum2 ~= nil then
      local i ;
      for i = 0 , bnum1.len - 1 do
         bnum2[i] = bnum1[i] ;
      end
      bnum2.len = bnum1.len ;
   else
      error("Function BigNum.copy: parameter nil") ;
   end
end

function BigNum.change( bnum1 , num )
   local j = 0 ;
   local len = 0  ;
   local num = num ;
   local l ;
   local oldLen = 0 ;
   if bnum1 == nil then
      error( "BigNum.change: parameter nil" ) ;
   elseif type( bnum1 ) ~= "table" then
      error( "BigNum.change: parameter error, type unexpected" ) ;
   elseif num == nil then
      bnum1.len = 1 ;
      bnum1[0] = 0 ;
      bnum1.signal = "+";
   elseif type( num ) == "table" and num.len ~= nil then  --check if num is a big number
      for i = 0 , num.len do
         bnum1[i] = num[i] ;
      end
      if num.signal ~= "-" and num.signal ~= "+" then
         bnum1.signal = "+" ;
      else
         bnum1.signal = num.signal ;
      end
      oldLen = bnum1.len ;
      bnum1.len = num.len ;
   elseif type( num ) == "string" or type( num ) == "number" then
      if string.sub( num , 1 , 1 ) == "+" or string.sub( num , 1 , 1 ) == "-" then
         bnum1.signal = string.sub( num , 1 , 1 ) ;
         num = string.sub(num, 2) ;
      else
         bnum1.signal = "+" ;
      end
      num = string.gsub( num , " " , "" )
      local sf = string.find( num , "e" )
      if sf ~= nil then
         local e = string.sub( num , sf + 1 )
         num = string.sub( num , 1 , sf - 1 )
         e = tonumber(e) ;
         if e ~= nil and e > 0 then
            e = tonumber(e) ;
         else
            error( "Function BigNum.change: string is not a valid number" ) ;
         end
         local dotpos=string.find(num..".","%.")
         num = string.gsub( num , "%." , "" )
         local dotshift = string.len(num)+1-dotpos
         for i = 1 , e do
            num = num .. "0"
         end
         if dotshift>0 then num = string.sub(num,1,-dotshift-1) end
      else
         sf = string.find( num , "%." ) ;
         if sf ~= nil then
            num = string.sub( num , 1 , sf - 1 ) ;
         end
      end

      l = string.len( num ) ;
      oldLen = bnum1.len ;
      if (l > RADIX_LEN) then
         local mod = l-( math.floor( l / RADIX_LEN ) * RADIX_LEN ) ;
         for i = 1 , l-mod, RADIX_LEN do
            bnum1[j] = tonumber( string.sub( num, -( i + RADIX_LEN - 1 ) , -i ) );
            --Check if string dosn't represents a number
            if bnum1[j] == nil then
               error( "Function BigNum.change: string is not a valid number" ) ;
               bnum1.len = 0 ;
               return 1 ;
            end
            j = j + 1 ;
            len = len + 1 ;
         end
         if (mod ~= 0) then
            bnum1[j] = tonumber( string.sub( num , 1 , mod ) ) ;
            bnum1.len = len + 1 ;
         else
            bnum1.len = len ;
         end
         for i = bnum1.len - 1 , 1 , -1 do
            if bnum1[i] == 0 then
               bnum1[i] = nil ;
               bnum1.len = bnum1.len - 1 ;
            else
               break ;
            end
         end

      else
         bnum1[j] = tonumber( num ) ;
         bnum1.len = 1 ;
      end
   else
      error( "Function BigNum.change: parameter error, type unexpected" ) ;
   end

   if oldLen ~= nil then
      for i = bnum1.len , oldLen do
         bnum1[i] = nil ;
      end
   end

   return 0 ;
end

function BigNum.put( bnum , int , pos )
   if bnum == nil then
      error("Function BigNum.put: parameter nil") ;
   end
   local i = 0 ;
   for i = 0 , pos - 1 do
      bnum[i] = 0 ;
   end
   bnum[pos] = int ;
   for i = pos + 1 , bnum.len do
      bnum[i] = nil ;
   end
   bnum.len = pos ;
   return 0 ;
end

function printraw( bnum )
   local i = 0 ;
   if bnum == nil then
      error( "Function printraw: parameter nil" ) ;
   end
   while 1 == 1 do
      if bnum[i] == nil then
         io.write( " len "..bnum.len ) ;
         if i ~= bnum.len then
            io.write( " ERROR! " ) ;
         end
         io.write( "\n" ) ;
         return 0 ;
      end
      io.write( "r"..bnum[i] ) ;
      i = i + 1 ;
   end
end

function max( int1 , int2 )
   if int1 > int2 then
      return int1 ;
   else
      return int2 ;
   end
end

function decr( bnum1 )
   local temp = {} ;
   temp = BigNum.new( "1" ) ;
   BigNum.sub( bnum1 , temp , bnum1 ) ;
   return 0 ;
end
---------------------------------------------

g.setbase(2)
g.setpos("0","0")

local false_literal = false
local true_literal = true
local bnParsec = BigNum.new("51399934464")
local bnParsecHalf = BigNum.mt.half(bnParsec)
local bnParsecQuarter = BigNum.mt.half(bnParsecHalf)
local bnAeon=bnParsec                   -- /c
local bnAeonHalf = bnParsecHalf         --/c  (c=1)
local bnTwoAeons=bnAeon+bnAeon          -- maybe I have to implement multiplication and division:)
local bnFourAeons=bnTwoAeons+bnTwoAeons -- -"-
local startGen = g.getgen()
local bnZero = BigNum.new(0)
local currentAeon = 0
local bnCurrentAeonBase = bnZero
local bnCurrentOffset
local nrGliderGPSECollisionsToShow = 10 -- 26 is maximum displayed, whatever bigger will be interrupted (there are 28 bits in total)
local lastfstep=11

while startGen>bnCurrentAeonBase + bnAeonHalf do
  bnCurrentAeonBase = bnCurrentAeonBase + bnAeon
  currentAeon = currentAeon + 1
end
local startAeon = currentAeon

g.autoupdate(true_literal)

local function smoothmag(imag, fmag)
    while imag ~= fmag do
        if imag < fmag then
            imag = imag + 1
        else
            imag = imag - 1
        end
        g.setmag(imag)
        g.sleep(200)
    end
    return imag
end

local function run_for_to(n, targetAeon, targetgen, istep, fstep, mags, note)
    local bnTargetGen
    local bnN
    local magindex
    local enterAeon = currentAeon
    fstep = fstep or 11
    if targetAeon~=nil then
      if targetAeon+0>100 then
        targetAeon = nil
      end
    end
    if (targetgen ~= nil) and targetAeon ~= nil then
    	bnTargetGen = BigNum.new(targetgen)
        if targetAeon<currentAeon then
--          g.note(showtext.." - target Aeon smaller than current one ")
            return
        end
        while currentAeon < targetAeon do
            bnCurrentAeonBase = bnCurrentAeonBase + bnAeon
            currentAeon = currentAeon + 1
        end
    end
    if (targetgen == nil or targetAeon==nil) and (n == nil) then
        g.note(showtext.." - run_for_to with unknown duration")
        return
    end
    bnCurrentOffset=BigNum.new(g.getgen()) - bnCurrentAeonBase
    local bnStartGen=startGen - bnCurrentAeonBase
    local bnEnterGen=bnCurrentOffset
    if n ~= nil then
        bnN = BigNum.new(n)
    end
    if (targetgen ~= nil) and (targetAeon~=nil) and (n ~= nil) then
       if bnN+bnEnterGen ~= bnTargetGen  then
          if bnStartGen > bnTargetGen then return end -- skipping milestones to restart
          if bnStartGen + bnN < bnTargetGen then -- must be script error
             if bnN+bnEnterGen > bnTargetGen+BigNum.new(2^lastfstep) then
            	g.note(showtext.."("..currentAeon..") : - run_for_to for not matching to "..BigNum.mt.tostring(bnEnterGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen))
            	return
             end
          end
          --g.note(showtext.."("..currentAeon..") : "..BigNum.mt.tostring(bnGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen).." running to first milestone after restart")
          bnN = bnTargetGen - bnEnterGen
       end
    end
    if (n==nil) then -- helper for editting at the end
	bnN = bnTargetGen - bnEnterGen
      --g.note(showtext.." : "..BigNum.mt.tostring(bnGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen))
    end
    if (targetgen == nil or targetAeon == nil) then -- helper for editting
      bnTargetGen = bnN + bnEnterGen
      while bnTargetGen>bnAeonHalf do
        bnCurrentAeonBase = bnCurrentAeonBase + bnAeon
        currentAeon = currentAeon + 1
        bnTargetGen = bnTargetGen - bnAeon
        bnEnterGen = bnEnterGen - bnAeon
      end
      g.setclipstr('run_for_to("'..BigNum.mt.tostring(bnN)..'", '..currentAeon..', "'..BigNum.mt.tostring(bnTargetGen)..'"')
      g.note(showtext.."("..currentAeon..") : "..BigNum.mt.tostring(bnEnterGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen))
    end
    local bnGen
    local sTargetGen = BigNum.mt.tostring(bnTargetGen)
    if bnN<=0 then return end
    local nn=BigNum.mt.tostring(bnN)
    istep = istep or math.min(30, math.floor(math.log(nn)/math.log(2))-6)
    local cstep = g.getstep()
    if cstep<fstep then cstep = fstep end
    if cstep>istep then cstep = istep end
    if mags then
        magindex = 1
        currmag = mags[magindex]
        g.setmag(currmag)
        magindex = magindex + 1
    end
    local bn3Eistep = BigNum.new(3*2^istep)
    g.setstep(cstep)
    while bnN > bnZero do
        g.show(showtext.."("..currentAeon..") : "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnTargetGen).." i"..istep.." f"..fstep)

        while (istep > fstep) and (bnN < bn3Eistep) do
            istep = istep - 2
            if istep < 0 then istep = 0 end
            bn3Eistep = BigNum.new(3*2^istep)
            cstep = istep
            if mags and magindex <= #mags then
                currmag = smoothmag(currmag, mags[magindex])
                magindex = magindex + 1
            end
            g.setstep(cstep)
        end
        g.step()
        bnGen=BigNum.new(g.getgen()) - bnCurrentAeonBase
        bnN = bnTargetGen - bnGen
        cstep = g.getstep()
        if (cstep<istep) then -- owerwriting manual slowdown
          cstep = cstep+1
          g.setstep(cstep)
        end
        if (cstep>istep) then -- owerwriting manual speedup
          cstep = cstep-1
          g.setstep(cstep)
        end
    end
    lastfstep = fstep
    if (n==nil) and (enterAeon==currentAeon) then
      -- no explicit duration when Aeon boundary is crossed
      bnN=bnGen-bnEnterGen
      g.setclipstr('run_for_to("'..BigNum.mt.tostring(bnN)..'", '..currentAeon..', "'..BigNum.mt.tostring(bnTargetGen)..'"')
      g.note(showtext.."("..currentAeon..") : ".." final generation: "..BigNum.mt.tostring(bnEnterGen).." + "..BigNum.mt.tostring(bnN).."->"..BigNum.mt.tostring(bnGen))
    end
    if note~=nil then
      g.note(note)
    end
end

local function run_from_to(bnN, targetAeon, bnTargetGen, istep, fstep, mags)
--bnTargetgen big number not nil (bnN==nil if to new Aeon)
  if bnN==nil then
    --g.note("T:"..BigNum.mt.tostring(bnTargetGen).." D:nil")
    run_for_to(nil, targetAeon, BigNum.mt.tostring(bnTargetGen), istep, fstep, mags)
  else
    local bnDuration = bnTargetGen - bnN
    --g.note("T:"..BigNum.mt.tostring(bnTargetGen).." D:"..BigNum.mt.tostring(bnDuration))
    run_for_to(BigNum.mt.tostring(bnDuration), targetAeon, BigNum.mt.tostring(bnTargetGen), istep, fstep, mags)
  end
end

local function distance_bits(prevTime)
  -- somehow when interrupted, restart skips to "remaining bits read"
  -- ... I should check why it happens, but it is method how to speed the show up,
  -- so it looks like desired feature.
  local bngcCollisionPos = bnParsecQuarter+0
  local bngcCollisionTime = -bnAeon-bnTwoAeons
  local collisionAeon=10 -- 3rd collision only
  local bnReadTime = -bnTwoAeons
  local gnPrevTime = BigNum.new(prevTime)
  local bngcCollisionTimeOffs
  local bnReadTimeOffs
  local i
  -- 12-8 12-4 12-2   12-1     12-0.5
  -- 12-6 12-3 12-1.5 12-0.75
  for i=3,nrGliderGPSECollisionsToShow,1 do
    bngcCollisionPos=BigNum.mt.half(bngcCollisionPos)
    bngcCollisionTime=BigNum.mt.half(bngcCollisionTime)
    bnReadTime=BigNum.mt.half(bnReadTime)
    if bnReadTime>-22000 then
      -- remaining bits should be processed extra
      break
    end
    bngcCollisionTimeOffs=bngcCollisionTime+bnAeon
    bnReadTimeOffs=bnReadTime+bnAeon
    if i==3 then -- Aeon exception
      bngcCollisionTimeOffs=bngcCollisionTimeOffs+bnAeon
    end
    g.setpos(BigNum.mt.tostring(bngcCollisionPos), BigNum.mt.tostring(bngcCollisionPos))
    showtext="Fast-forwarding to FSE glider with GPSE collision nr. "..i
    run_from_to(gnPrevTime, collisionAeon, bngcCollisionTimeOffs+2048, 30, 0, {-30,-27,-24,-21,-18,-16,-14,-12,-10,-8,-6,-4,-2,0})
    gnPrevTime=bngcCollisionTimeOffs+4096
    run_for_to("2048", collisionAeon, gnPrevTime, nil, 0, {0})
    g.setpos(0, 0)
    showtext="Fast-forwarding to distance bit read nr. "..i
    if i==3 then
      gnPrevTime=nil
      collisionAeon=11
    end
    run_from_to(gnPrevTime, collisionAeon, bnReadTimeOffs+2048, 30, 0, {-30, -2})
    showtext="Note the gliders going from the center ..."
    gnPrevTime=bnReadTimeOffs+4128
    run_for_to(2080, collisionAeon, gnPrevTime, nil, 0, {0})
  end
end

showtext="Starting crash FSE"
g.setpos(BigNum.mt.tostring(bnParsec), BigNum.mt.tostring(bnParsec))
run_for_to("1024",0,"1024",4,4,{-1})
showtext="Starting crash FNW"
g.setpos(BigNum.mt.tostring(-bnParsec), BigNum.mt.tostring(-bnParsec))
run_for_to("1024",0,"2048",4,4,{-1})
showtext="Fast-forwarding to first collision of gliders from GPSEs..."
g.setpos(0,0)
run_for_to(nil, 4, "960", 30, 6, {-30,-27,-24,-21,-18,-16,-14,-12,-10,-8,-6,-4,-2,0})
showtext="Note the gliders going  from the center"
run_for_to("992", 4, "1952", 4, 4, {0})
g.setpos(BigNum.mt.tostring(bnParsecHalf), BigNum.mt.tostring(bnParsecHalf))
showtext="Fast-forwarding to first FSE glider with GPSE collision..."
run_for_to(nil, 6, "448", 30, 2, {-30,-27,-24,-21,-18,-16,-14,-12,-10,-8,-6,-4,-2,0})
g.setpos("-"..BigNum.mt.tostring(bnParsecHalf), "-"..BigNum.mt.tostring(bnParsecHalf))
showtext="Meanwhile first FNW glider with GPSE collision..."
run_for_to("1024", 6, "1472")
g.setpos(BigNum.mt.tostring(bnParsecHalf), BigNum.mt.tostring(bnParsecHalf))
run_for_to("1024", 6, "2496")
g.setpos(0, 0)
showtext="Fast-forwarding to first distance bit read ..."
run_for_to(nil,8,"1280",30, 6, {-2})
showtext="Note the gliders going  from the center ... NW one will return (as the next bit is 1) as a '11' echo signal in next read bit, the SE near the glider line is the real signal"
run_for_to("1024", 8, "2304", 4, 4, {0})
g.setpos(BigNum.mt.tostring(bnParsecQuarter), BigNum.mt.tostring(bnParsecQuarter))
showtext="Fast-forwarding to second FSE glider with GPSE collision..."
run_for_to(nil, 9, "1184", 30,2,{-30,-27,-24,-21,-18,-16, -14, -12, -10, -8, -6, -4, -2, 0})
g.setpos("-"..BigNum.mt.tostring(bnParsecQuarter), "-"..BigNum.mt.tostring(bnParsecQuarter))
showtext="Meanwhile second FNW glider with GPSE collision..."
run_for_to("1024", 9, "2208")
g.setpos(BigNum.mt.tostring(bnParsecQuarter), BigNum.mt.tostring(bnParsecQuarter))
run_for_to("1024", 9, "3032")
g.setpos(0, 0)
showtext="Fast-forwarding to second distance bit read ..."
run_for_to(nil,10, "1088", 30, 6, {-2})
showtext="Note the gliders going  from the center ... NW one will return (as next bit is 1) as a '11' echo signal in next read bit, the SE near the glider line is the real signal, the further SE glider is the '11' echo which will modify the GPSE trash with no more consequences"
run_for_to("2048", 10, "3168", 4, 4, {0})
showtext="Fast-forwarding to first inserted MWSS bits ... here only 28 bits are encoded in pattern size"
run_for_to("225672", 10, "228840", nil, 4, {-24, -16, -8, -4, -2, 0})
showtext="Each inserted bit reduces pattern dimensions to half, there will be no chance to wait for result without the reduction"
run_for_to("17984", 10, "246824", nil, nil, {0})
showtextbase="Transform the initial crash site into a usable blinker elbow"
showtext=showtextbase
run_for_to("16384",10,"263208",nil,nil,nil,nil)
showtextbase="Building LWSS seed and blocks"
showtext=showtextbase
run_for_to("16386048",10,"16649256",nil,nil,nil,nil)
showtext=showtextbase.." - Creating block needed for ECCA construction and another one to construct a GPSE stopper"
run_for_to("21102592",10,"37751848",nil,nil,nil,nil)
showtext=showtextbase.." - LWSS seed construction started"
run_for_to("33882112",10,"71633960",nil,nil,nil,nil)
showtext=showtextbase.." - LWSS seed construction ended, stackbottom repositioning starts"
run_for_to("125435904",10,"197069864",nil,nil,{0,-3},nil)
g.setpos("600", "-600")
showtext=showtextbase.." - Stackbottom repositioning ended"
run_for_to("838074368",10,"1035144232",nil,nil,nil,nil)
showtextbase="Building glider seed"
showtext=showtextbase
g.setpos("1200", "-1200")
showtext=showtextbase.." - Creating target block for glider seed"
showtext=showtextbase.." - Start of glider seed recipe"
run_for_to("16580608",10,"1051724840",nil,nil,nil,nil)
showtext=showtextbase.." - End of glider seed recipe"
run_for_to("80740352",10,"1132465192",nil,nil,nil,nil)
showtext=showtextbase.." - Switch to stack items better for long fills"
run_for_to("2228224",10,"1134693416",nil,nil,{0,-2},nil)
g.setpos("600", "-1400")
showtextbase="Creating synced glider pair to invoke the seeds to create distant target"
showtext=showtextbase
showtext=showtextbase.." - Creating long gap on the stack"
showtext=showtextbase.." - You may notice the last 3 pairs of beehive pairs differ from he rest"
run_for_to("35782656",10,"1170476072",nil,nil,nil,nil)
showtext=showtextbase.." - Expensive transformation to standard stack items ended"
run_for_to("10616832",10,"1181092904",nil,nil,nil,nil)
showtext=showtextbase.." - Target for the subrecipe created, glider pair seed recipe starts"
run_for_to("12845056",10,"1193937960",nil,nil,nil,nil)
showtext=showtextbase.." - Glider pair seed recipe ended"
run_for_to("101842944",10,"1295780904",nil,nil,nil,nil)
showtext=showtextbase.." - Distant target creation seed activation"
run_for_to("3407872",10,"1299188776",nil,nil,nil,nil)
showtextbase="Distant target creation seed activated"
showtext=showtextbase
run_for_to("65536",10,"1299254312",nil,nil,nil,nil)
showtext=showtextbase.." - Stack have to return to the position to build DBCA"
run_for_to("1638400",10,"1300892712",nil,nil,nil,nil)
showtext=showtextbase.." - Stack returned  to the position to build DBCA"
run_for_to("3145728",10,"1304038440",nil,nil,nil,nil)
showtextbase="(Green) DBCA building recipe applied to the distant target, including cordership activation seed, the most expensive part of the RCT15 constant part "
showtext=showtextbase
showtext=showtextbase.." - (Green) DBCA sleeping pattern is ready, we have to replace the stack by reflector targets to ignite the corderships"
run_for_to("24649596928",11,"-25446299096",nil,nil,nil,nil)
showtext=showtextbase.." - Building 2 target blinkers for DBCA reflectors, 1 target blinker for ECCA reflectors and glider to activate the cordership"
run_for_to("10682368",11,"-25435616728",nil,nil,nil,nil)
showtext=showtextbase.." - Big move of stack bottom to ECCA target starts. It uses easily destroyable filler."
showtext=showtextbase.." - Expensive part converting stack top to blinker."
run_for_to("20578304",11,"-25415038424",nil,nil,nil,nil)
showtext=showtextbase.." - Filler removal."
run_for_to("11141120",11,"-25403897304",nil,nil,nil,nil)
showtext=showtextbase.." - Gap separating ECCA reflector target from 1st DBCA reflector target was created"
run_for_to("10354688",11,"-25393542616",nil,nil,nil,nil)
showtext=showtextbase.." - Gap separating 1st DBCA reflector target from 2nd DBCA reflector target was created"
run_for_to("6029312",11,"-25387513304",nil,nil,nil,nil)
showtext=showtextbase.." - Gap separating glider construction stack from the reflector targets created"
run_for_to("6881280",11,"-25380632024",nil,nil,nil,nil)
showtextbase="Activating the cordership to start DBCA reflectors"
showtext=showtextbase
run_for_to("13303808",11,"-25367328216",nil,nil,nil,nil)
showtext=showtextbase.." - DBCA reflectors activated"
run_for_to("65536",11,"-25367262680",nil,nil,nil,nil)
showtextbase="remnants of the original arm removed, green DBCA activated, white DBCA is going to be built"
showtext=showtextbase
run_for_to("589824",11,"-25366672856",nil,nil,nil,nil)
showtext=showtextbase.." - Creating eater at the Green line and White target using Green DBCA"
showtextbase="Green portion of DBCA really finished, White portion construction begins"
showtext=showtextbase
run_for_to("18874368",11,"-25347798488",nil,nil,nil,nil)
showtextbase="Decoder and better construction arm (DBCA) finished, FSE switching circuitry follows"
showtext=showtextbase
run_for_to("536936448",11,"-24810862040",nil,nil,{-2,-3},nil)
showtext=showtextbase.." - Half of N(+26) gliders absorber finished"
run_for_to("27131904",11,"-24783730136",nil,nil,nil,nil)
showtext=showtextbase.." - cleanup of the PI explosion from the initial GPSE collision starts"
g.setpos("0", "0")
showtext=showtextbase.." - cleanup of the PI explosion from the initial GPSE collision ended,"
run_for_to("29884416",11,"-24753845720",nil,nil,nil,nil)
showtext=showtextbase.." - GPSE stopping block positioned"
run_for_to("5701632",11,"-24748144088",nil,nil,nil,nil)
showtextbase="Releasing 3 gliders FSE to prepare a one time turner"
showtext=showtextbase
run_for_to("7602176",11,"-24740541912",nil,nil,nil,nil)
showtext=showtextbase.." - Releasing second glider FSE"
run_for_to("5111808",11,"-24735430104",nil,nil,nil,nil)
showtext=showtextbase.." - Releasing third glider FSE"
run_for_to("3932160",11,"-24731497944",nil,nil,nil,nil)
showtext=showtextbase.." - Target for ECCA construction and SW switching circuitry ready"
run_for_to("1441792",11,"-24730056152",nil,nil,nil,nil)
showtextbase="Creating Green ECCA with SoD"
showtext=showtextbase
showtext=showtextbase.." - Debug whole ECCA construction"
run_for_to("256",11,"-24730055896",nil,nil,nil,nil)
showtext=showtextbase.." - East p256 gun started"
run_for_to("5339610880",11,"-19390445016",nil,nil,nil,nil)
showtext=showtextbase.." - West p256 gun started"
run_for_to("23396352",11,"-19367048664",nil,nil,nil,nil)
showtextbase="ECCA with SoD started"
showtext=showtextbase
run_for_to("65536",11,"-19366983128",nil,nil,nil,nil)
showtext=showtextbase.." - Switch FSE"
g.setpos("0", "0")
showtext=showtextbase.." - Switch FSE ready"
run_for_to("212533248",11,"-19154449880",nil,nil,{0,0},nil)
g.setpos("0", "0")
showtextbase="Switched FSE"
showtext=showtextbase
run_for_to("327680",11,"-19154122200",nil,nil,{0,-3},nil)
g.setpos("500", "-500")
showtextbase="Corderabsorbers program sent, N gliders absorber is away so stopping the DBCA"
showtext=showtextbase
run_for_to("1012727808",11,"-18141394392",nil,nil,nil,nil)
showtext=showtextbase.." - signal 2 way cleaned, the bit later destroying the ECCA sent (222) already converted  to 112, will be converted to 111 after 8 aeons"
run_for_to("196608",11,"-18141197784",nil,nil,nil,nil)
showtextbase="And this is end"
showtext=showtextbase
run_for_to("4294311936",11,"-13846885848",nil,nil,nil,nil)
showtext=showtextbase.." - First glider arrived to FSE"
run_for_to("194706081792",15,"-24740541912",nil,nil,nil,nil)
g.setpos("51399934464", "51399934464")
showtext=showtextbase.." - Second glider arrived to FSE"
run_for_to("5111808",15,"-24735430104",nil,nil,nil,nil)
showtext=showtextbase.." - Third glider arrived to FSE"
run_for_to("3932160",15,"-24731497944",nil,nil,nil,nil)
showtext=showtextbase.." - Switched FSE"
run_for_to("5577375744",15,"-19154122200",nil,nil,{-3,-2},nil)
g.setpos("51399934964", "51399933964")
showtextbase="FSE Cleanup"
showtext=showtextbase
run_for_to("65536",15,"-19154056664",nil,nil,{-2,-1},nil)
g.setpos("51399934464", "51399934464")
showtextbase="FSE Cleanup finished"
showtext=showtextbase
run_for_to("27721728",15,"-19126334936",nil,nil,nil,nil)
g.setpos("51399934464", "51399934464")
showtextbase="FSE corderabsorbers ready"
showtext=showtextbase
run_for_to("985006080",15,"-18141328856",nil,nil,nil,nil)
showtext=showtextbase.." - reflecting 3rd bit, half of the signal will cause destroying ECCA"
run_for_to("129024",15,"-18141199832",nil,nil,nil,nil)
showtext=showtextbase.." - the show did not freeze, just a lot of hash entries should be computed to simulate it fast"
run_for_to("205599608832",19,"-18141328856",28,nil,nil,nil)
g.setpos("0", "0")
showtextbase="green ECCA program starts by DBCA destroyal"
showtext=showtextbase
run_for_to("65280",19,"-18141263576",nil,nil,{-1,-2},nil)
showtext=showtextbase.." - first data signal reflected to destroy FSE reflectors"
run_for_to("65536",19,"-18141198040",nil,nil,nil,nil)
showtext=showtextbase.." - Hitting DBCA block 1/3 on the way to stop the reset line"
run_for_to("26214400",19,"-18114983640",nil,nil,nil,nil)
g.setpos("0", "-2000")
showtext=showtextbase.." - DBCA reset line stopped"
run_for_to("786432",19,"-18114197208",nil,nil,nil,nil)
showtext=showtextbase.." - DBCA S p256 stopped"
run_for_to("9175040",19,"-18105022168",nil,nil,nil,nil)
showtext=showtextbase.." - DBCA N p256 stopped"
run_for_to("4259840",19,"-18100762328",nil,nil,nil,nil)
showtextbase="White ECCA part construction begins"
showtext=showtextbase
run_for_to("169804032",19,"-17930958296",nil,nil,{-2,-2},nil)
g.setpos("0", "0")
showtext=showtextbase.." - Kickback reaction finished by using glider of the other phase"
run_for_to("5046272",19,"-17925912024",nil,nil,nil,nil)
showtextbase="ECCA fully functional, cleanup of GPSE tracks starts"
showtext=showtextbase
run_for_to("488898560",19,"-17437013464",nil,nil,nil,nil)
showtextbase="The ECCA copies arm block FNW"
showtext=showtextbase
run_for_to("61800448",19,"-17375213016",nil,nil,{-2,-2},nil)
showtextbase="The ECCA launches a salvo of Corderships to clean the southeast trail"
showtext=showtextbase
run_for_to("21037056",19,"-17354175960",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - The ECCA launched 1th Cordership to clean the southeast trail"
run_for_to("43253760",19,"-17310922200",nil,nil,{-2,-2},'debug?')
showtextbase="The ECCA launches Corderfleet to clean the northwest trail"
showtext=showtextbase
run_for_to("1458962688",19,"-15851959512",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - Beehive 2 block for CSWR ship"
run_for_to("900071168",19,"-14951888344",nil,nil,{-2,-2},'debug?')
showtext=showtextbase.." - Beehive 2 block for CSWL fleet"
run_for_to("229048320",19,"-14722840024",nil,nil,{-2,-2},'debug?')
showtextbase="FSW Corderabsorbers creating salvo sent, Corderfleet SW would follow"
showtext=showtextbase
run_for_to("9175040",19,"-14713664984",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - The ECCA launched 18th Cordership to clean the southwest trail"
run_for_to("608632832",19,"-14105032152",nil,nil,{-2,-2},nil)
showtextbase="SW Corderfleet finished, cordership NWL follows"
showtext=showtextbase
showtext=showtextbase.." - The ECCA launched 23th Cordership to clean the northwest trail"
run_for_to("44957696",19,"-14060074456",nil,nil,{-2,-2},'debug?')
showtextbase="NW Corderfleet finished, FNW program creating the absorbers would follow after creating the final pattern seed"
showtext=showtextbase
showtext=showtextbase.." - Pattern seed build start"
run_for_to("256",19,"-14060074200",nil,nil,{-2,-2},nil)
showtextbase="(empty) Pattern seed finished, ECCA arm block destroyal would follow"
showtext=showtextbase
run_for_to("7798528",19,"-14052275672",nil,nil,{-2,-2},nil)
showtextbase="ECCA near arm block destroyed, FNW corderabsorbers creating savlo would follow"
showtext=showtextbase
run_for_to("3145728",19,"-14049129944",nil,nil,{-2,-2},nil)
showtextbase="This was the last recipe bit, now we should sent some ones as ECCA is not ready for speed near the singularity otherwise"
showtext=showtextbase
run_for_to("202047488",19,"-13847082456",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - Singularity bits come to ECCA, last glider emited by ECCA in FNW"
run_for_to("65246996640",20,"-20280",nil,nil,nil,nil)
showtext=showtextbase.." - signal destroying FSE reflectors arrives and is reflected back to ECCA"
run_for_to("136058621790",23,"-18141201882",nil,nil,nil,nil)
g.setpos("51399934464", "51399934464")
showtextbase="FSW cleanup and Corderabsorbers program is arriving"
showtext=showtextbase
run_for_to("3049984002",23,"-15091217880",nil,nil,{-2,-2},nil)
g.setpos("-51399934464", "51399934464")
showtextbase="FSW Corderabsorbers finished"
showtext=showtextbase
run_for_to("377552896",23,"-14713664984",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - clearing FNW trash"
run_for_to("664535040",23,"-14049129944",nil,nil,{-2,-2},nil)
g.setpos("-51399934464", "-51399934464")
showtext=showtextbase.." - FNW trash cleared"
run_for_to("19922944",23,"-14029207000",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - This was the last recipe bit finishing FNW corderabsorbrs"
run_for_to("182124544",23,"-13847082456",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - signal destroying ECCA arrived"
run_for_to("201305606144",27,"-18141214168",nil,nil,nil,nil)
g.setpos("0", "0")
showtext=showtextbase.." - ECCA destroyed"
run_for_to("16384",27,"-18141197784",nil,nil,nil,nil)
showtext=showtextbase.." - signal destroying ECCA FNW arm block arrived"
run_for_to("205599737856",31,"-18141197784",nil,nil,nil,nil)
g.setpos("-51399934464", "-51399934464")
showtextbase="The corderships begin colliding with the SE Corderabsorbers"
showtext=showtextbase
run_for_to("787021824",31,"-17354175960",nil,nil,{-2,-2},nil)
g.setpos("51399934464", "51399934464")
showtextbase="SW Corderfleet is arriving"
showtext=showtextbase
run_for_to("2640510976",31,"-14713664984",nil,nil,{-2,-2},nil)
g.setpos("-51399934464", "51399934464")
showtextbase="End of SW Corderfleet is arriving"
showtext=showtextbase
run_for_to("608632832",31,"-14105032152",nil,nil,{-2,-2},nil)
showtextbase="NWL Cordership is arriving"
showtext=showtextbase
run_for_to("327680",31,"-14104704472",nil,nil,{-2,-2},nil)
g.setpos("-51399934464", "-51399934464")
showtextbase="End of NW Corderfleet is arriving, see the glider sent back"
showtext=showtextbase
run_for_to("44630016",31,"-14060074456",nil,nil,{-2,-2},nil)
showtext=showtextbase.." - Glider returns to the center to invoke the Pattern seed"
run_for_to("65536",31,"-14060008920",nil,nil,{-2,-27},nil)
g.setpos("0", "0")
showtextbase="Glider returns to the center to invoke the Pattern seed"
showtext=showtextbase
run_for_to("205599676412",35,"-14060070364",nil,nil,{-27,-10,-7,-5,-3},'This is the end of the show')
